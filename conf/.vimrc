syntax on
set encoding=utf8

"""""""""""""""""""""""""""""""""""""""""""""""""""
" BASICS CONFIGURATION
"""""""""""""""""""""""""""""""""""""""""""""""""""
" Set the leader key
let mapleader='\\'

" Set the number of lines of history VIM has to keep
set history=500

" Enable filetype plugins
filetype indent on
filetype plugin indent on

" Set to auto read when a file is changed from the outside
set autoread
au FocusGained,BufEnter * checktime

" For regular expressions turn magic on
set magic

" Show matching brackets when text indicator is over them
set showmatch

" How many tenths of a second to blink when matching brackets
set mat=2

" No annoying sound on errors
set noerrorbells
set novisualbell
set t_vb=              " no beep or flash has visual bell
set timeoutlen=500

" Return to last edit position when opening files (You want this!)
if has("autocmd")
    au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$") | exe "normal! g`\"" | endif
endif

" folding: global toggle with zM & zR to fold all and unfold all
"          za or <space> to toggle a fold
"          zj and zk  move downwards and upwards to the next fold
nnoremap <space> za  " shortcut za with space to toggle fold/unfold
set nofoldenable
" set foldcolumn=2  " use 2 first column to give info on folds

"""""""""""""""""""""""""""
" => Search
"""""""""""""""""""""""""""
set incsearch " do incremental searching
set hlsearch
set ignorecase " make search case insensitive...

"""""""""""""""""""""""""""
" => Colors and fonts
"""""""""""""""""""""""""""
" Enable syntax highlighting
syntax enable

" Enable 256 colors palette in Gnome Terminal
if $COLORTERM == 'gnome-terminal'
    set t_Co=256
endif

" Set extra options when running in GUI mode
if has("gui_running")
    set guioptions-=T
    set guioptions-=e
    set t_Co=256
    set guitablabel=%M\ %t
endif

" Use Unix as the standard file type
set fileformats=unix,dos,mac

" Set line limits to 80 and 100 characters
" set colorcolumn=88,100   " tempted to remove this add trailling slashes on
"                          " mouse cup/paste

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Files, backups and undo
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Turn backup off, since most stuff is in SVN, git etc. anyway...
set nobackup
set nowritebackup
set noswapfile

""""""""""""""""""""""""""""""
" => Visual mode related
""""""""""""""""""""""""""""""
"" Visual mode pressing * or # searches for the current selection
"vnoremap <silent> * :<C-u>call VisualSelection('', '')<CR>/<C-R>=@/<CR><CR>
"vnoremap <silent> # :<C-u>call VisualSelection('', '')<CR>?<C-R>=@/<CR><CR>

"""""""""""""""""""""""""""
" => Filetype configuration
"""""""""""""""""""""""""""
" Set common configuration if non specific is specified
set expandtab           " Use spaces instead of tabs
set smarttab            " Be smart when using tabs ;)
set shiftwidth=2        " 1 tab = 4 spaces
set tabstop=4
set linebreak           " to mouse-copy/past firt do a :set nolinebreak or :set nolbr
set tw=88
set autoindent
set smartindent
set wrap

" Spacific file types configuration
au FileType make setlocal noexpandtab
au FileType python setlocal shiftwidth=4 tabstop=4 softtabstop=0 expandtab
au FileType javascript setlocal shiftwidth=2 tabstop=2 softtabstop=0 expandtab
au FileType json setlocal shiftwidth=4 tabstop=4 softtabstop=0 expandtab
au FileType vue setlocal shiftwidth=2 tabstop=2 softtabstop=0 expandtab
au FileType yaml setlocal shiftwidth=2 tabstop=2 softtabstop=2 expandtab
au FileType html setlocal shiftwidth=2 tabstop=2 softtabstop=2 expandtab

"""""""""""""""""""""""""""""""""""""""""""""""""""
" UTILS CONFIGURATION
"""""""""""""""""""""""""""""""""""""""""""""""""""

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => GUI related
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Set font according to system
if has("mac") || has("macunix")
    set gfn=IBM\ Plex\ Mono:h14,Hack:h14,Source\ Code\ Pro:h15,Menlo:h15
elseif has("win16") || has("win32")
    set gfn=IBM\ Plex\ Mono:h14,Source\ Code\ Pro:h12,Bitstream\ Vera\ Sans\ Mono:h11
elseif has("gui_gtk2")
    set gfn=IBM\ Plex\ Mono\ 14,:Hack\ 14,Source\ Code\ Pro\ 12,Bitstream\ Vera\ Sans\ Mono\ 11
elseif has("linux")
    set gfn=IBM\ Plex\ Mono\ 14,:Hack\ 14,Source\ Code\ Pro\ 12,Bitstream\ Vera\ Sans\ Mono\ 11
elseif has("unix")
    set gfn=Monospace\ 11
endif

" Disable scrollbars (real hackers don't use scrollbars for navigation!)
set guioptions-=r
set guioptions-=R
set guioptions-=l
set guioptions-=L

"""""""""""""""""""""""""""
" => Patterns highlighting
"""""""""""""""""""""""""""
filetype indent on
filetype plugin indent on

" Use the below highlight group when displaying bad whitespace is desired.
highlight BadWhitespace ctermbg=red guibg=red
au BufRead,BufNewFile *.py,*.pyw match BadWhitespace /^\t\+/

" Make trailing whitespace be flagged as bad.
au BufRead,BufNewFile *.py,*.pyw,*.c,*.h,*.R,*.rst match BadWhitespace /\s\+$/

" Automatic line return (for RST files only)
au BufRead,BufNewFile *.txt,*.md,*.rst,*.html set textwidth=88

"""""""""""""""""""""""""""
" => Encryption
"""""""""""""""""""""""""""
" Set vim -x to open encrypted file
set cm=blowfish2  " 2021-October, blowfish2 is recommended and is the default

"""""""""""""""""""""""""""
" => Python files
"""""""""""""""""""""""""""
let python_highlight_all = 1

" Allow easy access to ipdb with ipdb<tab> command for Python files
au FileType python ab <buffer>ipd import ipdb; ipdb.set_trace()

" Display the list of classes and functions of the current file for Python files
au FileType python nnoremap <buffer>,p :!clear; grep -E "def \|class" %<CR>

" Specific python-related files
au BufNewFile,BufRead *.jinja *.jinja2 set syntax=htmljinja
au BufNewFile,BufRead *.mako set ft=mako

" Shortcuts
au FileType python inoremap <buffer>$r return
au FileType python inoremap <buffer>$i import
au FileType python inoremap <buffer>$c class
au FileType python inoremap <buffer>$d def
au FileType python inoremap <buffer>$e # -*- coding: utf-8 -*-

"""""""""""""""""""""""""""
" => Vue files
"""""""""""""""""""""""""""
" Display the list of classes and functions of the current file for Vue files
au FileType vue nnoremap <buffer>,v :!clear; grep -E '^\s+\S+: {\|\() {$\|^export \|<script>\|import ' %<CR>

""""""""""""""""""""""""""""""
"""" => RST files
""""""""""""""""""""""""""""""
au FileType rst nnoremap <buffer><silent>title yyPVr=<CR>yypVr=o
au FileType rst nnoremap <buffer><silent>subtitle yyPVr-<CR>yypVr-o

"""""""""""""""""""""""""""
" => Other files (R, C)
"""""""""""""""""""""""""""
nnoremap ,r :!clear; grep -E "<- function" %
nnoremap ,c :!clear; grep -E "^int\|^void\|^struct\|^float\|^static" %
nnoremap ,odt :echo system(to_odt)

"""""""""""""""""""""""""""
" => rst2odt utils
"""""""""""""""""""""""""""
let bname = expand('%:r')
let fname = expand('%p')
let rst_style = 'rst2odt.py --stylesheet=$HOME/.styles.odt'
let rst2odt = "%s %s > %s.odt && echo '=> odt  Done !'; date && ls -al %s.odt"
let cmd = "%s %s > %s.odt && echo '=> odt  Done !' && date && ls -al %s.odt && echo '=> odt Done !' && date&& ls -la %s.odt"
let to_odt = printf(cmd, rst_style, fname, bname, bname, bname)

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Code execution
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
map <F5> :call CompileRun()<CR>
imap <F5> <Esc>:call CompileRun()<CR>
vmap <F5> <Esc>:call CompileRun()<CR>

func! CompileRun()
    exec "w"
    if &filetype == 'sh'
        exec "!clear; time bash %"
    elseif &filetype == 'python'
        exec "!clear; time python3 %"
    endif
endfunc

""""""""""""""""""""""""""""""""""""""""""""""""
"
" PLUGIN CONFIGURATION
"
" 	First install vim-plug plugin manager with :
" 		$ curl -sfLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
"""""""""""""""""""""""""""""""""""""""""""""""""""
call plug#begin('~/.vim/plugged')

" Simple Fold for Python fold Class, Methods, imports and functions
Plug 'tmhedberg/SimpylFold'    " get python compact folds: zM to fold and zR to unfold all
let g:SimpylFold_fold_blank=1

" Add below Status line and top Tab line
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" Install polyglot for highly comprehensive languages handling
Plug 'sheerun/vim-polyglot'

" Install NerdTree for file browsing
"   :NERDTree :NERDTreeClose, use s to open file in vsplit
"   use ww to mv to next buffer and  \r to refresh nerdtree
Plug 'scrooloose/nerdtree'

" Install vim-fugitive for :Git based commands e.g. Git blame
"  :Gclog -- % load all ancestral commit objects that touched the
"  current file into the quickfix list
Plug 'tpope/vim-fugitive'

" Install color preview for html, css, scss and less
Plug 'gko/vim-coloresque'

" Install javascript formatting plugin
Plug 'maksimr/vim-jsbeautify'

" Install emmet plugin for vim to create tags patterns
Plug 'mattn/emmet-vim'					" h1>lorem3 Ctrl-y, ul.classul>li*3 Ctrl-y, in insert mode, [un-]comment Ctrl-y/
                                        " https://github.com/mattn/emmet-vim/blob/master/TUTORIAL
                                        " https://docs.emmet.io/abbreviations/implicit-names/
" Install commentary plugin
Plug 'tpope/vim-commentary'		" on selection gc will comment, gcgc will uncomment and gcc comments a single line

" Install table creator
  " \tm or :TableModeEnable then |col1|col2| then || on new line
  "  whith \tic \tdc. On aligned data
  "  :Tableize/{separator patern} make it table
Plug 'dhruvasagar/vim-table-mode'

" Install autoclose tags
"   write <h1  then typing > will produce <h1>|</h1> double '>>' will indent
Plug 'alvan/vim-closetag'


" Auto pair characters such as '(', '{', '[', ...
Plug 'jiangmiao/auto-pairs'

" Autocomplete based on LRU
"   type Ctrl-p to fussy search on folder_files, MRU (most recent usedFiles)
"   Ctrl-j, k loop on files and Ctrl-f cycle between modes
Plug 'ctrlpvim/ctrlp.vim'

Plug 'wannesm/wmgraphviz.vim'
" "Pb with master branch FixMe" Plug 'mracos/mermaid.vim'   " mermaid color-syntax
" "To test  " Plug 'leafOfTree/vim-vue-plugin'   " used with .vue files
call plug#end()

" Set custom color column
highlight ColorColumn ctermbg=0

"""""""""""""""""""""""""
" => Airline
"""""""""""""""""""""""""
set laststatus=2    " Force the display of the airline
set noshowmode      " Remove duplicated display of current mode
let g:airline#extensions#tabline#enabled=1 " Set top airline
let g:airline#extensions#tabline#buffer_nr_show=1

"""""""""""""""""""""""""
" => NerdTree
"""""""""""""""""""""""""
let g:NERDTreeWinPos='left'                  " Set tree position and size
let g:NERDTreeWinSize=35
let NERDTreeShowHidden=0
let NERDTreeIgnore=['\.pyc$', '__pycache__'] " Set files to ignore in NERDTree

" Set F10 as default key for NERDTree command
map <F10> <ESC>:NERDTreeToggle<CR>

"""""""""""""""""""""""""
" => JS Beautify
"""""""""""""""""""""""""
let g:config_Beautifier = {
            \ 'js': {
            \ 'indent_size': 2,
            \ 'indent_with_tabs': 1,
            \ 'eol': '\n',
            \ 'preserve_newlines': 1,
            \ 'max_preserve_newlines': 2,
            \ 'keep_array_indentation': 1,
            \ 'break_chained_methods': 1,
            \ 'unindent_chained_methods': 0,
            \ 'indent_scripts': 'normal',
            \ 'brace_style': 'end-expand,preserve-inline',
            \ 'space_before_conditional': 1,
            \ 'unescape_strings': 1,
            \ 'jslint_happy': 1,
            \ 'end_with_newline': 1,
            \ 'wrap_line_length': 80,
            \ 'indent_inner_html': 0,
            \ 'comma_first': 0,
            \ 'e4x': 0,
            \ 'indent_empty_lines': 0,
            \ },
            \ }

" Use Ctrl-f to format Javascript and JSON files
autocmd FileType javascript nnoremap <buffer><silent><C-f> :call JsBeautify()<bar>call JsBeautify()<CR>
autocmd FileType json nnoremap <buffer><silent><C-f> :%!python -m json.tool<CR>

"""""""""""""""""""""""""
" => Auto-Pairs
"""""""""""""""""""""""""
let g:AutoPairsFlyMode = 0
"
" Disable auto-pairing in .vimrc file (mainly because of \" being a commentary)
au FileType vim let b:AutoPairs = {'(':')', '{':'}'}

"""""""""""""""""""""""""
" => spelling check 
"""""""""""""""""""""""""
"  z= to get propositions, zg to mark as good, zb to mark as bad 
"  ]s and [s move cursor to next/previous spelling problem
" set spell spelllang=fr,en
" set spell spelllang=fr
set spellsuggest=10

" Cursor not white when selecting it
colorscheme torte
