import openai
from llama_index.core.prompts import (
    ChatMessage,
    ChatPromptTemplate,
    MessageRole,
)
from llama_index.llms.openai import OpenAI

import os

openai.api_key = os.getenv('API_KEY')
LLM = OpenAI(model="gpt-3.5-turbo", temperature=0)

DEFAULT_SYSTEM_TEMPLATE = """
You are an expert evaluation system for a question answering chatbot.You are given the following information:
- a user query, and
- a generated answerYou may also be given a reference answer to use for reference in your evaluation.Your job is to judge the relevance and correctness of the generated answer.
Output a single score that represents a holistic evaluation.
You must return your response in a line with only the score.
Do not return answers in any other format.
On a separate line provide your reasoning for the score as well.Follow these guidelines for scoring:
- Your score has to be between 0 or 1, where 0 correspond a bad response and 1 a correct answer.
The generated answer has the exact same metrics as the reference answer, \
    but it is not as concise.
Example:
The query is: who is president of USA
The generated answer is: Emmanuel Macron
The reference answer is: Joe Biden

you should answer with the following:
0
the president of USA is not Emmanuel Macron.
"""

USER_MESSAGE_TEMPLATE = """
##query:
{query}

##generated answer:
{generated_answer}

##reference answer:
{reference_answer}
"""


CHAT_PROMPT_TEMPLATE = ChatPromptTemplate(
    message_templates=[
        ChatMessage(role=MessageRole.SYSTEM, content=DEFAULT_SYSTEM_TEMPLATE),
        ChatMessage(role=MessageRole.USER, content=USER_MESSAGE_TEMPLATE),
    ])

def evaluate(query, response, reference) -> bool:
    llm_answer = LLM.predict(CHAT_PROMPT_TEMPLATE, query=query,
                             generated_answer=response,
                             reference_answer=reference)
    score = int(llm_answer.split('\n')[0])
    return score == 1
