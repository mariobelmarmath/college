from llama_index.core import (
    StorageContext,
    load_index_from_storage,
    VectorStoreIndex,
    QueryBundle,
    Settings,
    get_response_synthesizer,
    SimpleDirectoryReader,)
from llama_index.core.node_parser import SentenceSplitter
from llama_index.readers.web import SimpleWebPageReader
from pathlib import Path
import openai
import sys
import os

Settings.chunk_overlap = 20
Settings.chunk_size = 128
openai.api_key = os.getenv("API_KEY")

VECTORDB = Path('/home/mario/data/anime/')

def add_url(url, vectordb):
    storage = Path(vectordb)
    assert storage.is_dir()

    print("Copy old index...")
    storage_context = StorageContext.from_defaults(persist_dir=storage)
    index = load_index_from_storage(storage_context)

    print("Loading new documents...")
    new_documents = SimpleWebPageReader(html_to_text=True).load_data([url])

    print("Parsing web page into nodes...")
    parser = SentenceSplitter()
    new_nodes = parser.get_nodes_from_documents(new_documents)
    old_nodes = [index.docstore.docs[key] for key in index.docstore.docs.keys()]
    assert type(new_nodes) == type(old_nodes)

    print("Adding new nodes to the existing index...")
    index = VectorStoreIndex(show_progress=True,
                             nodes=new_nodes+old_nodes)
    index.storage_context.persist(persist_dir=storage)
    print(f"{url} ready to index")

    # documents = SimpleWebPageReader(html_to_text=True).load_data([url])
    # index = VectorStoreIndex.from_documents(documents)
    # print(f"{url} ready to index")
    # index.storage_context.persist(persist_dir=storage)

if __name__ == "__main__":
    add_url(sys.argv[1], VECTORDB)
