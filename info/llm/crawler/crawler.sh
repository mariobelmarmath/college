#!/bin/bash

files=$(find ./data/onepiecet1 -type f)
for ffile in $files
do
  if [[ $ffile == *.png ]] || [[ $ffile == *Template* ]] || [[ $ffile == *Category* ]] || [[ $ffile != ./data/onepiecet1/wiki* ]]
  then
    echo "Remove $ffile"
    rm $ffile
  else
    echo "$ffile move to $ffile.html"
    mv $ffile $ffile.html
    rm $ffile
  fi
done

