#!/bin/bash
files=$(find ./data/onepiecet1/wiki -type f)
a=0
for file in $files
do
  if [[ $a > 5 ]]
  then
    break
  else
    python scrapper.py http://localhost/$file
    ((a++))
  fi
done
