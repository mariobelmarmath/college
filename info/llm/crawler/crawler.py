from llama_index.readers.web import SimpleWebPageReader
from llama_index.core import (
    StorageContext,
    load_index_from_storage,
    VectorStoreIndex,
    QueryBundle,
    Settings,
    get_response_synthesizer,
    SimpleDirectoryReader,)
from llama_index.readers.wikipedia import WikipediaReader
from llama_index.core.query_engine import RetrieverQueryEngine
from llama_index.core.postprocessor import SimilarityPostprocessor
from llama_index.core.node_parser import SentenceSplitter
from llama_index.core.retrievers import VectorIndexRetriever
from llama_index.postprocessor.rankgpt_rerank import RankGPTRerank
from llama_index.llms.openai import OpenAI
from pathlib import Path
import openai
import click
import os

Settings.chunk_size = 128
Settings.chunk_overlap = 20
openai.api_key = os.getenv("API_KEY")

question = """Who kidnapped Koby during the Egghead Arc ?"""
query_bundle = QueryBundle(question)

# documents = SimpleWebPageReader(html_to_text=True).load_data(
#     ["http://127.0.0.1/wiki/Marine.html"])
# index = VectorStoreIndex.from_documents(documents)

storage_context = StorageContext.from_defaults(persist_dir=Path('/home/mario/data/anime/'))
index = load_index_from_storage(storage_context)

# ===========simple
print("use simple method ...")
query_engine_simple = index.as_query_engine()
response_simple = query_engine_simple.query(query_bundle)

#. ==================answer with top_k=10
print("Use top_k=10 method...")
retriever = VectorIndexRetriever(
    index=index,
    similiraty_top_k=10,
)
print("... Generate response...")
query_engine_k = RetrieverQueryEngine(
    retriever=retriever,
    response_synthesizer=get_response_synthesizer(),
    node_postprocessors=[SimilarityPostprocessor(similarity_cutoff=0.7)],
)
response_k = query_engine_k.query(query_bundle)

## ===================== Reranker
print("Use reranker method...")
reranker = RankGPTRerank(
    llm=OpenAI(
        model="gpt-3.5-turbo",
        temperature="0.0",
    ),
    top_n=4,
    verbose=True,
)

print("... Generate response...")
query_engine_reranker = RetrieverQueryEngine(
    retriever=retriever,
    response_synthesizer=get_response_synthesizer(),
    node_postprocessors=[reranker],
)
response_reranker = query_engine_reranker.query(query_bundle)

print(f"""
With reranker:
{response_reranker.response}
With k:
{response_k.response}
With simple:
{response_simple.response}
""")
