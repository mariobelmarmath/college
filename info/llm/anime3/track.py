import pandas as pd
from datetime import datetime
from pathlib import Path
import sys
import matplotlib.pyplot as plt


FILE = ''

def order_openai_csv(FILE):
    assert FILE.suffix in ['.csv']

    df = pd.read_csv(FILE)
    df = df.drop(['organization_id', 'organization_name', 'api_key_id','api_key_name',
                  'api_key_redacted', 'api_key_type', 'project_id', 'project_name',
                  'usage_type', 'user'], axis =1 )
    df.timestamp = [str(datetime.fromtimestamp(date).date()) for date in df.timestamp]
    df.index = df.timestamp
    return df


if __name__ == "__main__":
    FILE = Path(sys.argv[1])
    df = order_openai_csv(FILE)
    print(df)
    df.plot(grid=True, kind='bar', subplots=True)
    plt.show()
