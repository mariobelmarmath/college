from llama_index.core import (
    VectorStoreIndex,
    SimpleDirectoryReader,
    StorageContext,
    Settings,
    load_index_from_storage)
from llama_index.core.callbacks import CallbackManager, TokenCountingHandler
from llama_index.llms.openai import OpenAI
from llama_index.experimental.query_engine import PandasQueryEngine
from pathlib import Path
import constants
import tiktoken
import openai
import sys
from track import order_openai_csv

openai.api_key = constants.API_KEY

PERSIST_DIR = "./storage"
persist_dir = Path(PERSIST_DIR)
data = Path('./data')
df = order_openai_csv(Path("./csv_file/activity-2024-05-01-2024-06-01.csv"))

assert data.is_dir() and persist_dir.is_dir()

token_counter = TokenCountingHandler(
    tokenizer=tiktoken.encoding_for_model("gpt-3.5-turbo").encode)
Settings.llm = OpenAI(model="gpt-3.5-turbo", temperature=0.2)
Settings.callback_manager = CallbackManager([token_counter])

# documents = SimpleDirectoryReader(data).load_data()
# index = VectorStoreIndex.from_documents(documents, show_progress=True)
# index.storage_context.persist(persist_dir=persist_dir)

if not persist_dir.is_dir():
    documents = SimpleDirectoryReader(data).load_data()
    index = VectorStoreIndex.from_documents(documents, show_progress=True)
    index.storage_context.persist(persist_dir=persist_dir)
else:
    storage_context = StorageContext.from_defaults(persist_dir=persist_dir)
    index = load_index_from_storage(storage_context)

query_engine = index.as_query_engine()
# query_engine = PandasQueryEngine(df=df, verbose=True)

if len(sys.argv) != 2:
    question = """How much API requests to Text-embedding-ada-002-v2 did I do ?"""
    question = "Ai-je prévu quelque chose en décembre ?"
    question = "Qu'ai-je prévu de faire en mai 2024, propose des choses en juin."
    question = """How much API requests to GPT-3.5 did I do ?"""
    question = """Combien de requetes à GPT3.5 turbo ai-je fait le 22 mai 204 ?"""
else:
    question = sys.argv[1]

response = query_engine.query(question)
print(response)
print(token_counter.total_embedding_token_count)

