from setuptools import find_packages, setup


setup(
    name="rag_pkg",
    version='0.42',
    packages=find_packages(),
    include_package_data=True,
    # py_modules=['rag_pkg.ragcli'],
    # package_dir={'': 'rag_pkg'},
    package=['rag_pkg'],
    install_requires=['Click',
                      "llama_index.core",
                      "pathlib",
                      "openai",
                      "llama-index-readers-wikipedia",
                      "llama-index-postprocessor-rankgpt-rerank",
                      "llama-index-llms-openai",
                      "wikipedia",
                      "click",
                      ],
    entry_points={"console_scripts": ["rag_cli = rag_pkg.ragcli:cli"]},
    # entry_points={"console_scripts": ["rag_cli = rag_pkg.ragcli:cli"]},
)

