from llama_index.core import (
    StorageContext,
    load_index_from_storage,
    VectorStoreIndex,
    QueryBundle,
    Settings,
    get_response_synthesizer,
    SimpleDirectoryReader,)
from llama_index.readers.wikipedia import WikipediaReader
from llama_index.core.query_engine import RetrieverQueryEngine
from llama_index.core.postprocessor import SimilarityPostprocessor
from llama_index.core.node_parser import SentenceSplitter
from llama_index.core.retrievers import VectorIndexRetriever
from llama_index.postprocessor.rankgpt_rerank import RankGPTRerank
from llama_index.llms.openai import OpenAI
from pathlib import Path
import openai
import click
import os

openai.api_key = os.getenv("API_KEY")

Settings.chunk_size = 128
Settings.chunk_overlap = 20

@click.group()
def cli():
    pass


@click.command()
@click.argument('documents_directory', nargs=1)
@click.argument('vectordb', nargs=1)
def initdb(documents_directory, vectordb):
    data = Path(documents_directory)
    storage = Path(vectordb)
    assert data.is_dir() and storage.is_dir()
    files = [x.name for x in data.iterdir()]
    for idx, file in enumerate(files):
        print(f"{idx:2}: {file} ready to index")
    documents = SimpleDirectoryReader(data).load_data()
    index = VectorStoreIndex.from_documents(documents)
    index.storage_context.persist(persist_dir=storage)
    click.echo(f'Your data from {documents_directory}, is now totally store in {vectordb}.')


@click.command()
@click.argument('documents_files', nargs=-1)
@click.argument('vectordb', nargs=1)
def updatedb(documents_files, vectordb):
    storage = Path(vectordb)
    assert storage.is_dir()
    for file in documents_files:
        file_path = Path(file)
        assert file_path.is_file()
        file_path = Path(documents_files)
        print(file_path, storage)

        print("Copy old index...")
        storage_context = StorageContext.from_defaults(persist_dir=storage)
        index = load_index_from_storage(storage_context)

        print("Loading new documents...")
        reader = SimpleDirectoryReader(input_files=[file_path])
        new_documents = reader.load_data()

        print("Parsing new documents into nodes...")
        parser = SentenceSplitter()
        new_nodes = parser.get_nodes_from_documents(new_documents)
        old_nodes = [index.docstore.docs[key] for key in index.docstore.docs.keys()]
        assert type(new_nodes) == type(old_nodes)

        print("Adding new nodes to the existing index...")
        index = VectorStoreIndex(show_progress=True,
                                 nodes=new_nodes+old_nodes)
        index.storage_context.persist(persist_dir=storage)
        print(f"{file_path} ready to index")


@click.command()
@click.argument('url_wikipedia', nargs=-1)
@click.argument('vectordb', nargs=1)
def add_wikipedia(url_wikipedia, vectordb):
    storage = Path(vectordb)
    reader = WikipediaReader()
    print(f"loading wikipedia pages: {url_wikipedia}")
    print(url_wikipedia)
    documents = reader.load_data(pages=url_wikipedia, lang_prefix='fr')
    index = VectorStoreIndex.from_documents(documents)
    index.storage_context.persist(persist_dir=storage)
    click.echo(f'Your data from {url_wikipedia}, is now totally store in {vectordb}.')


@click.command()
@click.argument('question', nargs=1)
@click.argument('vectordb', nargs=1)
@click.option('--chat_gpt', '-c', is_flag=True, help='Wil show chatgpt answer.')
def ask(question, vectordb, chat_gpt):
    storage = Path(vectordb)
    storage_context = StorageContext.from_defaults(persist_dir=storage)
    index = load_index_from_storage(storage_context)

    #. answer with top_k=10
    print("Use top_k=10 method...")
    retriever = VectorIndexRetriever(
        index=index,
        similiraty_top_k=10,
    )
    print("... Generate response...")
    query_engine = RetrieverQueryEngine(
        retriever=retriever,
        response_synthesizer=get_response_synthesizer(),
        node_postprocessors=[SimilarityPostprocessor(similarity_cutoff=0.7)],
    )
    response = query_engine.query(question)

    ## Reranker
    print("Use reranker method...")
    query_bundle = QueryBundle(question)
    reranker = RankGPTRerank(
        llm=OpenAI(
            model="gpt-3.5-turbo",
            temperature="0.0",
        ),
        top_n=4,
        verbose=True,
    )

    print("... Generate response...")
    query_engine_reranker = RetrieverQueryEngine(
        retriever=retriever,
        response_synthesizer=get_response_synthesizer(),
        node_postprocessors=[reranker],
    )
    response_reranker = query_engine_reranker.query(question)

    formatter = lambda x: [s.get_content() for s in x.source_nodes]
    print(f"""Question:
          {question}
          ======""")
    print(f"""RAG's Answer with top_k=10:
          {response}
          {formatter(response)}
          ======""")
    print(f"""RAG's Answer with reranker:
          {response_reranker}
          {formatter(response_reranker)}
          ======""")
    if chat_gpt:
        response_chatgpt = openai.chat.completions.create(
            model="gpt-3.5-turbo",
            messages=[ {"role": "user", "content": question}],
            max_tokens=150,
        )
        print(f"""Chatgpt's Answer:
              {response_chatgpt.choices[0].message.content }
              ======""")


cli.add_command(initdb)
cli.add_command(updatedb)
cli.add_command(ask)
cli.add_command(add_wikipedia)

if __name__ == "__main__":
    cli()
