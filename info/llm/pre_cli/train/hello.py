import click

@click.command()
@click.option('--verbose', '-v', is_flag=True, help='Wil print verbose messages.')
@click.option('--name', '-n', multiple=True, default=[], help='Who are you ?')
@click.password_option()
@click.argument('city')
def cli(verbose, name, password, city):
    """Programm to learn cli"""
    if password == 'a':
        if verbose:
            click.echo("we are in the verbose mode.")
        if name:
            for n in name:
                click.echo(f"Hello {n}")
                # click.echo("Hello {0}".format(n))
        click.echo(f'We received {password} as password.')
        click.echo(f'You are in {city}')
    else:
        click.echo('You password is not correct, please retry')
