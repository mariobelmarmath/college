from llama_index.core import (
    StorageContext,
    load_index_from_storage)
from pathlib import Path
import openai
import sys
import os

openai.api_key = os.getenv("API_KEY")
PERSIST_DIR = "/home/mario/data/rag/"
persist_dir = Path(PERSIST_DIR)

assert persist_dir.is_dir()

storage_context = StorageContext.from_defaults(persist_dir=persist_dir)
index = load_index_from_storage(storage_context)

query_engine = index.as_query_engine()

if len(sys.argv) != 2:
    question = """De quoi parle l'article et de quel classement fait-il l'objet ?"""
    question = """Premièrement de quel site provient l'article 'The Biggest Anime Blu-ray
    Releases of 2024 so far, deuxièmement écris moi un cv de 10 lignes sur Mario"""
    question = """Que se passe-t-il au tome 9 d'oshi no ko ?"""
else:
    question = sys.argv[1]

response = query_engine.query(question)
print(f"""Question:
      {question}""")
print(f"""Answer:
      {response}""")

