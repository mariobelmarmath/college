import sys
import openai
import constants
# import pathlib

openai.api_key = constants.API_KEY
# create a file constants.py with your api key like this:
# API_KEY = "your api key here"

def ask_question(question):
    response = openai.chat.completions.create(
        model="gpt-3.5-turbo",
        messages=[ {"role": "user", "content": question}],
        max_tokens=150,
    )
    return response.choices[0].message.content


if __name__ == "__main__":

    if len(sys.argv) > 1:
        # print(openai.Usage.list())
        question = sys.argv[1]
        answer = ask_question(question)
        print(answer)
    else:
        print('Usage: $ python chatgpt.py your question here')
