q1 = """
What is One Piece world seeker, and from wich manga come from ?
"""

r1 = """
One Piece: World Seeker is an open world action-adventure video game developed on the
manga One piece.

"""

q2 = """
Give me a description of Merlin from the game OnePiece world seeker
"""

r2 = """
Merlin is an average-sized man with fair skin, a thin nose, and spiky brown hair he
keeps underneath his Marine cap.
"""

q3 = """
give me a short Summary of the Episode 989 of One piece
"""

r3 = """
Big Mom battles with Chopper's tank. Denjiro's southern forces advance, restraining
Sasaki before splitting up. Meanwhile, Ulti encounters Luffy.
"""

q4 = """
Does Brook and Arlong Appear in the Epiosde 538 of One piece
"""

r4 = """
Brook appear in the Episode 538 but not Arlong.
"""

q5 = """
what is the shape of Festa Circle of One Piece
"""

r5 = """
Festa Circle is a small, circular island.
"""

q6 = """
In which Legend does Festa Circle appeared ?
"""

r6 = """
Festa Circle appears in Legend of the Rainbow Island.
"""

q7 = """
Why did Merling became defensive and retaliatory ?
"""

r7 = """
He became defensive and retaliatory because Luffy and Jeanne questionned his position in
the marines.
"""

q8 = """
Tell me in five short sentences what is added scenes in the episode 538
"""

r8 = """
Zoro fights with some of Hody's men
Usopp throws a bubly Coral to Zoro, but he misses the catch.
An Ammo Knight informs about the trouble before he was killed
The princes were at Gyoverly Hills and Ikaros makes his move after they have left.
A younger Shirahoshi is shown even before the flashbacks start.
"""

q9 = """
In the Festa circle, where does Torta live and where occured the cooking contest ?
"""

r9 = """
Torta live with his fatcher in Pepe Village and the cooking contest occured in Festa
Castle
"""

q10 = """
Who won between Merlin and Luffy, with which techniques
"""

r10 = """
Luffy won with his Gear 3.
"""

q11 = """
Class the following characters by order of appearance in the episode 538:
'Hody Jones', 'Akainu', 'Hoe'
"""

r11 = """
The first character who appeared was Hody Jones, then 'Hoe' and finally 'Akainu'
"""

q12 = """
When does the easygoing nature of Marianne show up ?
"""

r12 = """
First when she set up a picnic during a battle, and when she was too lazy to walk to
pick a cup of tee near her.
"""

q13 = """
Make me a short summary of what happen during the 'Operation: Meet Baroque Works'.
"""

r13 = """
Marianne, Gem, and Mikita hid out on Little Garden in the wax house. Upon finding out that
Baroque Works had been exposed after Crocodile's defeat in Arabasta, they flew from the
island on top of a pterodactyl with the help of a Colors Trap in order to go visit their
comrades in jail.
"""

q14 = """
tell if the following affirmation is true or not and justify:
Marianne can modify personality with the colors and can create explosive bomb.
"""

r14 = """
False
She can effectively modify the personalities but cant create explosive bomb
"""
