from llama_index.core import (
    StorageContext,
    load_index_from_storage,
    VectorStoreIndex,
    QueryBundle,
    Settings,
    get_response_synthesizer,
    SimpleDirectoryReader,)
from llama_index.readers.wikipedia import WikipediaReader
from llama_index.core.query_engine import RetrieverQueryEngine
from llama_index.core.postprocessor import SimilarityPostprocessor
from llama_index.core.node_parser import SentenceSplitter
from llama_index.core.retrievers import VectorIndexRetriever
from llama_index.postprocessor.rankgpt_rerank import RankGPTRerank
from llama_index.llms.openai import OpenAI
from pathlib import Path
import openai
import click
import os

openai.api_key = os.getenv("API_KEY")

Settings.chunk_size = 128
Settings.chunk_overlap = 20

def llm_reranker(question, vectordb="/home/mario/data/anime/"):
    storage = Path(vectordb)
    storage_context = StorageContext.from_defaults(persist_dir=storage)
    index = load_index_from_storage(storage_context)

    query_bundle = QueryBundle(question)
    reranker = RankGPTRerank(
        llm=OpenAI(
            model="gpt-3.5-turbo",
            temperature="0.0",
        ),
        top_n=10,
        verbose=True,
    )
    retriever = VectorIndexRetriever(
        index=index,
        similiraty_top_k=10,
    )

    query_engine_reranker = RetrieverQueryEngine(
        retriever=retriever,
        response_synthesizer=get_response_synthesizer(),
        node_postprocessors=[reranker],
    )
    response_reranker = query_engine_reranker.query(query_bundle)
    return dict(response=response_reranker, node=[s.get_content() for s in
                                                  response_reranker.source_nodes])

