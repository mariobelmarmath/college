from evaluator import evaluate
from question_set import *
import openai
from llama_index.llms.openai import OpenAI
from reranker import llm_reranker
import os

openai.api_key = os.getenv('API_KEY')
LLM = OpenAI(model="gpt-3.5-turbo", temperature=0)


def test():
    assert evaluate(q1,  llm_reranker(q1),  r1)
    assert evaluate(q2,  llm_reranker(q2),  r2)
    assert evaluate(q3,  llm_reranker(q3),  r3)
    assert evaluate(q4,  llm_reranker(q4),  r4)
    assert evaluate(q5,  llm_reranker(q5),  r5)
    assert evaluate(q6,  llm_reranker(q6),  r6)
    assert evaluate(q7,  llm_reranker(q7),  r7)
    assert evaluate(q8,  llm_reranker(q8),  r8)
    assert evaluate(q9,  llm_reranker(q9),  r9)
    assert evaluate(q10, llm_reranker(q10), r10)
    assert evaluate(q11, llm_reranker(q11), r11)
    assert evaluate(q12, llm_reranker(q12), r12)
    assert evaluate(q13, llm_reranker(q13), r13)
    assert evaluate(q14, llm_reranker(q14), r14)
    # assert evaluate(q15, llm_reranker(q15), r15)

if __name__ == "__main__":
    test()
