[Wiki Oshi no Ko FR](//oshinoko.fandom.com/fr)

  * Explorer

    * [ Page d'accueil ](https://oshinoko.fandom.com/fr/wiki/Wiki_Oshi_no_Ko_FR)
    * [ Discuter ](/fr/f)
    * [ Toutes les pages ](https://oshinoko.fandom.com/fr/wiki/Sp%C3%A9cial:AllPages)
    * [ Communauté ](https://oshinoko.fandom.com/fr/wiki/Sp%C3%A9cial:Community)
    * [ Cartes interactives ](https://oshinoko.fandom.com/fr/wiki/Sp%C3%A9cial:AllMaps)
    * [ Billets de blog récents ](/fr/Blog:Billets_récents)

  * Anime

    * [ Oshi no Ko (Anime) ](https://oshinoko.fandom.com/fr/wiki/Oshi_no_Ko_\(Anime\))
    * [ Opening ](https://oshinoko.fandom.com/fr/wiki/Idol)
    * [ Ending ](https://oshinoko.fandom.com/fr/wiki/Mephisto)
    * [ Liste des épisodes ](https://oshinoko.fandom.com/fr/wiki/Liste_des_%C3%A9pisodes)

  * Manga

    * [ Oshi no Ko (Manga) ](https://oshinoko.fandom.com/fr/wiki/Oshi_no_Ko_\(Manga\))
    * [ Liste des chapitres ](https://oshinoko.fandom.com/fr/wiki/Liste_des_chapitres)
    * [ Liste des Arcs ](https://oshinoko.fandom.com/fr/wiki/Liste_des_Arcs)
    * [ Auteurs ](https://oshinoko.fandom.com/fr/wiki/Auteurs)

      * [ Mengo Yokoyari ](https://oshinoko.fandom.com/fr/wiki/Mengo_Yokoyari)
      * [ Aka Akasaka ](https://oshinoko.fandom.com/fr/wiki/Aka_Akasaka)

  * Personnages

    * [ Aquamarine Hoshino ](https://oshinoko.fandom.com/fr/wiki/Aquamarine_Hoshino)
    * [ Ai Hoshino ](https://oshinoko.fandom.com/fr/wiki/Ai_Hoshino)
    * [ Ruby Hoshino ](https://oshinoko.fandom.com/fr/wiki/Ruby_Hoshino)
    * [ Kana Arima ](https://oshinoko.fandom.com/fr/wiki/Kana_Arima)
    * [ Akane Kurokawa ](https://oshinoko.fandom.com/fr/wiki/Akane_Kurokawa)
    * [ Memcho ](https://oshinoko.fandom.com/fr/wiki/Memcho)

[ ](/fr/wiki/Sp%C3%A9cial:Search "Rechercher") [ ](/fr/f "Discuter")

[ FANDOM ](//www.fandom.com/explore-fr?uselang=fr) [
](/fr/wiki/Sp%C3%A9cial:Recherche "Rechercher")

[ Jeux vidéo  ](//www.fandom.com/explore-fr?uselang=fr#Jeux_vidéo) [ Cinéma
](//www.fandom.com/explore-fr?uselang=fr#Cinéma) [ Télévision
](//www.fandom.com/explore-fr?uselang=fr#Télévision)

Wikis

  * [ Explorer les wikis ](//www.fandom.com/explore-fr?uselang=fe)
  * [ Centre des communautés ](http://communaute.fandom.com/wiki/Centre_des_communaut%C3%A9s)

[ Créer un wiki
](//createnewwiki.fandom.com/Special:CreateNewWiki?uselang=fr)

Vous n'avez pas de compte ?

[
S'inscrire](https://auth.fandom.com/register?source=mw&uselang=fr&redirect=https%3A%2F%2Foshinoko.fandom.com%2Ffr%2Fwiki%2FOshi_no_Ko_%28Manga%29&selang=fr)

* * *

[ Se
connecter](https://auth.fandom.com/signin?source=mw&uselang=fr&redirect=https%3A%2F%2Foshinoko.fandom.com%2Ffr%2Fwiki%2FOshi_no_Ko_%28Manga%29&selang=fr)

Advertisement

[ Se connecter
](https://auth.fandom.com/signin?source=mw&uselang=fr&redirect=https%3A%2F%2Foshinoko.fandom.com%2Ffr%2Fwiki%2FOshi_no_Ko_%28Manga%29&selang=fr)
[ S'inscrire
](https://auth.fandom.com/register?source=mw&uselang=fr&redirect=https%3A%2F%2Foshinoko.fandom.com%2Ffr%2Fwiki%2FOshi_no_Ko_%28Manga%29&selang=fr)

[ ![Wiki Oshi no Ko FR](https://static.wikia.nocookie.net/oshi-no-ko-
fr/images/e/e6/Site-logo.png/revision/latest?cb=20230521074623&path-prefix=fr)
](//oshinoko.fandom.com/fr)

[ Wiki Oshi no Ko FR ](//oshinoko.fandom.com/fr)

95

pages

[ ](/fr/wiki/Sp%C3%A9cial:Search "Rechercher") [ ](/fr/f "Discuter")

  * Explorer

    * [ Page d'accueil ](https://oshinoko.fandom.com/fr/wiki/Wiki_Oshi_no_Ko_FR)
    * [ Discuter ](/fr/f)
    * [ Toutes les pages ](https://oshinoko.fandom.com/fr/wiki/Sp%C3%A9cial:AllPages)
    * [ Communauté ](https://oshinoko.fandom.com/fr/wiki/Sp%C3%A9cial:Community)
    * [ Cartes interactives ](https://oshinoko.fandom.com/fr/wiki/Sp%C3%A9cial:AllMaps)
    * [ Billets de blog récents ](/fr/Blog:Billets_récents)

  * Anime

    * [ Oshi no Ko (Anime) ](https://oshinoko.fandom.com/fr/wiki/Oshi_no_Ko_\(Anime\))
    * [ Opening ](https://oshinoko.fandom.com/fr/wiki/Idol)
    * [ Ending ](https://oshinoko.fandom.com/fr/wiki/Mephisto)
    * [ Liste des épisodes ](https://oshinoko.fandom.com/fr/wiki/Liste_des_%C3%A9pisodes)

  * Manga

    * [ Oshi no Ko (Manga) ](https://oshinoko.fandom.com/fr/wiki/Oshi_no_Ko_\(Manga\))
    * [ Liste des chapitres ](https://oshinoko.fandom.com/fr/wiki/Liste_des_chapitres)
    * [ Liste des Arcs ](https://oshinoko.fandom.com/fr/wiki/Liste_des_Arcs)
    * [ Auteurs ](https://oshinoko.fandom.com/fr/wiki/Auteurs)

      * [ Mengo Yokoyari ](https://oshinoko.fandom.com/fr/wiki/Mengo_Yokoyari)
      * [ Aka Akasaka ](https://oshinoko.fandom.com/fr/wiki/Aka_Akasaka)

  * Personnages

    * [ Aquamarine Hoshino ](https://oshinoko.fandom.com/fr/wiki/Aquamarine_Hoshino)
    * [ Ai Hoshino ](https://oshinoko.fandom.com/fr/wiki/Ai_Hoshino)
    * [ Ruby Hoshino ](https://oshinoko.fandom.com/fr/wiki/Ruby_Hoshino)
    * [ Kana Arima ](https://oshinoko.fandom.com/fr/wiki/Kana_Arima)
    * [ Akane Kurokawa ](https://oshinoko.fandom.com/fr/wiki/Akane_Kurokawa)
    * [ Memcho ](https://oshinoko.fandom.com/fr/wiki/Memcho)

[ ](/fr/wiki/Oshi_no_Ko_\(Manga\)?veaction=edit)

dans : [Manga](/fr/wiki/Cat%C3%A9gorie:Manga "Catégorie:Manga"),
[Tome](/fr/wiki/Cat%C3%A9gorie:Tome "Catégorie:Tome")

#  Oshi no Ko (Manga)

[ Modifier ](/fr/wiki/Oshi_no_Ko_\(Manga\)?veaction=edit)

  * [ Modifier le wikicode ](/fr/wiki/Oshi_no_Ko_\(Manga\)?action=edit)
  * [ Voir l’historique ](/fr/wiki/Oshi_no_Ko_\(Manga\)?action=history)
  * [ Discussion (0) ](/fr/wiki/Discussion:Oshi_no_Ko_\(Manga\)?action=edit&redlink=1)

[![Ai Hoshino - C’est un secret](https://static.wikia.nocookie.net/oshi-no-ko-
fr/images/a/a4/Ai_Hoshino_-
_C%E2%80%99est_un_secret.jpeg/revision/latest/scale-to-width-
down/200?cb=20230521142147&path-
prefix=fr)](https://static.wikia.nocookie.net/oshi-no-ko-
fr/images/a/a4/Ai_Hoshino_-
_C%E2%80%99est_un_secret.jpeg/revision/latest?cb=20230521142147&path-
prefix=fr) |  "C’est un secret~" Cet article contient des **SPOILERS**
provenant des derniers chapitres. Attention en lisant !  
---|---  
  
**Oshi no Ko** (japonais: **【推しの子】** _Oshi no Ko_ , _litt._ "Les enfants de ma
star" _ou_ "Ma star") est un manga écrit par [Aka
Akasaka](/fr/wiki/Aka_Akasaka "Aka Akasaka") et dessiné par [Mengo
Yokoyari](/fr/wiki/Mengo_Yokoyari "Mengo Yokoyari"). Le manga est publié dans
le _Weekly Young Jump_ (depuis le n °21) à partir du 23 avril 2020, et est
actuellement en cours.

Le 13 janvier 2022, l’éditeur _Kurokawa_ a sorti les deux premiers tomes en
France, et continue à sortir les tomes depuis.

## Sommaire

  * 1 Synopsis
  * 2 Intrigue
    * 2.1 **Prologue :**
    * 2.2 **Le Monde du Spectacle :**
    * 2.3 **Téléréalité de Rencontre :**
    * 2.4 **Première Scène :**
    * 2.5 **Pièce de théâtre adaptée d’un manga :**
    * 2.6 **Vie Privée :**
    * 2.7 **Clé de voûte :**
    * 2.8 **Scandale :**
    * 2.9 **Film :**
  * 3 Personnages
  * 4 Tomes
  * 5 Matériel Promotionnel
    * 5.1 **Icon Present :**

## Synopsis[[](/fr/wiki/Oshi_no_Ko_\(Manga\)?veaction=edit&section=1 "Modifier
la section : Synopsis")]

_"Dans le monde du spectacle, le mensonge est une arme !"_

Le docteur Gorô est obstétricien dans un hôpital de campagne. Il est loin du
monde de paillettes dans lequel évolue Aï Hoshino, une chanteuse au succès
grandissant dont il est "un fan absolu". Ces deux-là vont se rencontrer dans
des circonstances exceptionnelles qui changeront leur vie à jamais !

## Intrigue[[](/fr/wiki/Oshi_no_Ko_\(Manga\)?veaction=edit&section=2 "Modifier
la section : Intrigue")]

### **[Prologue :](/fr/wiki/Prologue
"Prologue")**[[](/fr/wiki/Oshi_no_Ko_\(Manga\)?veaction=edit&section=3
"Modifier la section : Prologue :")]

**Prologue** (japonais: **プロローグ** _Purorōgu_ ) est le premier arc du manga. Il
couvre l’entièreté du [tome 1](/fr/wiki/Tome_1 "Tome 1") et raconte l'histoire
de la façon dont [Gorô](/fr/wiki/Gor%C3%B4_Amamiya "Gorô Amamiya") s'est
réincarné en le fils de son _oshi_ , [Ai Hoshino](/fr/wiki/Ai_Hoshino "Ai
Hoshino"), et son enfance en tant qu'[Aquamarine
Hoshino](/fr/wiki/Aquamarine_Hoshino "Aquamarine Hoshino").

### **[Le Monde du Spectacle :](/fr/wiki/Le_Monde_du_Spectacle "Le Monde du
Spectacle")**[[](/fr/wiki/Oshi_no_Ko_\(Manga\)?veaction=edit&section=4
"Modifier la section : Le Monde du Spectacle :")]

**Le Monde du Spectacle** (japonais: **芸能界** _Geinō-kai_ , _litt_.
"L’Industrie du Divertissement") est le deuxième arc du manga. Il couvre
l'ensemble du [tome 2](/fr/wiki/Tome_2 "Tome 2") et raconte l'histoire des
jumeaux entrant au lycée et travaillant lentement dans le monde du showbiz
pour leurs raison personnelles.

### **[Téléréalité de Rencontre
:](/fr/wiki/T%C3%A9l%C3%A9r%C3%A9alit%C3%A9_de_Rencontre "Téléréalité de
Rencontre")**[[](/fr/wiki/Oshi_no_Ko_\(Manga\)?veaction=edit&section=5
"Modifier la section : Téléréalité de Rencontre :")]

**Téléréalité de Rencontre** (japonais: **恋愛リアリティショー** _Ren'ai Riariti Shō_ )
est le troisième arc du manga. Il couvre l'ensemble [tome 3](/fr/wiki/Tome_3
"Tome 3") et les deux premiers chapitres du [tome 4](/fr/wiki/Tome_4 "Tome
4"). Il raconte l'histoire d'[Aqua](/fr/wiki/Aquamarine_Hoshino "Aquamarine
Hoshino") entrant dans l'émission de télé-réalité "Ici Commence l’Amour
Déterminé" (également connu sous le nom de _l’Amour Déter._ ) afin d'obtenir
des informations sur [Ai](/fr/wiki/Ai_Hoshino "Ai Hoshino") auprès du
producteur Kaburagi.

### **[Première Scène :](/fr/wiki/Premi%C3%A8re_Sc%C3%A8ne "Première
Scène")**[[](/fr/wiki/Oshi_no_Ko_\(Manga\)?veaction=edit&section=6 "Modifier
la section : Première Scène :")]

**Première Scène** (japonais: **ファーストステージ** _Fāsutosutēji_ , _litt_. "Première
étape’) est le quatrième arc du manga. Il couvre le reste du [tome
4](/fr/wiki/Tome_4 "Tome 4") et montre l’introduction de
[Memcho](/fr/wiki/Memcho "Memcho") dans le nouveau groupe d'idoles
[B-Komachi](/fr/wiki/B-Komachi "B-Komachi"), qui était l’ancien groupe d'[Ai
Hoshino](/fr/wiki/Ai_Hoshino "Ai Hoshino"), et leur apparition au _Japan Idol
Festival_.

### **Pièce de théâtre adaptée d’un manga
:**[[](/fr/wiki/Oshi_no_Ko_\(Manga\)?veaction=edit&section=7 "Modifier la
section : Pièce de théâtre adaptée d’un manga :")]

**Pièce de théâtre adaptée d’un manga** (japonais: **2.5次元舞台** _2.5-Jigen
butai_ , _litt_. "Pièce de théâtre en 2.5D") est le cinquième arc du manga. Il
couvre l’entièreté du [tome 5](/fr/wiki/Tome_5 "Tome 5"), du [tome
6](/fr/wiki/Tome_6 "Tome 6") et les six premiers chapitres du [tome
7](/fr/wiki/Tome_7 "Tome 7"). Il montre la préparation de l'adaptation de la
pièce de théâtre d'un célèbre manga, Tokyo Blade, et la soirée d'ouverture de
la pièce.

### **Vie Privée :**[[](/fr/wiki/Oshi_no_Ko_\(Manga\)?veaction=edit&section=8
"Modifier la section : Vie Privée :")]

**Vie Privée** (japonais: **プライベート** _Puraibēto_ ) est le sixième arc du
manga. Il couvre le reste du [tome 7](/fr/wiki/Tome_7 "Tome 7") et l’entièreté
du [tome 8](/fr/wiki/Tome_8 "Tome 8"). Il montre la révélation choquante
qu'[Aqua](/fr/wiki/Aquamarine_Hoshino "Aquamarine Hoshino") a trouvée
concernant le mystère de son père, ainsi que la cover du tournage du clip
vidéo de la nouvelle chanson des [B-Komachi](/fr/wiki/B-Komachi "B-Komachi") à
Miyazaki, le lieu de naissance d'[Aqua](/fr/wiki/Aquamarine_Hoshino
"Aquamarine Hoshino") et [Ruby](/fr/wiki/Ruby_Hoshino "Ruby Hoshino") ainsi
que l'endroit où leurs deux vies précédentes,
[Gorô](/fr/wiki/Gor%C3%B4_Amamiya "Gorô Amamiya") et
[Sarina](/fr/wiki/Sarina_Tendoji "Sarina Tendoji"), se sont terminées.

### **Clé de voûte
:**[[](/fr/wiki/Oshi_no_Ko_\(Manga\)?veaction=edit&section=9 "Modifier la
section : Clé de voûte :")]

**Clé de voûte** (japonais: **中座** _Chūza_ ) est le septième arc du manga. Il
montre [Ruby Hoshino](/fr/wiki/Ruby_Hoshino "Ruby Hoshino") alors qu'elle
commençait à se frayer un chemin dans le monde du divertissement en se
présentant à une émission de variétés web
qu'[Aqua](/fr/wiki/Aquamarine_Hoshino "Aquamarine Hoshino") met en vedette, et
plus tard, suit ce dernier alors qu'il découvre les secrets cachés de sa sœur
et le mystère continu de leur père.

### **Scandale :**[[](/fr/wiki/Oshi_no_Ko_\(Manga\)?veaction=edit&section=10
"Modifier la section : Scandale :")]

**Scandale** (japonais: **スキャンダル** _Sukyandaru_ ) est le huitième arc du
manga. Il montre le scandale potentiel impliquant [Kana
Arima](/fr/wiki/Kana_Arima "Kana Arima") après qu'une photo sordide d'elle
avec un réalisateur prometteur ait été prise par un paparazzi, et la mesure
drastique prise pour atténuer le problème par [Aqua
Hoshino](/fr/wiki/Aquamarine_Hoshino "Aquamarine Hoshino").

### **Film :**[[](/fr/wiki/Oshi_no_Ko_\(Manga\)?veaction=edit&section=11
"Modifier la section : Film :")]

**Film** (japonais: **映画** _Eiga_ ) est le neuvième arc du manga. Il est
actuellement en cours, et il montre la production du film documentaire "Quinze
ans de mensonges", y compris la collecte de fonds et le casting des
personnages impliqués, et l'audition pour le rôle d'[Ai
Hoshino](/fr/wiki/Ai_Hoshino "Ai Hoshino"), qui a fini par être joué par [Ruby
Hoshino](/fr/wiki/Ruby_Hoshino "Ruby Hoshino").

## Personnages[[](/fr/wiki/Oshi_no_Ko_\(Manga\)?veaction=edit&section=12
"Modifier la section : Personnages")]

  * [Aquamarine Hoshino](/fr/wiki/Aquamarine_Hoshino "Aquamarine Hoshino")
  * [Ruby Hoshino](/fr/wiki/Ruby_Hoshino "Ruby Hoshino")
  * [Ai Hoshino](/fr/wiki/Ai_Hoshino "Ai Hoshino")
  * [Gorô Amamiya](/fr/wiki/Gor%C3%B4_Amamiya "Gorô Amamiya")
  * [Sarina Tendoji](/fr/wiki/Sarina_Tendoji "Sarina Tendoji")
  * [Kana Arima](/fr/wiki/Kana_Arima "Kana Arima")
  * [Akane Kurokawa](/fr/wiki/Akane_Kurokawa "Akane Kurokawa")

**Voir plus :
[Personnages](https://oshinoko.fandom.com/fr/wiki/Catégorie:Personnage).**

## Tomes[[](/fr/wiki/Oshi_no_Ko_\(Manga\)?veaction=edit&section=13 "Modifier
la section : Tomes")]

**Numéro** | **Couverture** | **Résumé** | **Date**  
---|---|---|---  
[1](/fr/wiki/Tome_1 "Tome 1") |  [![Tome 1
FR](https://static.wikia.nocookie.net/oshi-no-ko-
fr/images/6/6f/Tome_1_FR.jpeg/revision/latest/scale-to-width-
down/109?cb=20230618083833&path-
prefix=fr)](https://static.wikia.nocookie.net/oshi-no-ko-
fr/images/6/6f/Tome_1_FR.jpeg/revision/latest?cb=20230618083833&path-
prefix=fr) [](/fr/wiki/Fichier:Tome_1_FR.jpeg) | Le docteur Gorô est
obstétricien dans un hôpital de campagne. Il est loin du monde à paillettes
dans lequel évolue Aï Hoshino, une chanteuse au succès grandissant dont il est
"un fan absolu". Ces deux-là vont se rencontrer dans des circonstances
exceptionnelles qui changeront leur vie à jamais !  |

  * 17 juillet 2020 (JP)
  * 13 janvier 2022 (FR)

  
[2](/fr/wiki/Tome_2 "Tome 2") |  [![Tome 2
FR](https://static.wikia.nocookie.net/oshi-no-ko-
fr/images/2/2f/Tome_2_FR.webp/revision/latest/scale-to-width-
down/110?cb=20230628112232&path-
prefix=fr)](https://static.wikia.nocookie.net/oshi-no-ko-
fr/images/2/2f/Tome_2_FR.webp/revision/latest?cb=20230628112232&path-
prefix=fr) [](/fr/wiki/Fichier:Tome_2_FR.webp) | Réincarnés dans la peau des
enfants de leur idole, Aï Hoshino, les jumeaux Aqua et Ruby vivent leur
meilleure vie de bébés. Malheureusement, ce bonheur n’est que passager. Les
années passent, les jumeaux entrent bientôt au lycée et s’apprêtent à braver
le monde du spectacle, Aqua avec l’espoir de "se venger" et Ruby avec celui de
"briller" comme sa mère !  |

  * 16 octobre 2020 (JP)
  * 13 janvier 2022 (FR)

  
[3](/fr/wiki/Tome_3 "Tome 3") |  [![Tome 3
FR](data:image/gif;base64,R0lGODlhAQABAIABAAAAAP///yH5BAEAAAEALAAAAAABAAEAQAICTAEAOw%3D%3D)](https://static.wikia.nocookie.net/oshi-
no-ko-fr/images/2/26/Tome_3_FR.webp/revision/latest?cb=20230630092707&path-
prefix=fr) [![Tome 3 FR](https://static.wikia.nocookie.net/oshi-no-ko-
fr/images/2/26/Tome_3_FR.webp/revision/latest/scale-to-width-
down/109?cb=20230630092707&path-
prefix=fr)](https://static.wikia.nocookie.net/oshi-no-ko-
fr/images/2/26/Tome_3_FR.webp/revision/latest?cb=20230630092707&path-
prefix=fr) [](/fr/wiki/Fichier:Tome_3_FR.webp) | Les jumeaux poursuivent leurs
études dans un lycée artistique. Entourée de jeunes artistes ayant tous déjà
un pied dans le milieu, Ruby est impatiente d’enfin démarrer sa carrière de
chanteuse de girls band. Aqua, lui, rencontre un producteur sur le tournage
d’une série. Ce dernier lui propose de participer à une émission de
Téléréalité de rencontre. Il en viendra à se demander ce qu’est la "réalité"
selon le Monde du spectacle…  |

  * 19 février 2021 (JP)
  * 12 mai 2022 (FR)

  
[4](/fr/wiki/Tome_4 "Tome 4") |  [![Tome 4
FR](data:image/gif;base64,R0lGODlhAQABAIABAAAAAP///yH5BAEAAAEALAAAAAABAAEAQAICTAEAOw%3D%3D)](https://static.wikia.nocookie.net/oshi-
no-ko-fr/images/0/0a/Tome_4_FR.webp/revision/latest?cb=20230630092728&path-
prefix=fr) [![Tome 4 FR](https://static.wikia.nocookie.net/oshi-no-ko-
fr/images/0/0a/Tome_4_FR.webp/revision/latest/scale-to-width-
down/110?cb=20230630092728&path-
prefix=fr)](https://static.wikia.nocookie.net/oshi-no-ko-
fr/images/0/0a/Tome_4_FR.webp/revision/latest?cb=20230630092728&path-
prefix=fr) [](/fr/wiki/Fichier:Tome_4_FR.webp) | Les jumeaux font leurs débuts
dans le show-business. Aqua se met en couple avec Akane Kurokawa pour les
besoins d’ _Ici commence l’amour déterminé_. Seulement, une fois les caméras
coupées et le tournage bouclé, que va-t-il rester des relations forgées
pendant l’émission ? De son côté, Ruby s’apprête à lancer officiellement sa
carrière de chanteuse en montant su scène pour la première fois avec son
groupe, les nouvelles B-Komachi !  |

  * 19 mai 2021 (JP)
  * 18 août 2022 (FR)

  
[5](/fr/wiki/Tome_5 "Tome 5") |  [![Tome 5
FR](data:image/gif;base64,R0lGODlhAQABAIABAAAAAP///yH5BAEAAAEALAAAAAABAAEAQAICTAEAOw%3D%3D)](https://static.wikia.nocookie.net/oshi-
no-ko-fr/images/f/fe/Tome_5_FR.webp/revision/latest?cb=20230630092752&path-
prefix=fr) [![Tome 5 FR](https://static.wikia.nocookie.net/oshi-no-ko-
fr/images/f/fe/Tome_5_FR.webp/revision/latest/scale-to-width-
down/109?cb=20230630092752&path-
prefix=fr)](https://static.wikia.nocookie.net/oshi-no-ko-
fr/images/f/fe/Tome_5_FR.webp/revision/latest?cb=20230630092752&path-
prefix=fr) [](/fr/wiki/Fichier:Tome_5_FR.webp) | Les jumeaux font de leur
mieux pour percer dans le monde impitoyable du show-business. L’étape du
premier concert passée, le groupe de Ruby, fraîchement formé, devra être à la
hauteur des espérances des ses nouveaux fans. Quant à Aqua, il se voit
proposer un rôle dans une pièce de théâtre adaptée d’un manga. Sur les
planches, il sera accompagné d’Akane Kurokawa sa "petite-amie" de téléréalité
et Kana Arima, qui est, elle, réellement amoureuse de lui. Vous sentez l’orage
venir ?  |

  * 18 août 2031 (JP)
  * 10 novembre 2022 (FR)

  
[6](/fr/wiki/Tome_6 "Tome 6") |  [![Tome 6
FR](data:image/gif;base64,R0lGODlhAQABAIABAAAAAP///yH5BAEAAAEALAAAAAABAAEAQAICTAEAOw%3D%3D)](https://static.wikia.nocookie.net/oshi-
no-ko-fr/images/b/bf/Tome_6_FR.jpeg/revision/latest?cb=20230630093319&path-
prefix=fr) [![Tome 6 FR](https://static.wikia.nocookie.net/oshi-no-ko-
fr/images/b/bf/Tome_6_FR.jpeg/revision/latest/scale-to-width-
down/109?cb=20230630093319&path-
prefix=fr)](https://static.wikia.nocookie.net/oshi-no-ko-
fr/images/b/bf/Tome_6_FR.jpeg/revision/latest?cb=20230630093319&path-
prefix=fr) [](/fr/wiki/Fichier:Tome_6_FR.jpeg) | La cinquième partie "Pièce de
théâtre adaptée d’un manga" bat son plein. La première représentation est
proche. Au départ, Abiko Samejima, la mangaka de _Tokyo Blade_ , avait refusé
le texte en bloc, mais elle a finalement trouvé un terrain d’entente avec GOA,
le dramaturge qui l’a écrite. Ensemble, ils élaborent une nouvelle pièce qui
les satisfait tous les deux. Les comédiens sauront-ils s’en emparer et donner
vie aux personnages du manga sur les planches ? Soucieux d’offrir une
performance convaincante, Aqua devra se confronter à son traumatisme et à la
perte d’Aï.  |

  * 19 novembre 2021 (JP)
  * 9 février 2023 (FR)

  
[7](/fr/wiki/Tome_7 "Tome 7") |  [![Tome 7
FR](data:image/gif;base64,R0lGODlhAQABAIABAAAAAP///yH5BAEAAAEALAAAAAABAAEAQAICTAEAOw%3D%3D)](https://static.wikia.nocookie.net/oshi-
no-ko-fr/images/b/ba/Tome_7_FR.jpeg/revision/latest?cb=20230630093342&path-
prefix=fr) [![Tome 7 FR](https://static.wikia.nocookie.net/oshi-no-ko-
fr/images/b/ba/Tome_7_FR.jpeg/revision/latest/scale-to-width-
down/110?cb=20230630093342&path-
prefix=fr)](https://static.wikia.nocookie.net/oshi-no-ko-
fr/images/b/ba/Tome_7_FR.jpeg/revision/latest?cb=20230630093342&path-
prefix=fr) [](/fr/wiki/Fichier:Tome_7_FR.jpeg) | Voici enfin la première
représentation de la pièce adaptée du célèbre manga _Tokyo Blade_. Rivales
depuis leur plus jeune âge, Kana Arima ancienne enfant star et Akane Kurokawa
actuelle étoile montante de Lalalaï, s’affrontent sur scène, chacune avec une
façon de jouer bien à elle. Quant à Aqua, il va se servir de son traumatisme
pour enfin mettre de l’émotion dans son jeu.

Quel effet aura cette nouvelle expérience sur lui ? Quel genre d’acteur
choisira-t-il se devenir ? Voici enfin le climax de la 5e partie "Pièce de
théâtre adaptée d’un manga" !

|

  * 18 février 2022 (JP)
  * 11 mai 2023 (FR)

  
[8](/fr/wiki/Tome_8 "Tome 8") |  [![Tome 8
FR](data:image/gif;base64,R0lGODlhAQABAIABAAAAAP///yH5BAEAAAEALAAAAAABAAEAQAICTAEAOw%3D%3D)](https://static.wikia.nocookie.net/oshi-
no-ko-fr/images/4/4f/Tome_8_FR.jpeg/revision/latest?cb=20230720155725&path-
prefix=fr) [![Tome 8 FR](https://static.wikia.nocookie.net/oshi-no-ko-
fr/images/4/4f/Tome_8_FR.jpeg/revision/latest/scale-to-width-
down/109?cb=20230720155725&path-
prefix=fr)](https://static.wikia.nocookie.net/oshi-no-ko-
fr/images/4/4f/Tome_8_FR.jpeg/revision/latest?cb=20230720155725&path-
prefix=fr) [](/fr/wiki/Fichier:Tome_8_FR.jpeg) | Le sixième arc "Vie Privée"
bat son plein ! Apprendre la vérité au sujet de son père à calmé le désir de
vengeance d’Aqua. De leu côté, Ruby et les B Komachi ont gagné en visibilité
grâce à leur chaîne YouTube et décident de tourner leur premier clip.

Profitant de la fin des représentations de _Tokyo Blade_ , Aqua et Akane se
joignent au groupe pour un voyage dans le mythique village de Takachiho, là où
Ruby et Aqua sont morts avant de se réincarner.

|

  * 17 juin 2022 (JP)
  * 17 août 2023 (FR)

  
[9](/fr/wiki/Tome_9 "Tome 9") |  [![Tome 9
FR](data:image/gif;base64,R0lGODlhAQABAIABAAAAAP///yH5BAEAAAEALAAAAAABAAEAQAICTAEAOw%3D%3D)](https://static.wikia.nocookie.net/oshi-
no-ko-fr/images/f/fd/Tome_9_FR.jpeg/revision/latest?cb=20231111065821&path-
prefix=fr) [![Tome 9 FR](https://static.wikia.nocookie.net/oshi-no-ko-
fr/images/f/fd/Tome_9_FR.jpeg/revision/latest/scale-to-width-
down/110?cb=20231111065821&path-
prefix=fr)](https://static.wikia.nocookie.net/oshi-no-ko-
fr/images/f/fd/Tome_9_FR.jpeg/revision/latest?cb=20231111065821&path-
prefix=fr) [](/fr/wiki/Fichier:Tome_9_FR.jpeg) | En voyage au village de
Takachiho pour tourner des clips. Ruby apprend que l’assassin d’Aï avait un
complice.

Ne supportant pas que ce criminel soit encore en vie, elle jure de se venger
en le tuant de ses mains. Nous retrouvons nos héros six mois plus tard, chacun
poursuivant activement son chemin vers les étoiles. La 7e partie "Clé de
voûte" démarre sur les chapeaux de roues !

|

  * 19 octobre 2022 (JP)
  * 9 novembre 2023 (FR)

  
10 |  [![Tome 10
JP](data:image/gif;base64,R0lGODlhAQABAIABAAAAAP///yH5BAEAAAEALAAAAAABAAEAQAICTAEAOw%3D%3D)](https://static.wikia.nocookie.net/oshi-
no-ko-fr/images/f/fb/Tome_10_JP.webp/revision/latest?cb=20230630094025&path-
prefix=fr) [![Tome 10 JP](https://static.wikia.nocookie.net/oshi-no-ko-
fr/images/f/fb/Tome_10_JP.webp/revision/latest/scale-to-width-
down/110?cb=20230630094025&path-
prefix=fr)](https://static.wikia.nocookie.net/oshi-no-ko-
fr/images/f/fb/Tome_10_JP.webp/revision/latest?cb=20230630094025&path-
prefix=fr) [](/fr/wiki/Fichier:Tome_10_JP.webp)

JP

|  |

  * 19 janvier 2023 (JP)
  * 8 février 2024 (FR)

  
11 |  [![Tome 11
JP](data:image/gif;base64,R0lGODlhAQABAIABAAAAAP///yH5BAEAAAEALAAAAAABAAEAQAICTAEAOw%3D%3D)](https://static.wikia.nocookie.net/oshi-
no-ko-fr/images/a/ad/Tome_11_JP.webp/revision/latest?cb=20230630094047&path-
prefix=fr) [![Tome 11 JP](https://static.wikia.nocookie.net/oshi-no-ko-
fr/images/a/ad/Tome_11_JP.webp/revision/latest/scale-to-width-
down/110?cb=20230630094047&path-
prefix=fr)](https://static.wikia.nocookie.net/oshi-no-ko-
fr/images/a/ad/Tome_11_JP.webp/revision/latest?cb=20230630094047&path-
prefix=fr) [](/fr/wiki/Fichier:Tome_11_JP.webp)

JP

|  |

  * 17 mars 2023 (JP)
  * Inconnue (FR)

  
12 |  [![Tome 12
JP](data:image/gif;base64,R0lGODlhAQABAIABAAAAAP///yH5BAEAAAEALAAAAAABAAEAQAICTAEAOw%3D%3D)](https://static.wikia.nocookie.net/oshi-
no-ko-fr/images/d/d8/Tome_12_JP.jpeg/revision/latest?cb=20231111070628&path-
prefix=fr) [![Tome 12 JP](https://static.wikia.nocookie.net/oshi-no-ko-
fr/images/d/d8/Tome_12_JP.jpeg/revision/latest/scale-to-width-
down/108?cb=20231111070628&path-
prefix=fr)](https://static.wikia.nocookie.net/oshi-no-ko-
fr/images/d/d8/Tome_12_JP.jpeg/revision/latest?cb=20231111070628&path-
prefix=fr) [](/fr/wiki/Fichier:Tome_12_JP.jpeg)

JP

|  |

  * 19 juillet 2023 (JP)
  * Inconnue (FR)

  
13 |  [![Tome 13
JP](data:image/gif;base64,R0lGODlhAQABAIABAAAAAP///yH5BAEAAAEALAAAAAABAAEAQAICTAEAOw%3D%3D)](https://static.wikia.nocookie.net/oshi-
no-ko-fr/images/d/d5/Tome_13_JP.jpeg/revision/latest?cb=20231111070700&path-
prefix=fr) [![Tome 13 JP](https://static.wikia.nocookie.net/oshi-no-ko-
fr/images/d/d5/Tome_13_JP.jpeg/revision/latest/scale-to-width-
down/108?cb=20231111070700&path-
prefix=fr)](https://static.wikia.nocookie.net/oshi-no-ko-
fr/images/d/d5/Tome_13_JP.jpeg/revision/latest?cb=20231111070700&path-
prefix=fr) [](/fr/wiki/Fichier:Tome_13_JP.jpeg)

JP

|  |

  * 17 novembre 2023 (JP)
  * Inconnue (FR)

  
  
## Matériel
Promotionnel[[](/fr/wiki/Oshi_no_Ko_\(Manga\)?veaction=edit&section=14
"Modifier la section : Matériel Promotionnel")]

### **Icon Present
:**[[](/fr/wiki/Oshi_no_Ko_\(Manga\)?veaction=edit&section=15 "Modifier la
section : Icon Present :")]

… (à compléter)

Liste des catégories

  * [Catégorie](/fr/wiki/Sp%C3%A9cial:Cat%C3%A9gories "Spécial:Catégories") : 
  * [Manga](/fr/wiki/Cat%C3%A9gorie:Manga "Catégorie:Manga")
  * [Tome](/fr/wiki/Cat%C3%A9gorie:Tome "Catégorie:Tome")
  * Ajouter des catégories

Annuler Enregistrer

Sauf mention contraire, le contenu de la communauté est disponible sous
licence [CC-BY-SA](https://www.fandom.com/fr/licensing-fr).

Advertisement

## Fan Feed

More Wiki Oshi no Ko FR

  * [ 1 Ai Hoshino ](/fr/wiki/Ai_Hoshino "Ai Hoshino")
  * [ 2 Aquamarine Hoshino ](/fr/wiki/Aquamarine_Hoshino "Aquamarine Hoshino")
  * [ 3 Ruby Hoshino ](/fr/wiki/Ruby_Hoshino "Ruby Hoshino")

##  [ ![Fandom
logo](https://static.wikia.nocookie.net/6a181c72-e8bf-419b-b4db-18fd56a0eb60)
](//www.fandom.com/explore-fr?uselang=fr "Fandom logo")

### Propriétés

  * [ Fandom ](//www.fandom.com/explore-fr?uselang=fr)
  * [ Muthead ](https://www.muthead.com/)
  * [ Fanatical ](https://www.fanatical.com/)

### Nous suivre

  * [ ](https://www.facebook.com/fandom.fr)
  * [ ](https://twitter.com/fandom_fr)

### Vue d'ensemble

  * [ Qu'est-ce que Fandom ? ](https://www.fandom.com/what-is-fandom)
  * [ À propos ](https://www.fandom.com/about?uselang=fr)
  * [ Emplois ](https://www.fandom.com/careers)
  * [ Presse ](https://www.fandom.com/press?uselang=fr)
  * [ Contact ](https://www.fandom.com/about?uselang=fr)
  * [ Conditions d'utilisation ](https://www.fandom.com/fr/terms-of-use-fr)
  * [ Politique de confidentialité ](https://www.fandom.com/fr/privacy-policy-fr)
  * [ Loi sur les services numériques ](https://www.fandom.com/digital-services-act)
  * [ Plan du site global ](//community.fandom.com/Sitemap)
  * [ Plan du site local ](/fr/wiki/Local_Sitemap)
  * Cookie Preferences 

### Communauté

  * [ Centre des communautés ](http://communaute.fandom.com/wiki/Centre_des_communaut%C3%A9s)
  * [ Support ](https://fandom.zendesk.com/)
  * [ Aide ](http://communaute.fandom.com/wiki/Aide:Contenu)
  * [ Ne pas vendre mes infos ](https://www.fandom.com/do-not-sell-my-info)

### Publicité

  * [ Kit de presse ](https://about.fandom.com/mediakit)

### Applications Fandom

Emportez vos fandoms favoris partout avec vous.  ![Fandom App
logo](https://static.wikia.nocookie.net/6c42ce6a-b205-41f5-82c6-5011721932e7)

  * [ ![Store icon](https://static.wikia.nocookie.net/464fc70a-5090-490b-b47e-0759e89c263f) ](https://apps.apple.com/fr/app/fandom-videos-news-reviews/id1230063803)
  * [ ![Store icon](https://static.wikia.nocookie.net/f7bb9d33-4f9a-4faa-88fe-2a0bd8138668) ](https://play.google.com/store/apps/details?id=com.fandom.app&hl=fr&referrer=utm_source%3Dwikia%26utm_medium%3Dglobalfooter)

Wiki Oshi no Ko FR est une communauté de FANDOM appartenant à la catégorie
Anime.

Visiter le site mobile

[ Follow on IG ](https://bit.ly/FandomIG) [ TikTok
](https://bit.ly/TikTokFandom) [ Join Fan Lab ](https://bit.ly/FanLabWikiBar)

