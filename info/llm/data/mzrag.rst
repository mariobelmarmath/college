
Hide navigation sidebar
Hide table of contents sidebar
mzrag library

Contents:

    Quickstart API Usage
    Project Cli
    Retrieval Augmented Generation (RAG)
    Inference servers
    GPU Benchmark
    Experiments Plan

Toggle table of contents sidebar
Retrieval Augmented Generation (RAG)
RAG introduction

RAG allows to query LLM with user documents.

Fine-tuning offers many benefits for optimizing LLMs. But it’s also got some limitations. For one, it doesn’t allow for dynamic integration of new or proprietary data. The model’s knowledge remains static post-training, leading it to hallucinate when asked about data outside of its training set.

RAG, on the other hand, dynamically retrieves and incorporates up-to-date and proprietary data from an external database, mitigating the hallucination issue and providing more contextually accurate responses. RAG gives you query-time control over exactly what information is provided to the model, allowing prompts to be tailored to specific users at the exact time a query is made.

RAG is also more computationally efficient and flexible than fine-tuning. Fine-tuning requires the entire model to be retrained for each dataset update, a time-consuming and resource-intensive task. Conversely, RAG only requires updating the document vectors, enabling easier and more efficient information management.

RAG’s modular approach also allows for the fine-tuning of the retrieval mechanism separately, permitting adaptation to different tasks or domains without altering the base language model. RAG enhances the power and accuracy of large language models, making it a compelling alternative to fine-tuning. In practice, enterprises tend to use RAG more often than fine-tuning.
Alternative text

Here some research papers related to RAG:(with links)

    Retrieval-Augmented Generation for Large Language Models: A Survey: https://arxiv.org/pdf/2312.10997

    Financial Report Chunking for Effective Retrieval Augmented Generation : https://arxiv.org/abs/2402.05131

    Seven Failure Points When Engineering a Retrieval Augmented Generation System : https://arxiv.org/abs/2401.05856

Data Flow
Retrieval Augmented Generation
pre-training
Response
Response
Query
Search
Query + revelevant Docs
Gigantic Dataset
Base LLM : llama2
Q/A System
E
U
KB
BL
Knowledge Base
User
../_images/rag_components.png
IndexingProcess
KnowledgeBase
QueryingProcess
DataLoading
Chunking
Node1
User
GenerateResponse
EmbeddingModel
RerankModule
RetrievalModule
User Query
User Query
Output
ReRanked Nodes
Embeded Query
Top-k Nodes
Nodes
Indexed Data
Embedding 1
Generate Embeddings for Nodes
Split Documents into Chunks
Data Loader
Vector Database
Semantic Search
rerank
Generate Embedding for Query
LLM : Llama2
SynthesisModule
Context
GeneratedResponse
User Query Prompt
Indexing stage

Preparing a knowledge base, LlamaIndex help you prepare the knowledge base with a suite of data connectors and indexes.

Data Connectors: A data connector (i.e. Reader) ingest data from different data sources and data formats into a simple Document representation (text and simple metadata).

Documents / Nodes: A Document is a generic container around any data source - for instance, a PDF, an API output, or retrieved data from a database. A Node is the atomic unit of data in LlamaIndex and represents a “chunk” of a source Document. It’s a rich representation that includes metadata and relationships (to other nodes) to enable accurate and expressive retrieval operations.

Data Indexes: Once you’ve ingested your data, LlamaIndex will help you index the data into a format that’s easy to retrieve. Under the hood, LlamaIndex parses the raw documents into intermediate representations, calculates vector embeddings, and infers metadata. The most commonly used index is the VectorStoreIndex
Querying stage

Retrieving relevant context from the knowledge to assist the LLM in responding to a question. In the querying stage, the RAG pipeline retrieves the most relevant context given a user query, and pass that to the LLM (along with the query) to synthesize a response.

This gives the LLM up-to-date knowledge that is not in its original training data, (also reducing hallucination). The key challenge in the querying stage is retrieval, orchestration, and reasoning over (potentially many) knowledge bases.

LlamaIndex provides composable modules that help you build and integrate RAG pipelines for Q&A (query engine), chatbot (chat engine), or as part of an agent. These building blocks can be customized to reflect ranking preferences, as well as composed to reason over multiple knowledge bases in a structured way.
RAG Building Blokcs

Chunkers Chunkers are used to produce chunks from raw source documents. Chunks are piece of text obtained by splitting a source document using a defined strategy. For example, chunks can be sentences, paragraphs with a bounded size (in terms of number of caracters.)

It is also possible to implement smart chunkers with a dynamic size based on the similarly between adjacents sentences.

Embedding Model Embedding models are types of AI language models that are encode text information into vectors. Those vectors, known as embedding vectors represents the meaning of an input text.

The embedding vectors generally have a huge number of dimensions. (e.g. 1536 dimensions for openAI/text-embedding-ada002 model.)

Large Language Model (LLM) LLMS are able to generate text according to an input context. That context includes a user prompt and eventually the concatenation of relevant chunks from a knowledge base.

Retrievers: A retriever defines how to get relevant context from a knowledge base (i.e. index) when given a query.

Basically a context is a set of chunks that are relevant to the user query. The standard strategy find the the most relevant chunks accroding to the vector similary (cosine similarly, euclidean similarity) between user prompt’s embedding and the embedding of the chunks of the entire knowledge base.

Rerankers: Rerankers provide a finer method to select relevant chunks than the simple vector similarity. The pipeline is the following:

    First, top N chunks are retrieved using vector similarity.

    Then, the reranker is applied to the top N chunks to select top K chunks. Generally 

Common rerankers incluces Cross-Encoder encoder transformer, which produce a similarity score for a pair of sentences and BM25 which is a classical information retrieval algorithm, that uses the term frequency and inverse document frequency to score the similarity between a query and a document.

Response Synthesizers: A response synthesizer generates a response from an LLM, using a user query and a given set of retrieved text chunks. There are several strategies used to generate a response :

    Refine : Chunks are iteraly fed to the LLM which a generate a partial answer. Then using the remaining chunks, the LLM is asked to improve that partial anwser.

    Compact : Chunks are concatenated into a single text string followed by the initial user prompt.

mzrag Python library

mzrag is multipurpose RAG library based on lama-index

(mzrag) luis@spinoza:~/g/rd/mzrag/pkg$ make qa_lines_count
  76 ./mzrag/document_extraction/pydantic_models/mz_basemodel.py
 106 ./mzrag/document_extraction/document_extractor.py
 240 ./mzrag/document_extraction/pydantic_models/lease_contract_model.py
 345 ./mzrag/document_extraction/pydantic_models/pydantic_extractor.py
813
   0 ./mzrag/tests/__init__.py
   2 ./mzrag/tests/simple_test.py
2
 104 ./mzrag/evaluation/correctness_evaluator.py
104
  12 ./mzrag/chat/role.py
  25 ./mzrag/chat/llm_client.py
  56 ./mzrag/chat/streaming.py
 100 ./mzrag/chat/vllm_server.py
 113 ./mzrag/chat/message.py
 132 ./mzrag/chat/conversation.py
 246 ./mzrag/chat/chat_server.py
686
  41 ./mzrag/embedding.py
  45 ./mzrag/__init__.py
  65 ./mzrag/reponse_synthesis.py
  67 ./mzrag/llm.py
  72 ./mzrag/logger.py
  77 ./mzrag/indexing.py
 115 ./mzrag/config.py
 117 ./mzrag/storage.py
 123 ./mzrag/utils.py
 125 ./mzrag/migration.py
 221 ./mzrag/cli.py
1068
   8 ./mzrag/retriever/retriever_type.py
  41 ./mzrag/retriever/abstract_retriever.py
  41 ./mzrag/retriever/auto_merging_retriever.py
  45 ./mzrag/retriever/simple_retriever.py
  94 ./mzrag/retriever/fusion_retriever.py
 157 ./mzrag/retriever/hyde_retriever.py
  18 ./mzrag/chunker/hierarchical_chunker.py
  35 ./mzrag/chunker/utils.py
  43 ./mzrag/chunker/qa_json_chunker.py
  45 ./mzrag/chunker/base_chunker.py
 127 ./mzrag/chunker/structure_aware_chunker.py
 175 ./mzrag/chunker/semantic_chunker.py
3788
==> Total: 3874
(mzrag) luis@spinoza:~/g/rd/mzrag/pkg$

Next
Inference servers
Previous
Project Cli
Copyright © 2023, Luis Belmar Letelier
Made with Sphinx and @pradyunsg's Furo
On this page

        RAG introduction
        Data Flow
        Indexing stage
        Querying stage
        RAG Building Blokcs
        mzrag Python library


