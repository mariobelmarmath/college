- je m'appelle Mario Belmar Letelier -- Zhou
- Lycée Lavoisier Paris 5eme 75005 l'année 2023-24
- Collège: Lavoisier Paris 5eme 75005 les années 2019-2023
- stage de 3eme au CNRS dans de la linguistique et de l'IA (reconnaissance de typologies
  dans une grammaire) voir https://anime.in-girum.net/cours/src/3eme_stage/index.html
- site web: https://anime.in-girum.net/cours/src/3eme_stage/index.html
- Intégralité du code / des programmes produit depuis 4 ans https://gitlab.com/mariobelmarmath/college
- Chinois diplome HSK3 (HSK4 en cours d'apprentissage)
- Russe diplome A1
- Espagnol étudié depuis la 4eme à l'école
