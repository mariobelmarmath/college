"""
$ cat  dream_2docs.xml   # 2 first docs from dream-en-open.xml
<corpus id="dream">
    <doc author="Justin E. Abbott" title="The Kātkari Language (A Preliminary Study.)" glottolog_ref_id="85916" hhtype="grammar_sketch" inlg="English [eng]" lgcode="Katkari [kfu]" macro_area="Eurasia" year="1905">
        <sentence id="en2c442e1-en2c48c08">
            <w msd="DT" pos="DT" norm="The" lemma="the" ref="1" deprel="det"> The </w>
            <w msd="NNP" pos="NNP" norm="Katkari" lemma="Katkari" ref="2" deprel="compound"> Katkari </w>
            <w msd="NN" pos="NN" norm="Language" lemma="language" ref="3" deprel="ROOT"> Language </w>
            <w msd="." pos="." norm="." lemma="." ref="4" deprel="punct"> .  </w>
        </sentence>
        <sentence id="en2c44ea6-en2c441f6">
            <w msd="NN" pos="NN" norm="4MlA" lemma="4mla" ref="01" deprel="compound"> 4MlA </w>
            <w msd="NNP" pos="NNP" norm="GiWi" lemma="GiWi" ref="02" deprel="compound"> GiWi </w>
            <w msd="." pos="." norm="." lemma="." ref="42" deprel="punct"> .  </w>
        </sentence>
    </doc>
    <doc author="Albert Arden" title="A progressive grammar of the Telugu language" glottolog_ref_id="315274" hhtype="grammar" inlg="English [eng]" lgcode="Telugu [tel]" macro_area="Eurasia" year="1905">
        <sentence id="en369adb150-en3693dddd2">
            <w msd="DT" pos="DT" norm="This" lemma="this" ref="01" deprel="nsubj"> This </w>
            <w msd="VBZ" pos="VBZ" norm="is" lemma="be" ref="02" deprel="cop"> is </w>
            <w msd="DT" pos="DT" norm="a" lemma="a" ref="03" deprel="det"> a </w>
            <w msd="JJ" pos="JJ" norm="digital" lemma="digital" ref="04" deprel="amod"> digital </w>
        </sentence>
        <sentence id="en369144c6e-en3699509c0">
            <w msd="DT" pos="DT" norm="The" lemma="the" ref="01" deprel="det"> The </w>
            <w msd="NN" pos="NN" norm="letter" lemma="letter" ref="02" deprel="compound"> letter </w>
            <w msd="NN" pos="NN" norm="o" lemma="o" ref="03" deprel="nsubjpass"> o </w>
            <w msd="." pos="." norm="." lemma="." ref="19" deprel="punct"> .  </w>
        </sentence>
    </doc>
</corpus>
$
"""
import xml.etree.ElementTree as ET
import bibtexparser
import pandas as pd

pd.set_option('display.width', 200)

tree = ET.parse('data/dream-en-open.xml')
# tree = ET.parse('data/dream_2docs.xml')
# tree = ET.parse('data/dream_2docs.xml')
root = tree.getroot()
library = bibtexparser.parse_file("data/marcdream/hh.bib")
# library = bibtexparser.parse_file("data/references.bib")
# library = bibtexparser.parse_file("data/references_sample.bib")

def make_xml_df():
    df = pd.DataFrame([d.attrib for d in root])
    df = df.drop(columns=['inlg', 'lgcode', 'macro_area'])
    columns = ['author', 'title', 'glottolog_ref_id', 'hhtype', 'year']
    assert list(df.columns) == columns
    df['glottolog_ref_id'] = df.glottolog_ref_id.astype(int)
    df = df.sort_values('glottolog_ref_id')
    return df

def make_bib_df():
    dict_data = [dict(i.items()) for i in library.entries[:2]]
    df = pd.DataFrame(dict_data)
    df['glottolog_ref_id'] = df.glottolog_ref_id.astype(int)
    df.drop_duplicates()
    df = df.sort_values(by=['glottolog_ref_id'])
    return pd.DataFrame(dict_data)


def merge(df_xml, df_bib):
    df_merge = df_xml.merge(df_bib, on='glottolog_ref_id', how='left').drop_duplicates()
    df_merge['year'] = [a[:4] for a in df_merge['year']]
    df_merge['year'] = df_merge.year.astype(int)
    return df_merge


if __name__ == "__main__":

    df_xml = make_xml_df()
    print(df_xml.shape, df_xml.columns)
    print(df_xml.head())

    df_bib = make_bib_df()
    print(df_bib.shape, df_bib.columns)
    print(df_bib.head())

    df_merge = merge(df_xml, df_bib)
    print(df_merge.head())

    # df_merge.to_excel("_build/documents_merge_output.xlsx")
    df_bib.to_excel("_build/documents_bib_output.xlsx")
    # # print("_build/documents_xml_output.xlsx created")
