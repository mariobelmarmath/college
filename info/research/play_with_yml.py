import yaml
import json
from pathlib import Path

DB = Path('tasks_db.yml')

with DB.open() as tfile:
    db = yaml.safe_load(tfile)
# print(db)

json_string = json.dumps(db, indent=2)
# print(json_string)


