# !python -m spacy download en_core_web_sm
from pprintpp import pprint as pp
from pprint import pprint as pp
import spacy


with open('data/test1_article.txt', 'r') as myfile:
            text = myfile.read()
            sample_text = text[1027000:]
            # sample_text = text[1127000:]
            # start of chapter reference

nlp = spacy.load("en_core_web_sm")

doc = nlp(sample_text)
# doc = nlp(text)

# doc = nlp("""
# I don't like read english book. He already visited U.K and really likes read manga in google
#             """)


# idx_like = [token.i for token in doc if token.text in ['like', 'likes']]
# dict_doc = {}
# def p_index():
#     for token in doc:
#         dict_doc.update({token.text: token.i})
#     print(dict_doc)
# pp(dict_doc, sort_dicts=False)
# dict_doc2 = {}
# def p_index2():
#     for idx in idx_like:
#         for token in doc:
#             if token.i in [idx + 1, idx - 1]:
#                 dict_doc2.update({token.text: token.i})
#     print(dict_doc2)
# def p_chunk():
#     for chunk in doc.noun_chunks:
#         print(chunk.text)
# def p_ents():
#     for token in doc.ents:
#         print(token.text, token.start_char, token.end_char, token.label_, token.label)

dict_sents = {}
def get_dict_sents():
    for sents_id, sents in enumerate(doc.sents):
        dict_sents.update({sents_id: sents})
    return dict_sents

def p_sents(dict_sents):
    for sents_id, sents in enumerate(doc.sents):
        for word in sents:
            if word.text in ['Reference', 'References', 'reference', 'references']:
                print(dict_sents[sents_id+1])


if __name__ == "__main__":
    dict_sents = get_dict_sents()
    p_sents(dict_sents)
