To do
=====

- wget dream-en-open.xml should be downloaded in _build so we can clean with make clean
- same for data/references.bib.gz
- transform text to word -> (word, gramatical_function, rank_index)
- les auteurs cités dans la liste de références à la fin du livre ou de l'article.
- optimisation: supprimer après l'installation les références les blocs dupliqués.

Done
====

- fix the two try ... try ...
- clean up the df['year']
- la maison d'édition (ou l'université si c'est une thèse)
- le nom de la revue si c'est un article.
- save outputs in _build
- Add to references.bib.gz wget in Makefile
- type de la source (livre ou article)
-  l'année de parution de la source 
  - sometime we have "2012-2014" or "2012-[...]"
-  qui est l'auteur (ou les auteurs) 
  - we have one string with all authors
