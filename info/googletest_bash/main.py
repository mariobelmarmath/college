import subprocess
from pathlib import Path
from subprocess import run
from PIL import Image


# result = subprocess.run([sys.executable, 'ls -la' ])
# print(result)

# ls = run(['ls', '..'])
# print(ls)

def create_upload(file: str):
    create = run(['touch', file])
    copy = run(['rclone', 'copy', file, 'googledrive:' ])
    print(file, 'uploaded')


def mv_file_upload(path: str, path_name: str):
    move = run(['mv', path, '.'])
    copy = run(['rclone', 'copy', path_name, 'googledrive:' ])
    print(path, 'move and uploaded')


def convert_move_upload(path, where):
    pathstr = str(path)
    wherestr = str(where)

    image1 = Image.open(pathstr)
    im1 = image1.convert('RGB')
    im1.save(wherestr)
    move = run(['mv', wherestr, './image/'])
    p_img_mv = f"./image/{where.name}"
    copy = run(['rclone', 'copy', p_img_mv, 'googledrive:' ])
    print(path.name, 'convert to', where.name, ', move and uploaded')

if __name__ == "__main__":
    # file = Path('test4.txt')
    # create_upload(file.name)

    # path = Path('/home/mario/img1_pdf.pdf')
    # mv_file_upload(str(path), path.name)

    path_img = Path('/home/mario/img2.jpg')
    where_img = Path('/home/mario/img2_pdf.pdf')
    convert_move_upload(path_img, where_img)
