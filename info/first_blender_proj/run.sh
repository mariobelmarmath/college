if ! [ -x "$(command -v blender)" ]
then
    echo '==> Please install blender'
    echo 'apt-get install blender'
    exit 1
else
    blender 1gameboy.blend
fi
