/* 1. Create a report that shows the CategoryName and Description from the categories table */
/* sorted by CategoryName. */
SELECT
    CategoryName,
    Description
FROM Categories
ORDER BY CategoryName;

/* 2. Create a report that shows the ContactName, CompanyName, ContactTitle and Phone */
/* number from the customers table sorted by Phone. */
SELECT
    ContactName,
    CompanyName,
    ContactTitle,
    Phone
FROM Customers
ORDER BY Phone;

/* 3. Create a report that shows the capitalized FirstName and capitalized LastName renamed */
/* as FirstName and Lastname respectively and HireDate from the employees table sorted from */
/* the newest to the oldest employee. */
SELECT
    HireDate,
    upper(FirstName) AS FirstName,
    upper(LastName) AS LastName
FROM Employees
ORDER BY HireDate;


/* 4. Create a report that shows the top 10 OrderID, OrderDate, ShippedDate, CustomerID, */
/* Freight from the orders table sorted */
SELECT
    OrderID,
    OrderDate,
    ShippedDate,
    CustomerID,
    Freight
FROM Orders
/* ORDER BY OrderID */
ORDER BY Freight DESC
LIMIT 10;

/* 5. Create a report that shows all the CustomerID in lowercase letter and */
/* renamed as ID from the customers table. */
SELECT lower(CustomerID) AS ID FROM Customers;


/* 6. Create a report that shows the CompanyName, Fax, Phone, Country, HomePage */
/* from the suppliers table sorted by the Country in descending order then by */
/* CompanyName in ascending order. */
SELECT
    CompanyName,
    Fax,
    Phone,
    Country,
    HomePage
FROM Suppliers
ORDER BY Country DESC, CompanyName ASC;

/* 7. Create a report that shows CompanyName, ContactName of all customers from */
/* ‘Buenos Aires' only. */
SELECT
    CompanyName,
    ContactName
FROM Customers
WHERE City = "Buenos Aires";

/* 8. Create a report showing ProductName, UnitPrice, QuantityPerUnit of products */
/* that are out of stock. */
SELECT
    ProductName,
    UnitPrice,
    QuantityPerUnit
FROM Products
WHERE UnitsInStock = 0;

/* 9. Create a report showing all the ContactName, Address, City of all customers */
/* not from Germany, Mexico, Spain. */
SELECT
    ContactName,
    Address,
    City
FROM Customers
WHERE Country NOT IN ("Germany", "Mexico", "Spain");

/* 10.Create a report showing OrderDate, ShippedDate, CustomerID, Freight of all */
/* orders placed on 21 May 1996. */

SELECT
    OrderDate,
    ShippedDate,
    CustomerID,
    Freight
FROM Orders
WHERE OrderDate = "1996-05-21";

/* 11.Create a report showing FirstName, LastName, Country from the employees not */
/* from United States. */

SELECT
    FirstName,
    LastName,
    Country
FROM Employees
/* where Country <> 'United States' */
WHERE Country NOT IN ("United States");

/* 12.Create a report that shows the EmployeeID, OrderID, CustomerID, */
/* RequiredDate, ShippedDate from all orders shipped later than the required date. */

SELECT
    EmployeeID,
    OrderID,
    CustomerID,
    RequiredDate,
    ShippedDate
FROM Orders
WHERE ShippedDate > RequiredDate;

/* 13.Create a report that shows the City, CompanyName, ContactName of customers */
/* from cities starting with A or B. */

SELECT
    City,
    CompanyName,
    ContactName
FROM Customers
WHERE City LIKE "A%" OR City LIKE "B%";

/* 14.Create a report showing all the even numbers of OrderID from the orders */
/* table. */

SELECT OrderID
FROM Orders
WHERE (OrderID % 2) = 0;

/* 15.Create a report that shows all the orders where the freight cost more than */
/* $500. */

/* does not work */
SELECT
    Freight,
    OrderID
FROM Orders
HAVING Freight > 500;

/* 16.Create a report that shows the ProductName, UnitsInStock, UnitsOnOrder, */
/* ReorderLevel of all products that are up for reorder. */

SELECT
    ProductName,
    UnitsInStock,
    UnitsOnOrder,
    ReorderLevel
FROM Products
WHERE ReorderLevel = 0;

/* 17.Create a report that shows the CompanyName, ContactName number of all */
/* customer that have no fax number. */

SELECT
    CompanyName,
    ContactName
FROM Customers
/* where isnull(Fax) = 1 */
WHERE Fax IS null;

/* 18.Create a report that shows the FirstName, LastName of all employees that do */
/* not report to anybody. */

SELECT
    FirstName,
    LastName
FROM Employees
WHERE ReportsTo IS null;

/* 19.Create a report showing all the odd numbers of OrderID from the orders */
/* table. */

SELECT OrderID
FROM Orders
WHERE mod(OrderID, 2) = 1;

/* 20.Create a report that shows the CompanyName, ContactName, Fax of all */
/* customers that have Fax number and sorted by ContactName. */

SELECT
    CompanyName,
    ContactName,
    Fax
FROM Customers
WHERE NOT Fax IS null
ORDER BY ContactName;

/* 21.Create a report that shows the City, CompanyName, ContactName of customers */
/* from cities that has letter L in the name sorted by ContactName. */

SELECT
    City,
    CompanyName,
    ContactName
FROM Customers
WHERE City LIKE ("%L%")
ORDER BY ContactName;

/* 22.Create a report that shows the FirstName, LastName, BirthDate of employees */
/* born in the 1950s. */

SELECT
    FirstName,
    LastName,
    BirthDate
FROM Employees
WHERE BirthDate LIKE "1950%";

/* 23.Create a report that shows the FirstName, LastName, the year of Birthdate as */
/* birth year from the employees table. */

SELECT
    FirstName,
    LastName,
    year(BirthDate) AS BirthYear
FROM Employees;

/* 24.Create a report showing OrderID, total number of Order ID as NumberofOrders */
/* from the orderdetails table grouped by OrderID and sorted by NumberofOrders in */
/* descending order. HINT: you will need to use a Groupby statement. */

SELECT
    OrderID,
    count(OrderID) AS NumberofOrders
FROM `Order Details`
GROUP BY OrderID
ORDER BY NumberofOrders DESC;

/* 25.Create a report that shows the SupplierID, ProductName, CompanyName from all */
/* product Supplied by Exotic Liquids, Specialty Biscuits, Ltd., Escargots */
/* Nouveaux sorted by the supplier ID. */

SELECT
    P.SupplierID,
    P.ProductName,
    S.CompanyName
FROM Products AS P
INNER JOIN Suppliers AS S
    ON P.SupplierID = S.SupplierID
WHERE
    S.CompanyName IN (
        "Exotic Liquids",
        "Specialty Biscuits",
        "Ltd.",
        "Ecargots Nouveaux"
    )
ORDER BY P.SupplierID;

/* 26.Create a report that shows the ShipPostalCode, OrderID, OrderDate, */
/* RequiredDate, ShippedDate, ShipAddress of all orders with ShipPostalCode */
/* beginning with "98124". */

SELECT
    ShipPostalCode,
    OrderID,
    OrderDate,
    RequiredDate,
    ShippedDate,
    ShipAddress
FROM Orders
WHERE ShipPostalCode LIKE "98124%";

/* 27.Create a report that shows the ContactName, ContactTitle, CompanyName of */
/* customers that the has no "Sales" in their ContactTitle. */

SELECT
    ContactName,
    ContactTitle,
    CompanyName
FROM Customers
WHERE NOT ContactTitle LIKE "%Sales%";

/* 28.Create a report that shows the LastName, FirstName, City of employees in */
/* cities other than "Seattle"; */

SELECT
    LastName,
    FirstNAme,
    City
FROM Employees
/* where not City = 'Seattle' */
WHERE City <> "Seattle";

/* 29.Create a report that shows the CompanyName, ContactTitle, City, Country of */
/* all customers in any city in Mexico or other cities in Spain other than Madrid. */

SELECT
    CompanyName,
    ContactTitle,
    City,
    Country
FROM Customers
/* where */
/*     (City = 'Mexico') */
/*     OR */
/*     (Country = 'Spain' and City <> 'Madrid') */
WHERE
    Country IN ("Mexico", "Spain")
    AND City <> "Madrid";

/* 30.  Create a select statement that outputs the following: */
/* ContactInfo */
/* ----------- */
/* Nancy Davolio can be reached at x5236 */
/* XXXXX XXXXXio can be reached at xxxxx */
/* XXXXX XXXXXio can be reached at xxxxx */
/* ... */

SELECT
    concat(
        LastName, " ", FirstName, " can be reached at x", Extension
    ) AS ContactInfo
FROM Employees;

/* 31. Create a report that shows the ContactName of all customers that do not */
/* have letter A as the second alphabet in their Contactname. */

SELECT ContactName
FROM Customers
WHERE ContactName NOT LIKE "_A%";

/* 32. Create a report that shows the average UnitPrice rounded to the next whole */
/* number, total price of UnitsInStock and maximum number of orders from the */
/* products table. All saved as AveragePrice, TotalStock and MaxOrder */
/* respectively. */

SELECT
    round(avg(UnitPrice), 0) AS AveragePrice,
    sum(UnitsInStock) AS TotalStock,
    max(UnitsOnOrder) AS MaxOrder
FROM Products;

/* 33. Create a report that shows the SupplierID, CompanyName, CategoryName, */
/* ProductName and UnitPrice from the products, suppliers and categories table. */

SELECT
    S.SupplierID,
    S.CompanyName,
    C.CategoryName,
    P.ProductName,
    P.UnitPrice
FROM Products AS P
INNER JOIN Suppliers AS S
    ON
        P.SupplierID = S.SupplierID
INNER JOIN Categories AS C ON
    P.CategoryID = C.CategoryID;

/* 34. Create a report that shows the CustomerID, sum of Freight, from the orders */
/* table with sum of freight greater $200, grouped by CustomerID. HINT: you will */
/* need to use a Groupby and a Having statement. */

SELECT
    CustomerID,
    sum(Freight) AS TotalFreight
FROM Orders
GROUP BY CustomerID
HAVING TotalFreight > 200;

/* 35. Create a report that shows the OrderID ContactName, UnitPrice, Quantity, */
/* Discount from the order details, orders and customers table with discount given */
/* on every purchase. */

SELECT
    Od.OrderID,
    C.ContactName,
    Od.UnitPrice,
    Od.Quantity,
    Od.Discount,
    (Od.UnitPrice * Od.Quantity * (1 - Od.Discount)) AS SubTotal
FROM `Order Details` AS Od
INNER JOIN Orders AS O ON Od.OrderID = O.OrderID
INNER JOIN Customers AS C ON O.CustomerID = C.CustomerID
LIMIT 10;

/* 36. Create a report that shows the EmployeeID, the LastName and FirstName as */
/* employee, and the LastName and FirstName of who they report to as manager from */
/* the employees table sorted by Employee ID. HINT: This is a SelfJoin. */



/* 37. Create a report that shows the average, minimum and maximum UnitPrice of */
/* all products as AveragePrice, MinimumPrice and MaximumPrice respectively. */
/* 38. Create a view named CustomerInfo that shows the CustomerID, CompanyName, */
/* ContactName, ContactTitle, Address, City, Country, Phone, OrderDate, */
/* RequiredDate, ShippedDate from the customers and orders table. HINT: Create a */
/* View. */
/* 39. Change the name of the view you created from customerinfo to customer */
/* details. */
/* 40. Create a view named ProductDetails that shows the ProductID, CompanyName, */
/* ProductName, CategoryName, Description, QuantityPerUnit, UnitPrice, */
/* UnitsInStock, UnitsOnOrder, ReorderLevel, Discontinued from the supplier, */
/* products and categories tables. HINT: Create a View */
/* 41. Drop the customer details view. */
/* 42. Create a report that fetch the first 5 character of categoryName from the */
/* category tables and renamed as ShortInfo. */
/* 43. Create a copy of the shipper table as shippers_duplicate. Then insert a */
/* copy of shippers data into the new table HINT: Create a Table, use the LIKE */
/* Statement and INSERT INTO statement. */
/* 44. Create a select statement that outputs the following from the */
/* shippers_duplicate Table: 45. Create a report that shows the CompanyName and */
/* ProductName from all product in the Seafood category. */
/* 46. Create a report that shows the CategoryID, CompanyName and ProductName from */
/* all product in the categoryID 5. */
/* 47. Delete the shippers_duplicate table. */
