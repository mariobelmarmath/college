DROP TABLE IF EXISTS `Employees`;
CREATE TABLE `Employees` (
  `EmployeeID` int NOT NULL AUTO_INCREMENT,
  `LastName` varchar(30) NOT NULL,
  `FirstName` varchar(30) NOT NULL,
  `Grade` varchar(20),
  PRIMARY KEY (`EmployeeID`)
);

INSERT INTO Employees VALUES ('1', 'Dubois', 'Alice', 'senior');
INSERT INTO Employees VALUES ('2', 'Martin', 'Bob', 'junior');
INSERT INTO Employees VALUES ('3', 'Dupont', 'Claire', 'senior');
INSERT INTO Employees VALUES ('4', 'Jean', 'Martin', 'manager');
INSERT INTO Employees VALUES ('5', 'Marie', 'Durand', 'manager');
INSERT INTO Employees VALUES ('6', 'Paul', 'Curie', 'manager');

DROP TABLE IF EXISTS `Projects`;
CREATE TABLE `Projects` (
  `ProjectID` varchar(1) NOT NULL,
  `CustomerID` int NOT NULL,
  `EmployeeID` int NOT NULL,
  PRIMARY KEY (`ProjectID`),

INSERT INTO Projects VALUES ('B', '2', '5');
INSERT INTO Projects VALUES ('C', '3', '6');

DROP TABLE IF EXISTS `Customers`;
CREATE TABLE `Customers` (
  `CustomerID` int NOT NULL AUTO_INCREMENT,
  `CustomerName` varchar(30) NOT NULL,
  PRIMARY KEY `Customers` (`CustomerID`)
);

INSERT INTO Customers VALUES ('1', 'X');
INSERT INTO Customers VALUES ('2', 'Y');
INSERT INTO Customers VALUES ('3', 'Z');

DROP TABLE IF EXISTS `ContactEmployees`;
CREATE TABLE `ContactEmployees` (
  `Phone` varchar(12) NOT NULL,
  `EmployeeID` int NOT NULL,
  KEY `Employees` (`EmployeeID`)
);

INSERT INTO ContactEmployees VALUES ('123-456-7890', '1');
INSERT INTO ContactEmployees VALUES ('987-654-3210', '2');
INSERT INTO ContactEmployees VALUES ('555-555-5555', '3');
INSERT INTO ContactEmployees VALUES ('987-654-3210', '2');
INSERT INTO ContactEmployees VALUES ('344-132-1111', '1');
INSERT INTO ContactEmployees VALUES ('343-222-2345', '2');

DROP TABLE IF EXISTS `Schedules`;
CREATE TABLE `Schedules` (
	`EmployeeID` int NOT NULL,
	`ProjectID` varchar(1) NOT NULL,
	`DDate` DateTime NOT NULL,
	`Hour` int(1),
	KEY `Projects` (`ProjectID`),
	KEY `Employees` (`EmployeeID`)
);

INSERT INTO Schedules VALUES ('1', 'A', '2024-06-01', '5');
INSERT INTO Schedules VALUES ('1', 'B', '2024-06-01', '4');
INSERT INTO Schedules VALUES ('2', 'C', '2024-06-01', '3');
INSERT INTO Schedules VALUES ('3', 'A', '2024-06-02', '2');
INSERT INTO Schedules VALUES ('3', 'B', '2024-06-02', '6');

select * from Employees;
select * from Schedules;
select * from ContactEmployees;
select * from Customers;
select * from Projects;
