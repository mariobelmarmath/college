import sqlalchemy
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.engine.url import URL
import os
import pandas as pd
# from sql_python_db import (
#     Employee,
#     Project,
#     ContactInfo,
#     Schedule,
#     Grade,
#     Customer,
# )

MARIADB_URI = URL.create(
    drivername="mysql+pymysql",
    username="mario",
    password=str(os.getenv("MY_PASS_SQL")),
    host="localhost",
    port="3306",
    database="sqlalchemy",
)

engine = sqlalchemy.create_engine(MARIADB_URI)
Base = declarative_base()

d_employee = {
    "employeeID": [1, 2, 3, 4, 5, 6],
    "lastname": ["Dubois", "Martin", "Dupont", "Martin", "Durand", "Curie"],
    "firstname": ["Alice", "Bob", "Claire", "Jean", "Paul", "Marie"],
    "gradeID": [1, 2, 1, 3, 3, 3],
}
df_employee = pd.DataFrame(d_employee)
df_employee.to_sql("Employees", engine, index=False, if_exists='replace')

d_project = {
    "projectID": [1, 2, 3],
    "customerID": [1, 2, 3],
    "employeeID": [4, 5, 6],
}
df_project = pd.DataFrame(d_project)
df_project.to_sql("Projects", engine, index=False, if_exists="replace")

d_contact = {
    "phoneID": [i+1 for i in range(5)],
    "phonenumber": ["123-456-7890", "987-654-3210", "555-555-5555", "344-132-1111",
                    "343-222-2345"],
    "employeeID": [1, 2, 3, 1, 2]
}
df_contact = pd.DataFrame(d_contact)
df_contact.to_sql("ContactInfos", engine, index=False, if_exists="replace")

d_schedule = {
    "scheduleID": [i+1 for i in range(5)],
    "employeeID": [1, 1, 2, 3, 3],
    "projectID": [1, 2, 3, 1, 2],
    "ddate": ["2024-01-06", "2024-01-06", "2024-01-06", "2024-02-06", "2024-02-06"],
    "hour": [5, 4, 3, 2, 6],
}
df_schedule = pd.DataFrame(d_schedule)
df_schedule.to_sql("Schedules", engine, index=False, if_exists="replace")

d_grade = {
    "gradeID": [1, 2, 3],
    "gradename": ["Senior", "Junior", "Manager"],
}
df_grade = pd.DataFrame(d_grade)
df_grade.to_sql("Grades", engine, index=False, if_exists="replace")

d_customer = {
    "customerID": [1, 2, 3],
    "firstname": ["Jean", "Paul", "Marie"],
    "lastname": ["Martin", "Durand", "Curie"],
}
df_customer = pd.DataFrame(d_customer)
df_customer.to_sql("Customers", engine, index=False, if_exists="replace")

Base.metadata.create_all(engine)
Session = sqlalchemy.orm.sessionmaker()
Session.configure(bind=engine)
session = Session()
session.commit()
