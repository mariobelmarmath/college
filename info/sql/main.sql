USE northwind;


SELECT concat(LastName, ' ', FirstName) FROM Employees;

SELECT count(*) AS NumOfEmployees FROM Employees;
SELECT CompanyName FROM Shippers;

SELECT count(*) AS NumOfCompanies FROM Company;

SELECT
    T.TerritoryDescription,
    Ee.EmployeeID,
    Ee.TerritoryID,
    concat(E.FirstName, ' ', E.LastName) AS NName
FROM EmployeeTerritories AS Ee
INNER JOIN Employees AS E ON (Ee.EmployeeID = E.EmployeeID)
INNER JOIN Territories AS T ON (Ee.TerritoryID = T.TerritoryID);;

SELECT
    CompanyName,
    Phone
FROM Shippers
UNION DISTINCT
SELECT
    CompanyName,
    Phone
FROM Customers
UNION DISTINCT
SELECT
    CompanyName,
    Phone
FROM Suppliers
ORDER BY CompanyName;

SELECT
    Big.CompanyName,
    Big.Phone,
    count(*) AS CCount
FROM (
    SELECT
        CompanyName,
        Phone
    FROM Shippers
    UNION DISTINCT
    SELECT
        CompanyName,
        Phone
    FROM Customers
    UNION DISTINCT
    SELECT
        CompanyName,
        Phone
    FROM Suppliers
    ORDER BY Big.CompanyName
) AS Big;

/* SELECT */
/*     OrderID, */
/*     format(sum(UnitPrice * Quantity * (1 - Discount)), 2) AS Subtotal */
/* /1* format(---, 2) for 2 number after the coma *1/ */
/* FROM "Order Details" */
/* GROUP BY OrderID */
/* ORDER BY OrderID; */

WITH B AS (
    SELECT
        OrderID,
        format(sum(UnitPrice * Quantity * (1 - Discount)), 2) AS Subtotal
    FROM `Order Details`
    GROUP BY OrderID
)
;

SELECT DISTINCT
    A.OrderID,
    B.Subtotal,
    date(A.ShippedDate) AS ShippedDate,
    year(A.ShippedDate) AS Year
FROM Orders AS A
INNER JOIN
    B
    ON A.OrderID = B.OrderID
WHERE
    A.ShippedDate IS NOT null
    AND A.ShippedDate BETWEEN '1996-12-24' AND '1997-09-30'
/* and a.ShippedDate between date('1996-12-24') and ('1997-09-30') */
 /* both work */
ORDER BY A.ShippedDate
;
/* LIMIT 100 */

SELECT DISTINCT
    B.*,
    A.CategoryName
FROM Categories AS A
INNER JOIN Products AS B ON A.CategoryID = B.CategoryID
WHERE B.Discontinued = 'N'
/* where not b.Discontinued = 'N' */
ORDER BY B.ProductName;

SELECT DISTINCT
    Y.OrderID,
     Y.ProductID,
     X.ProductName,
     Y.UnitPrice,
     Y.Quantity,
     Y.Discount,
     round(
     (Y.UnitPrice * Y.Quantity * (1 - Y.Discount)), 2) AS ExtendedPrice */
 FROM Products AS X 
 INNER JOIN `Order Details` AS Y ON X.ProductID = Y.ProductID */
 ORDER BY Y.OrderID;
 limit 30;


/* order Products and return the fifth first */
SELECT
    ProductID,
    ProductName,
    QuantityPerUnit,
    UnitPrice
FROM Products
ORDER BY UnitPrice
LIMIT 5;


/* take fifth first product from Products and order only them */
SELECT A.* FROM (
    SELECT
        ProductID,
        ProductName,
        QuantityPerUnit,
        UnitPrice
    FROM Products
    LIMIT 5
) AS A
ORDER BY A.UnitPrice;


SELECT * FROM
    (
        SELECT DISTINCT
            ProductName AS Ten_Most_Expensive_Products,
            UnitPrice
        FROM Products
        ORDER BY UnitPrice DESC
    ) AS A
LIMIT 4;

SELECT T.* FROM (
    SELECT
        CompanyName,
        City,
        Country,
        'Customer' AS Relationship
    FROM Customers
    UNION DISTINCT
    SELECT
        CompanyName,
        City,
        Country,
        'Supplier'
    FROM Suppliers
) AS T
ORDER BY T.Country;
