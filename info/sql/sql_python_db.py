import sqlalchemy
from sqlalchemy.ext.declarative import declarative_base
import pandas as pd
from sqlalchemy.engine.url import URL
import os


MARIADB_URI = URL.create(
    drivername="mysql+pymysql",
    username="mario",
    password=str(os.getenv("MY_PASS_SQL")),
    host="localhost",
    port="3306",
    database="sqlalchemy",
)

engine = sqlalchemy.create_engine(MARIADB_URI)
Base = declarative_base()

class Employee(Base):
   __tablename__ = 'Employees'
   employeeID = sqlalchemy.Column(sqlalchemy.Integer, primary_key=True)
   firstname = sqlalchemy.Column(sqlalchemy.String(length=100))
   lastname = sqlalchemy.Column(sqlalchemy.String(length=100))
   gradeID = sqlalchemy.Column(sqlalchemy.Integer, sqlalchemy.ForeignKey("Grades.gradeID"))


class Project(Base):
   __tablename__ = 'Projects'
   projectID = sqlalchemy.Column(sqlalchemy.Integer, primary_key=True)
   customerID = sqlalchemy.Column(sqlalchemy.Integer,
                                  sqlalchemy.ForeignKey('Customers.customerID'))
   employeeID = sqlalchemy.Column(sqlalchemy.Integer,
                                  sqlalchemy.ForeignKey("Employees.employeeID"))



class ContactInfo(Base):
   __tablename__ = 'ContactInfos'
   phoneID = sqlalchemy.Column(sqlalchemy.Integer, primary_key=True)
   phonenumber = sqlalchemy.Column(sqlalchemy.String(12))
   employeeID = sqlalchemy.Column(sqlalchemy.Integer,
                                  sqlalchemy.ForeignKey("Employees.employeeID"))



class Schedule(Base):
   __tablename__ = 'Schedules'
   scheduleID = sqlalchemy.Column(sqlalchemy.Integer, primary_key=True)
   employeeID = sqlalchemy.Column(sqlalchemy.Integer,
                                  sqlalchemy.ForeignKey("Employees.employeeID"))
   projectID = sqlalchemy.Column(sqlalchemy.Integer, sqlalchemy.ForeignKey("Projects.projectID"))
   ddate = sqlalchemy.Column(sqlalchemy.DateTime)
   hour = sqlalchemy.Column(sqlalchemy.Integer)



class Grade(Base):
   __tablename__ = 'Grades'
   gradeID = sqlalchemy.Column(sqlalchemy.Integer, primary_key=True)
   gradename = sqlalchemy.Column(sqlalchemy.String(200))



class Customer(Base):
   __tablename__ = 'Customers'
   customerID = sqlalchemy.Column(sqlalchemy.Integer, primary_key=True)
   firstname = sqlalchemy.Column(sqlalchemy.String(200))
   lastname = sqlalchemy.Column(sqlalchemy.String(200))


Base.metadata.create_all(engine)
Session = sqlalchemy.orm.sessionmaker()
Session.configure(bind=engine)
session = Session()
session.commit()
