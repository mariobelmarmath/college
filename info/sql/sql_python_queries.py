import sqlalchemy
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import select
from sql_python_db import (
    Employee,
    Project,
    ContactInfo,
    Schedule,
    Grade,
    Customer,
    session
)
from sqlalchemy.engine.url import URL
import os


product = session.query(Project).where(Project.projectID == 1).all()

# if you do:
employees = session.query(Employee).all()
# need to do
for employee in employees:
    print(employee.lastname, employee.firstname)

# better to do:
emp = Employee
employees = session.query(emp.lastname, emp.firstname).all()
# and then, simply do:
print(employees)

num_emp = session.query(Project).count()
posses_phone = session.query(ContactInfo).where(ContactInfo.employeeID == 1).count()


# order by ddate asc
order_query = session.query(Schedule).order_by(Schedule.ddate).all()
# same as:
order_query = session.query(Schedule).order_by(Schedule.ddate.asc()).all()

# order by ddate desc
order_query = session.query(Schedule).order_by(Schedule.ddate.desc()).all()

# Join
join_phone = select(Employee.lastname, ContactInfo.phonenumber).join(ContactInfo)
print(session.execute(join_phone).all())

# double join !
join_phone_grade = select(Employee.lastname,
                          ContactInfo.phonenumber,
                          Grade.gradename).join(ContactInfo).join(Grade)
print(session.execute(join_phone_grade).all())

# limit
join_limit = join_phone_grade.limit(2)
print(session.execute(join_limit).all())

# offset give the n-th to the last (opposite of limit)
join_phone_grade_offset = select(
    Employee.employeeID,
    Employee.lastname,
    ContactInfo.phonenumber,
    Grade.gradename).join(ContactInfo).join(Grade).offset(3)
print(session.execute(join_phone_grade_offset).all())
