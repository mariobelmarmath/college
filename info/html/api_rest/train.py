from typing_extensions import Annotated, Union, Set
from pydantic import BaseModel, Field, HttpUrl
from enum import Enum
from fastapi import FastAPI, Path, Query, Body, UploadFile
from pathlib import Path
from PIL import Image
import subprocess
from subprocess import run

@app.get("/") # décorateur
async def root():
    """
    If we call http://localhost:8000/
    that will return (GET) {'message': 'Hello World'}
    $ uvicorn main:app --reload
    """
    return {'message': 'Hello World'}

@app.get("/test")
async def test():
    """
    If we call http://localhost:8000/test/
    that will return (GET) {'message': 'Hello World'}
    $ uvicorn main:app --reload
    """
    return {'message': 'Hello Test'}


@app.get('/users')
async def read_users():
    """
    GET get data from the server to the client
    GET get data server =====> client
    GET is a read only operation
    """
    return ['john', 'mario']


@app.get('/users/{user_name}')
async def read_user(user_name: str):
    if user_name == 'mario':
        return {'user_name': user_name.upper(), "description": "Super Mario"}
    else:
        return {'user_name': user_name.lower()}


class ModelName(str, Enum):
    """
    print(ModelName.marionet) -> '<ModelName.marionet: 'marionet'>'
    print(ModelName.marionet.value) -> 'marionet'
    """
    marionet = 'marionet'
    jeannet = 'jeannet'
    alicenet = 'alicenet'

@app.get("/mode1s/{mode1_name}")
async def get_mode1(mode1_name: ModelName):
    """
    /mode1s/jeannet
    {"mode1_name": JEANNET, 'message': 'Have some residuals'}
    We use ModelName as a predefine set of values
    """
    if mode1_name is ModelName.marionet:
        return {"mode1_name": mode1_name, "message": 'Deep  learning FTW'}
    if mode1_name.value == 'alicenet':
        return {"mode1_name": mode1_name, 'message': 'Le CNN all the images'}
    return {"mode1_name": mode1_name.upper(), 'message': 'Have some residuals'}


@app.get('/files/{path:path}')
async def get_path(path):
    """
    :path allow to automaticaly escape "/" e.g.
    /files//home/mario/cv.pdf
    return {"path": "/home/mario/cv.pdf"}
    """
    return {"path": path}


@app.get('/simple_items/')
async def read_simple_items(skip: int = 0, limit: int =10):
    """
    return potentialy all fake_items_db
    /simple_items/
    [{"item_name": 'foo'}, {"item_name": "boo"}, {"item_name": "baz"} ]

    /simple_items/?int=1&limit=1
    [{"item_name": "boo"}]

    if you want to inform FastAPI that limit is optionnal
    replace limit: int by limit: Union[int, None] = None
    """
    return FAKE_ITEMS_DB[skip: skip + limit]


@app.get("/simple_items/{item_id}")
async def read_item(item_id: str, q: Union[str, None] = None):
    """
    /simple_items/foo?toto
    {"item_id": foo, "q": "toto"}
    """
    if q:
        return {"item_id": item_id, 'q': q}
    return {"item_id": item_id}


@app.get('/simple_items_with_description/{item_id}')
async def read_item(item_id: str, q: Union[str, None] = None, short: bool = False):
    """
    '/simple_items_with_description/foo?q=ok'
    {"item_id": foo, "q": "ok",
     "description": "This is an amazing item that has a long description"}
    """
    item = {'item_id': item_id}
    if q:
        item.update({'q': q})
    if not short:
        item.update({"description": "This is an amazing item that has a long description"})
        # add to item {"description": "This is an amazing item that has a long description"}
    return item

@app.get('/users/{user_id}/items/{item_id}')
async def read_user_item(user_id: int, item_id: str, q: Union[str, None] = None, short:
                         bool = False):
    """
    we can get information from more than one if you define by default, this value will be optional

    """
    item = {'item_id': item_id, 'owner_id': user_id}
    if q:
        item.update({'q': q})
    if not short:
        item.update({"description": "This is an amazing item that has a long description"})
    return item


class Item(BaseModel):
    name: str = 'toto'
    description: Union[str, None] = None
    price: float = 42
    tax: Union[float, None] = None


@app.post("/items_simple/")
async def create_simple_item(item: Item):
    """
    POST send data from client (laptop) =====> server
    POST is a write operation
    """
    # Here we are suppose to write the item on a DB
    return item

@app.post('/myitems/')
async def create_item(item: Item):
    """
    e.g.
    $ cat my.json
    {"name": "Mario",
     "description": "Cool",
     "price": 42,
     "tax": 3, }
    $
    $ curl xxxx - @my.json
    $
    """
    item_dict = item.dict()
    if item.tax:
        price_with_tax = item.price + item.tax
        item_dict.update({"price with tax": price_with_tax})
    # here we are suppose to write on DB or FS
    return item_dict

@app.put('/myitems/{item_id}')
async def create_item(item_id: int, item: Item):
    return {"item_id": item_id, **item.dict()}
    # why http://127.0.0.1:8000/items/4 don't work ?

@app.put('/myitems/{item_id}')
async def create_item(item_id: int, item: Item, q: Union[str, None] = None):
    result = {"item_id": item_id, **item.dict()}
    if q:
        result.update({'q': q})
    return result

@app.get('/myitems/{item_id}')
async def read_item(
    item_id: Annotated[int, Path(title="the ID of the item to get", gt=0, le=10)],
    # gt: Greater Than | le = Less than or Equal ->> 0<item_id<=10
    # utility of Path ?
    q: Annotated[Union[str, None], Query(alias="item-query")] = None,
    size: Annotated[Union[float, None], Query(alias='size-test-for-float', gt=0, lt=10.3)] = None
):
    results = {'item_id': item_id}
    if size:
        results.update({'size': size})
    if q:
        results.update({'q': q})
    return results
    # http://127.0.0.1:8000/items/4?item-query=four --->>>
    # {"item_id":4,"q":"four"}


class User(BaseModel):
    name: str
    full_name: Union[str, None] = None

@app.put('/items/{item_id}')
async def create_item(item_id: int, user: User, item: Item):
    return {"item_id": item_id, "user": user, 'item': item}

@app.put('/items/{item_id}')
async def create_items(
    item_id: int, user: User, item: Item, test: Annotated[int, Body()]
    # item_id: int, user: User, item: Item, test: int
    # without Body(), test will be define like a q
):
    return {"item_id": item_id, "user": user, 'item': item, "test": test}


@app.put('/items/{item_id}')
async def update_items(
    item_id: int, item: Annotated[Item, Body(embed=True)] ):
    # item_id: int, item: Item):
    # what difference ?
    return {"item_id": item_id, 'item': item}


class Item2(BaseModel):
    name: str
    description: Union[str, None] = Field(
        default=None, title='the description of the item', max_length=300
    )
    price: float = Field(gt=0, description='The price must be greater than zero')
    more: str = 'more'
    tax: Union[float, None] = None


@app.put('/items/{item_id}')
async def update_items(
    item_id: int, item2: Annotated[Item2, Body(embed=True)] ):
    return {"item_id": item_id, 'item': item2}


class Image(BaseModel):
    url: HttpUrl
    # url: HttpUrl = 'bonjour'
    # he shouldn't be agree with 'bonjour'
    name: str

class Product(BaseModel):
    name: str
    description: Union[str, None] = None
    price: float
    tax: Union[float, None] = None
    tag: Set[str] = set()
    # with set tag will be unique
    image: Union[list[Image], None] = None

@app.put('/items/{item_id}')
async def update_items(item_id: int, product: Product):
    """
    can be typep as int, bool, float, str, ...
    """
    return {"item_id": item_id, 'product': Product}


@app.post('/Images/')
async def create_image(images: list[Image]):
  return image
