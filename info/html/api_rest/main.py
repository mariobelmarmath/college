from fastapi import FastAPI, Path, Query, Body, UploadFile
from pathlib import Path
from PIL import Image
import subprocess
from subprocess import run

UPLOAD_DIR = Path() / 'uploadfiles'

app = FastAPI()

@app.post('/uploads/')
async def create_upload_file(file_upload: UploadFile):
    data = await file_upload.read()
    save_to =  UPLOAD_DIR / file_upload.filename
    with open(save_to, 'wb') as f:
        f.write(data)
    path = Path(f'uploadfiles/{file_upload.filename}')
    if path.suffix == '.jpg':
        pathstr = str(path)
        image1 = Image.open(pathstr)
        where = path.with_suffix('.pdf')
        wherestr = str(where)
        image1.save(wherestr, 'PDF', quality=300)
        copy = run(['rclone', 'copy', wherestr, 'googledrive:' ])
        return f' {path.name} have been push in Gdrive as {where.name} '
    else:
        return f' {file_upload.filename} have been save'
