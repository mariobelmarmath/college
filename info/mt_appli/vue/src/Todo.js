// Todo.js
import { fetch_url, is_localhost } from '/src/Config.js';

export default {
    data() {
        return {
            description: "",
            description_put: "",
            fetch_url: fetch_url,
            id_delete: "",
            id_put: "",
            is_localhost: is_localhost,
            tasks: [] ,
            title: "",
            title_put: "",
        }
    },
    async mounted() {
        await this.fetchTasks();
    },
    methods: {
        async fetchTasks() {
            const url = this.fetch_url + "tasks"
            const req = await fetch(url, {"headers": {"accept": "application/json", },
                                          "method": "GET", });
            this.tasks = await req.json();
        },

        async postTasks() {
            const requestoptions = {
                "method": "POST",
                "headers": {"accept": "application/json",
                            "Content-Type": "application/json"},
                body: JSON.stringify({
                    "title": this.title,
                    "description": this.description, })
            };
            const url = this.fetch_url + "tasks/"
            const req = await fetch(url ,requestoptions);
            location.reload();
        },

        async delTasks() {
            const url = this.fetch_url + "tasks/" + this.id_delete + '/'
            fetch(url, { method: 'DELETE'  })
        },

        async delTasks_button(task) {
            const url2 = this.fetch_url + "tasks/" + task.id + '/';
            console.log(url2)
            await fetch(url2, { mode: 'cors', method: 'DELETE'});
            location.reload();
        },

        async refresh() {
            location.reload();
        },

        async putTasks() {
            const requestoptions = {
                "method": "PUT",
                "headers": {"accept": "application/json",
                            "Content-Type": 'application/json'},
                body: JSON.stringify({
                    "title": this.title_put,
                    "description": this.description_put, })
            };
            const url = this.fetch_url + "tasks/" + this.id_put
            const req = await fetch(url ,requestoptions);
            location.reload();
        },
    },
    template: `<div>

    <h1>Todo Management</h1>
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page">root</li>
        <li class="breadcrumb-item"><router-link to="/edit/dont_use_url">edit</router-link></li>
        <li class="breadcrumb-item"><router-link to="/about">About</router-link></li>
      </ol>
    </nav>
    <h4>Number of Tasks: {{tasks.length}}</h4>

    <div class="form-group row shadow round mb-3">
          <div class="col">
              <label class="m-3"><i class="fa fa-plus" style="font-size: 20px"></i> a task:</label>
          </div>
          <div class="col-sm-4 mt-2">
              <input class="form-control" type="text" placeholder="title" name="title" v-model="title">
          </div>
          <div class="col-sm-4 mt-2">
              <input class="form-control" type="text" placeholder="description" name="description" v-model="description">
          </div>
          <div class="col mt-2">
              <button class=" btn btn-primary m-1 mb-2" v-on:click="postTasks()">add tasks</button><br>
          </div>
          </div>

    <div class="row mt-4">
    <div class="col shadow round pt-2">
    <table class='table table-striped'>
      <thead>
        <tr class="table-dark">
          <td scope='col'>title</td>
          <td scope='col'>description</td>
          <td scope='col'>action</td>
        </tr>
      </thead>
      <tbody>
        <tr v-for="task in tasks">
          <td><strong>{{task.title}}</strong></td>
          <td>{{task.description}}</td>
          <td>
            <button class='btn btn-outline-primary btn-sm m-1 mb-2'
            @click="delTasks_button(task);"><i class="fa fa-remove"></i></button>

            <router-link :to="{ name: 'edit', params: {id: task.id}}" class="btn
            btn-outline-primary btn-sm m-1 mb-2"><i class="fa
            fa-pencil"></i></router-link>

          </td>
        </tr>
      </tbody>
    </table>
    </div>

    </div>
    </div>

`
}
