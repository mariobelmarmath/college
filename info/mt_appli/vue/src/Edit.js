// Edit
import { fetch_url, is_localhost } from '/src/Config.js';

export default {
    data() {
      return {
        id_put: this.id,
        title_put: "",
        description_put: "",
        is_localhost: is_localhost,
        tasks: [],
      }
    },
    async mounted() {
        await this.fetchTasks();
        await this.search_task();
    },
    computed: {
      search_task() {
        for (const t in this.tasks) {
          if (this.tasks[t].id === this.id_put ) {
            this.title_put = this.tasks[t].title
            this.description_put = this.tasks[t].description
            return this.tasks[t]
          }
        }
      }

    },
    props: {
      id:{ type: String, required: true}
    },
    methods: {
        async fetchTasks() {
            const url = fetch_url + "tasks"
            const req = await fetch(url, {"headers": {"accept": "application/json", },
                                          "method": "GET", });
            this.tasks = await req.json();
        },

        async putTasks() {
            const requestoptions = {
                "method": "PUT",
                "headers": {"accept": "application/json",
                            "Content-Type": 'application/json'},
                body: JSON.stringify({
                    "title": this.title_put,
                    "description": this.description_put, })
            };
            const url = fetch_url + "tasks/" + this.id_put
            const req = await fetch(url ,requestoptions);
            location.reload();
        },
    },
    template: `
      <div class="container">
<h1>Edit</h1>
<div>
  <nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><router-link to="/">root</router-link></li>
      <li class="breadcrumb-item active" aria-current="page">todo</li>
      <li class="breadcrumb-item"><router-link to="/about">About</router-link></li>
    </ol>
  </nav>
</div>

<h2>Edit: {{id_put}}</h2>
<form>
  <div class="row mb-3 align-items-center">
    <label for="titleInput"
         class="col-sm-2 col-form-label">Title:</label>
    <div class="col-sm-10">
      <input type="text"
           class="form-control"
           id="titleInput"
           placeholder="Enter title"
           v-model="title_put"
           required="">
    </div>
  </div>
  <div class="row mb-3 align-items-center">
    <label for="descriptionTextarea"
         class="col-sm-2 col-form-label">Description:</label>
    <div class="col-sm-10">
      <textarea class="form-control"
           id="descriptionTextarea"
           rows="3"
           placeholder="Enter description"
           v-model="description_put"
           required=""></textarea>
    </div>
  </div><input type="hidden"
        placeholder="id"
        name="id_put"
        v-model="id_put"> <button class='btn btn-primary m-1 mb-2'
        v-on:click="putTasks()">update tasks</button>
  <a class='btn btn-primary m-1 mb-2' href="/">Return Home</a>
</form>


   <pre style="text-align: right"> Environment : {{ is_localhost ? "Dev": "Production"}} </pre>
    
</div>
`
}
