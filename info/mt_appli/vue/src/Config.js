// Config.js
const is_localhost = window.location.host == "0.0.0.0:8000"
const fetch_url = is_localhost ? "http://0.0.0.0:8080/" : "https://anime-api.in-girum.net/"
export { fetch_url, is_localhost };
