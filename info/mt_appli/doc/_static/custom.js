const path = window.location.pathname;
if (path.includes("html/index.html")              ||
    path.includes("projects_infra/index.html")
) {
    document.querySelector(".toctree-wrapper.compound").classList.add("toctree3cols");
}


if (path.includes("12_projects_archi/index.html")
) {
    document.querySelector(".toctree-wrapper.compound").classList.add("toctree3cols_nested");
}

