# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'Mario Knowledge'
copyright = '2023, Mario'
author = 'Mario'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    'sphinx_inline_tabs',
    'sphinxcontrib.mermaid',
    'sphinx_copybutton',
#    'sphinx.ext.viewcode'
    'sphinx.ext.graphviz'
]

# sudo apt install texlive-extra-utils
mermaid_pdfcrop = "/usr/bin/pdfcrop"

templates_path = ['_templates']
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']



# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'furo'
html_static_path = ['_static']

html_css_files = ['custom.css']
html_js_files = ['custom.js']
