Todo
=====
- [api] we should not load the db in every view, have a global loader

Done
====

Go from this::

  (doc) luis@spinoza:~/proj/g/alien/college/info/m-todo$ tree ../mt_appli/
  ../mt_appli/
  ├── app.py
  ├── data
  │   └── tasks-db.yml
  ├── etc
  │   ├── nginx
  │   │   └── sites-available
  │   │       └── mt_appli
  │   └── systemd
  │       └── system
  │           └── mt_appli.service
  ├── gunicorn_conf.py
  ├── Makefile
  ├── __pycache__
  │   ├── app.cpython-310.pyc
  │   └── gunicorn_conf.cpython-310.pyc
  └── requirements.txt

  (doc) luis@spinoza:~/proj/g/alien/college/info/m-todo$ tree
  .
  ├── api
  │   ├── main.py
  │   ├── Makefile
  │   └── test2.yml
  ├── doc
  │   ├── _build
  │   ├── conf.py
  │   ├── index.rst
  │   ├── Makefile
  │   ├── requirements.txt
  │   ├── src
  │   │   ├── web_dataflow.mmd
  │   │   └── webdev.rst
  │   ├── _static
  │   │   ├── custom.css
  │   │   └── custom.js
  │   └── _templates
  ├── data
  │   └── tasks-db.yml
  ├── deploy
  │   └── clone_copy_systemd_nginx.sh
  ├── Makefile
  └── vue
      ├── index.html
      ├── Makefile
      └── src
          └── PetList.js

to this::

  (doc) luis@spinoza:~/proj/g/alien/college/info/mt_appli$ tree 
  ./mt_appli/
  ├── Makefile
  ├── requirements.txt
  ├── app.py
  ├── gunicorn_conf.py
  ├── deploy
  │   └── clone_copy_systemd_nginx.sh
  ├── etc
  │   ├── nginx
  │   │   └── sites-available
  │   │       └── mt_appli
  │   └── systemd
  │       └── system
  │           └── mt_appli.service
  ├── data
  │   └── tasks-db.yml
  ├── doc
  │   ├── _build
  │   ├── conf.py
  │   ├── index.rst
  │   ├── Makefile
  │   ├── requirements.txt
  │   ├── src
  │   │   ├── web_dataflow.mmd
  │   │   └── webdev.rst
  │   ├── _static
  │   │   ├── custom.css
  │   │   └── custom.js
  │   └── _templates
  ├── api
  │   ├── main.py
  │   ├── Makefile
  │   └── test2.yml
  └── vue
      ├── index.html
      ├── Makefile
      └── src
          └── PetList.js

  (doc) luis@spinoza:~/proj/g/alien/college/info/m-todo$ tree
  (doc) luis@spinoza:~/proj/g/alien/college/info/m-todo$



