const Home = { template: '<div>Home</div>'  }
const About = { template: '<div>About</div>'  }
// const UserName = { template: '<div>User name: {{$route.params.userName}}</div>'  }
// const UserId = { template: '<div>User id: {{ $route.params.userId }}</div>'  }
const User = {
  template: `
    <div>
      User name: {{$route.params.user}} <br>
      <button @click="$router.back">go</button> <br>
      <button @click="$router.push({name: 'user', params: { user: 'jean' }})">go to /users/jean</button> <br>
      <button @click="$router.push({path: '/users/edouardo'})">go to /users/eduardo</button> <br>
      <router-view></router-view>
    </div>
  `  }

const UserSettings = { 
  template: `
    <div>
      <h2>User Settings</h2>
      <ul>
        <li><router-link to="/settings/profile">/settings/profile</router-link></li>
        <li><router-link to="/settings/emails" >/settings/emails </router-link></li>
      </ul>
      <router-view/>
      <router-view name="helper"/>
    </div>
  ` }

const UserProfilePreview = { template: `<div><p>User Profile Preview</p></div>` }
const UserProfile = { template: `<div><p>User Profile</p></div>` }

const UserMail = { template: `<div><p>Mail</p></div>` }
const UserPost = { template: '<div>Post</div>' }
const NotFound = { template: '<div>Not Found</div>'  }
const UserProfile2 = { template: `<div><p>User Profile</p></div>` }
const username = 'eduardo'

const routes = [
  { path: '/settings',
    component: UserSettings,
    children: [
     { path: 'emails',
       component: UserMail, },
     { path: 'profile',
       components: {
         default: UserProfile,
         helper: UserProfilePreview,
       },
     }
    ]
  },
  { path: '/', component: Home  },
  { path: '/about', component: About  },
  { path: '/users/:user',
    component: User,
    name: 'user',
    children: [
      {
        path: 'profile',
        component: UserProfile2,
      },
      {
        path: 'post',
        component: UserPost,

      }
    ]
  },
  { path: '/:pathMatch(.*)*', component: NotFound  },
  ]


const router = VueRouter.createRouter({
  history: VueRouter.createWebHashHistory(),
  routes, // short for `routes: routes`
  })

const app = Vue.createApp({})
app.use(router)

app.mount('#app')

// import moment from "moment"
// moment(value).format('YYYY-MM-DD')
