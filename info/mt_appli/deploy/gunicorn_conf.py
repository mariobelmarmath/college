from multiprocessing import cpu_count

# Socket Path
# bind = 'unix:/home/mario/mt_appli/gunicorn.sock'
bind = '127.00.1:8000'

# Worker Options
workers = cpu_count() + 1
worker_class = 'uvicorn.workers.UvicornWorker'

# Logging Options
loglevel = 'debug'
accesslog = '/tmp/mt_appli/access_log'
errorlog =  '/tmp/mt_appli/error_log'
