set -e

# fresh new clone 
rm -fr /tmp/college
cd /tmp/
cd git clone xxx

# create app_user
sudo adduser -s /bin/bash -d /home/app_user/ -m -G sudo app_user


# deploy fastapi on app_user 
cd /tmp/college/info
rsync -aP  mt_appli /home/app_user/
rm -fr /home/app_user/mt_appli/etc

# deploy mt_appli and restart
cd /tmp/college/info
sudo cp mt_appli/etc/systemd/system/mt_appli.service /etc/systemd/system/
sudo systemctl daemon-reload
sudo systemctl restart mt_appli.service
sudo systemctl status mt_appli.service

# deploy nginx and restart
cd /tmp/college/info
sudo cp mt_appli/etc/nginx/sites-available/mt_appli /etc/nginx/sites-available/
sudo nginx -t
sudo systemctl restart nginx
sudo systemctl status nginx

