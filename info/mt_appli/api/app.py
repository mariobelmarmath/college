import yaml
from fastapi.middleware.cors import CORSMiddleware
from pydantic import BaseModel
import uuid
from pathlib import Path
from fastapi import FastAPI

DB = Path('data/tasks-db.yml')

app = FastAPI()


origins = ["https://anime.in-girum.net", "http://localhost:8080",
           "http://0.0.0.0:8000"]
# origins = ["*"]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


class Item(BaseModel):
    title: str
    status: str = 'open'
    description: str


def LOAD():
    with DB.open() as tfile:
        db = yaml.safe_load(tfile)
    return db


def write_in_yml(data: dict):
    with open(DB, 'w') as file:
        yaml.dump(data, file)


@app.get('/')
async def say_hello():
    return {'message': 'Hello'}


@app.get('/tasks')
async def get_tasks():
    return LOAD()



class Item(BaseModel):
    title: str
    status: str = 'open'
    description: str


@app.post('/tasks/')
async def add_task(item: Item):
    db = LOAD()
    d = dict(item)
    d.update({'id': str(uuid.uuid4())})
    db.append(d)
    write_in_yml(db)
    return db


@app.delete('/tasks/{id}/')
async def del_task(id: str):
    db = LOAD()
    db_with_no_id = [d for d in db if not d['id'] == id]
    write_in_yml(db_with_no_id)
    return db_with_no_id   # ?


@app.put('/tasks/{id}/')
async def update_task(id: str, item: Item):
    db = LOAD()
    task_id = [task for task in db if task['id'] == id]
    if task_id:
       task = task_id[0]
    d = dict(item)
    keep_id = {'id': task.pop('id')}
    task.update(d)
    task.update(keep_id)
    db_with_no_id = [d for d in db if not d['id'] == id]
    db_with_no_id.append(task)
    write_in_yml(db_with_no_id)
    return db_with_no_id


@app.get('/tasks/{id}/')
async def research_task(id: str): #-> Union[None, dict]:
    db = LOAD()
    task_id = [task for task in db if task['id'] == id]
    if task_id:
       task = task_id[0]
       return task
    print(f'no {id} in DB')
    return None
