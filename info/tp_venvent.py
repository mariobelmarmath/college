def bisournous(i, moi, autre):
    return 1


def traitre(i, moi, autre):
    return 0


def sympa_irl(i, moi, autre):
    if i % 3 == 0: return 0
    else: return 1


def partie(N, strat1, strat2):
    tab_strat1 = []
    tab_strat2 = []

    for i in range(N):
        i += 1
        print(i)
        tab_strat1.append(strat1(i, tab_strat1, tab_strat2))
        tab_strat2.append(strat2(i, tab_strat2, tab_strat1))
    return tab_strat1, tab_strat2


def score(t, c, p, d, tab_strat1, tab_strat2):
    joueur1 = 0
    joueur2 = 0

    if len(tab_strat2) != len(tab_strat1): return None
    for idx, i in enumerate(tab_strat1):
        manche = (tab_strat1[idx], tab_strat2[idx])
        # manche = (i, tab_strat2[idx])
        if manche == (1, 1):
            joueur2 += c
            joueur1 += c
        if manche == (0, 0):
            joueur1 += p
            joueur2 += p
        if manche == (0, 1):
            joueur1 += t
            joueur2 += d
        if manche == (1, 0):
            joueur1 += d
            joueur2 += t
    return joueur1, joueur2


def rancunier(i, moi, autre):
    if 0 in autre:
        return 0
    return 1


def miroir_gentil(i, moi, autre):
    if len(moi) == 0:
        return 1
    else:
        return autre[i-1]


def miroir_mechant(i, moi, autre):
    if len(moi) == 0:
        return 0
    else:
        return autre[i-1]


def majorité(i, moi, autre):
    c0 = len(['xxx' for i in autre if i == 0 ])
    c1 = len(['xxx' for i in autre if i == 1 ])
    if c0 > c1:
        return 0
    else:
        return 1


def mastermind(i, moi, autre):
    # 3 premières manches
    if i == 1: return 0
    if i == 2 or 3: return 1
    # Après les 3 premières

    if autre[0: 3] == 3*[1]:
        return 0
    else:
        return autre[i-1]
