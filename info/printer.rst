$ sudo dpkg -i hll2375dwpdrv-4.0.0-1.i386.deb 
$ dpkg -L hll2375dwpdrv | grep 2375DW
/opt/brother/Printers/HLL2375DW/cupswrapper/brother-HLL2375DW-cups-en.ppd
$

$ ls /opt/brother/Printers/HLL2375DW/cupswrapper/brother-HLL2375DW-cups-en.ppd


Configure the printer with ``lpadmin``::

  .. $ sudo lpadmin -E -p HLL2375DW -v socket://192.168.3.3 \
  ..     -P /opt/brother/Printers/HLL2375DW/cupswrapper/brother-HLL2375DW-cups-en.ppd \
  ..     -L 'Brother Printer Salon'
  .. $

``correct:: 

  $ sudo lpadmin -E -p HLL2375DW -v socket://192.168.3.3
      -i /opt/brother/Printers/HLL2375DW/cupswrapper/brother-HLL2375DW-cups-en.ppd
      -L 'Brother Printer Salon'
  $

All the config is now in ``/etc/cups/printers.conf``::

  $ vim /etc/cups/printers.conf

Make HLL2375DW the default printer::

  $ lpadmin -d HLL2375DW

check that the printer is up an running::

  luis@spinoza:~$ lpstat -a
  HLL2375DW accepte des requêtes depuis dim. 26 mars 2023 20:35:30
  luis@spinoza:~$

