To share files, we can just copy them inside our ngnix, idealy in a folder with a
basicAuth, then we only have to share the link::

    luis@spinoza:~/$ tar cf mario_boxe.tar mario_boxe/
    luis@spinoza:~/$ scp mario_boxe.tar root@raspberry_remote:/var/www/anime.in-girum.net/html/
    mario_boxe.tar                                           100%   17MB   7.4MB/s   00:02    
    luis@spinoza:~/$ # get mario vido with http://anime.in-girum.net/mario_boxe.tar
    luis@spinoza:~/$

How to untar::

    luis@spinoza:~/Téléchargements/a$ tar xvf mario_boxe.tar
    mario_boxe/
    mario_boxe/run.sh
    mario_boxe/VID-20230422-WA0002.mp4
    mario_boxe/VID-20230422-WA0001.mp4
    luis@spinoza:~/Téléchargements/a$

A ``bash`` file to test the presense of a software (here ``mplayer``)::

    luis@spinoza:~/Téléchargements/a$ cd mario_boxe/
    luis@spinoza:~/Téléchargements/a/mario_boxe$ cat run.sh
    if ! [ -x "$(command -v mplayer)" ]
    then
        echo '==> Please install mplayer'
        echo 'apt-get install mplayer'
        exit 1
    else
        mplayer -fs VID-20230422-WA0001.mp4
        mplayer -fs VID-20230422-WA0002.mp4 -loop 4 -vf rotate=1
    fi
    luis@spinoza:~/Téléchargements/a/mario_boxe$ ./run.sh

.. note::

   With ``mplayer`` we can control the number of loop, and rotate.
