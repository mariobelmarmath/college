XFA forms & PDF DRM (ACSM)
============================

XFA forms in PDF files
----------------------------

Donwload the last deb:

- https://code-industry.net/free-pdf-editor/

install:

.. code:: bash

  $ sudo dpkg -i master-pdf-editor-5.9.82-qt5.x86_64.deb
  $ sudo apt install -y qtcreator qtbase5-dev qt5-qmake cmake
  $ /opt/master-pdf-editor-5/masterpdfeditor5 luis.pdf

Bibliography
~~~~~~~~~~~~~~

- Have a look on https://pdfjs.express/documentation/annotation/xfdf to add edit feature
  to JS devs

  - https://hacks.mozilla.org/2021/10/implementing-form-filling-and-accessibility-in-the-firefox-pdf-viewer/
- study  XFA Forms Vs XFDF (XML Forms Data Format)


Remove DRM from a PDF acsm file
--------------------------------

wget this binary: `knock-1.3.1 <https://web.archive.org/web/20221020182238mp_/https://github.com/BentonEdmondson/knock/releases/download/1.3.1/knock-1.3.1-x86_64-linux>`__ and add dependencies.

.. code:: bash

  $ chmod u+x knock-1.3.1-x86_64-linux
  $ sudo apt install -y qtcreator qtbase5-dev qt5-qmake cmake

.. code:: bash

  $ ./knock-1.3.1-x86_64-linux mecanique-f-et-applications-7e-ed.acsm
  anonymously signing in...
  downloading the file from Adobe...
  Download 100%
  removing DRM from the file...
  PDF file generated at mecanique-fondements-et-applications-7e-ed.pdf
  $

.. note::

  acsm file is just an XML:

  .. code:: bash

    $ file mecanique-fondements-et-applications-7e-ed.acsm
    mecanique-fondements-et-applications-7e-ed.acsm: Unicode text, UTF-8 text

    $ cat mecanique-fondements-et-applications-7e-ed.acsm
    <fulfillmentToken fulfillmentType="buy" auth="user" xmlns="http://ns.adobe.com/adept">
      <distributor>urn:uuid:ecbda3a5-bb64-47ac-9823-b5516746887a</distributor>
      <operatorURL>http://acs.tea-ebook.com/fulfillment</operatorURL>
      <transaction>TEA-PRD$$teaffusion:order-185178$$9782100839780$$pdf</transaction>
      <purchase>2024-02-17T19:26:46+01:00</purchase>
      <expiration>2024-02-21T19:26:46+01:00</expiration>
      <resourceItemInfo>
        <resource>urn:uuid:d69c4844-056d-4130-8c6e-e3d1821b2697</resource>
        <resourceItem>0</resourceItem>
        <metadata>
          <dc:title xmlns:dc="http://purl.org/dc/elements/1.1/">Mécanique – Fondements et applications</dc:title>
          <dc:creator xmlns:dc="http://purl.org/dc/elements/1.1/">José-Philippe Pérez</dc:creator>
          <dc:format xmlns:dc="http://purl.org/dc/elements/1.1/">application/pdf</dc:format>
        </metadata>
        <licenseToken>
          <resource>urn:uuid:d69c4844-056d-4130-8c6e-e3d1821b2697</resource>
          <permissions>
            <display/>
            <excerpt/>
            <print/>
            <play/>
          </permissions>
        </licenseToken>
      </resourceItemInfo>
      <hmac>dQIbPic8eltsN7yXSh6QQWWzcgk=</hmac>
    </fulfillmentToken>
    $



