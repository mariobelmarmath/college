from bs4 import BeautifulSoup
from pathlib import Path
from sys import argv
import pandas as pd

HTML_FILES = "_build/pages"
OUTPUT_TXT = "_build/output"


def make_csv_content(i_file, list_of_words):
    """
    from list_of_words create an _output.txt file | separated

    Fixme: should return a string or list to later write to file
    """
    Path(OUTPUT_TXT).mkdir(parents=True,
                           exist_ok=True)  # like `mkdir -p OUTPUT_TXT`
    text_output = open(f"{OUTPUT_TXT}/{i_file}_output.txt", "w")
    for idx, content in enumerate(list_of_words):
        text_output.write(content)
        if idx % 2 == 0:
            text_output.write('|')
        else:
            text_output.write('\r')


def main(ax:str):
    """
    <div class="thing text-text"
         data-learnable-id="24239114617090"
         data-thing-id="369859537">
      <div class="thinguser"><i class="ico ico-seed ico-purple"></i></div>
      <div class="ignore-ui pull-right"><input type="checkbox"></div>
      <div class="col_a col text"><div class="text">да</div></div>
      <div class="col_b col text"><div class="text">oui</div></div>
    </div>
    OUTPUT_TXT = "_build/output"
    """

    html_files = Path(HTML_FILES + ax)
    files = list(Path(html_files).rglob('./*'))
    # print('len: ', len(files))

    for idx, file in enumerate(files[:]):
        output_file = Path(OUTPUT_TXT + ax) / f'{file.stem}.txt'
        soup = BeautifulSoup(Path(file).read_text(), 'html.parser')
        ca = [i.get_text() for i in soup.select('.col_a .text')]
        cb = [i.get_text() for i in soup.select('.col_b .text')]
        df = pd.DataFrame(ca, cb)
        df.to_csv(sep="|", header=False, path_or_buf=output_file)
        print('.', end='')


if __name__ == "__main__":
    ax = '_a1' if argv[-1] == '1' else '_a2'
    main(ax)
