from functools import partial
import matplotlib.animation as animation
import matplotlib.pyplot as plt
import numpy as np

if __name__ == '__main__':
    fig, axis = plt.subplots(1, 1)
    a = np.array([[0, 1, 0],
                  [0, 1, 0],
                  [0, 1, 0]])
    b = np.array([[0, 0, 0],
                  [1, 1, 1],
                  [0, 0, 0]])
    axis.set_title('Hello')
    y, x = a.nonzero()
    scat = axis.scatter(x, y, marker='s', s=200, color='blue')
    plt.pause(1)
    axis.cla()
    y, x = b.nonzero()
    scat = axis.scatter(x, y, marker='s', s=200, color='red')
    plt.show()

