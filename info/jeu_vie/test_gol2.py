import pytest
import numpy as np
from gol import init_frame_from_string



MS = """
............
............
............
............
.....X......
.....X......
.....X......
............
............
............
............
............
"""


@pytest.fixture
def string_osciliator():
    ms = ("\n"
          "............\n"
          "............\n"
          "............\n"
          "............\n"
          ".....X......\n"
          ".....X......\n"
          ".....X......\n"
          "............\n"
          "............\n"
          "............\n"
          "............\n"
          "............\n")
    return ms


@pytest.fixture
def matrix_osciliator():
    mx = np.array([[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                   [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                   [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                   [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                   [0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0],
                   [0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0],
                   [0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0],
                   [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                   [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                   [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                   [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                   [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]])
    return mx


def test_string(string_osciliator):
    assert string_osciliator == MS


def test_init_frame_from_string(string_osciliator, matrix_osciliator):
    out = init_frame_from_string(string_osciliator)
    assert np.array_equal(out, matrix_osciliator)


@pytest.mark.skip(reason="skip this test for now")
def test_init_frame_from_string2(string_osciliator, matrix_osciliator):
    out = init_frame_from_string(string_osciliator)
    assert np.array_equal(out, matrix_osciliator)

