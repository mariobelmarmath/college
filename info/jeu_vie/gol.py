# from copy import deepcopy
# from ipdb import set_trace
# from plt import main
# from pprint import pprint
# from typing import Any, Tuple

from functools import partial
from random import randint
import matplotlib.animation as animation
import matplotlib.pyplot as plt
import numpy as np

# Oscillateur
INIT_FRAME: str = """
............
............
............
............
.....X......
.....X......
.....X......
............
............
............
............
............
"""


def init_frame_from_string(init_frame: str) -> np.ndarray:
    mat = np.array([[1 if pt == 'X' else 0 for pt in i]
                    for i in init_frame.split()])
    return mat


def gol_next_frame(frame: np.ndarray) -> np.ndarray:
    # new_frame = frame.copy()
    new_frame = np.array([[0 for l in range(10)] for i in range(10)])

    new_frame[randint(0, 9), randint(0, 9)] = 1
    return new_frame


def make_all_frames(frame_nbr) -> list[np.ndarray]:
    frame = init_frame_from_string(INIT_FRAME)
    all_frames = []
    for _ in range(frame_nbr):
        next_frame = gol_next_frame(frame)
        all_frames.append(next_frame)
        frame = next_frame
    return all_frames


def plot_frame(frame: np.ndarray, axis, item: int):
    x, y = frame.nonzero()
    sx, sy = frame.shape
    # axis.axis([0-1, sx, 0-1, sy])
    axis.set_title(f'Jeu de la vie \n  gen: {item}')
    axis.axis([-1, 11, -1, 11])
    scat = axis.scatter(x, y, marker='s', s=200)
    return [axis]


def animate_all(item, frames, axis):
    return plot_frame(frames[item], axis, item)


if __name__ == '__main__':
    iterations_nbr = 190
    fig, axis = plt.subplots(1, 1)
    plt.title('Jeu de la Vie')

    all_frames = make_all_frames(iterations_nbr)
    plt.show()
