import pytest
import numpy as np
from gol import INIT_FRAME
from gol import init_frame_from_string


def test_init_frame_from_string():
    out = init_frame_from_string(INIT_FRAME)
    expected = np.array([[0,0,0,0,0,0,0,0,0,0,0,0],
                         [0,0,0,0,0,0,0,0,0,0,0,0],
                         [0,0,0,0,0,0,0,0,0,0,0,0],
                         [0,0,0,0,0,0,0,0,0,0,0,0],
                         [0,0,0,0,0,1,0,0,0,0,0,0],
                         [0,0,0,0,0,1,0,0,0,0,0,0],
                         [0,0,0,0,0,1,0,0,0,0,0,0],
                         [0,0,0,0,0,0,0,0,0,0,0,0],
                         [0,0,0,0,0,0,0,0,0,0,0,0],
                         [0,0,0,0,0,0,0,0,0,0,0,0],
                         [0,0,0,0,0,0,0,0,0,0,0,0],
                         [0,0,0,0,0,0,0,0,0,0,0,0]])
    assert np.array_equal(expected, out)


# @pytest.mark.skip(reason="not working yet")
def test_init_frame_from_string2():
    assert 5 == 5
