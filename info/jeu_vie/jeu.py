from typing import Union
from time import sleep
import numpy as np

SHAPE = 20
SLEEP_DURATION = 0.1
RULES = {'alive': { 0: 0,
                    1: 0,
                    2: 1,
                    3: 1,
                    4: 0,
                    5: 0,
                    6: 0,
                    7: 0,
                    8: 0, },
         'dead' : { 0: 0,
                    1: 0,
                    2: 0,
                    3: 1,
                    4: 0,
                    5: 0,
                    6: 0,
                    7: 0,
                    8: 0, },}

# matrice = np.zeros(shape=(9 , 9 ), dtype=int)
# matrice[4,3:6] = 1
#[[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
# [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
# [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
# [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
# [0, 0, 0, 1, 1, 1, 0, 0, 0, 0],
# [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
# [0, 0, 0, 0, 0, 1, 1, 0, 0, 0],
# [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
# [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
# [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]])

# matrice = np.zeros(shape=(9 , 9 ), dtype=int)
#matrice[3:5,3:5] = 1
#matrice[5:7,5:7] = 1
##[[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
## [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
## [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
## [0, 0, 0, 1, 1, 0, 0, 0, 0, 0],
## [0, 0, 0, 1, 1, 0, 0, 0, 0, 0],
## [0, 0, 0, 0, 0, 1, 1, 0, 0, 0],
## [0, 0, 0, 0, 0, 1, 1, 0, 0, 0],
## [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
## [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
## [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]])


def get_init_matrix_01():
    """
    [[0, 0, 0, 0, 0, 0, 0, 0, 0],
     [0, 0, 0, 0, 0, 0, 0, 0, 0],
     [0, 0, 0, 0, 0, 0, 0, 0, 0],
     [0, 0, 0, 0, 0, 0, 0, 0, 0],
     [0, 0, 1, 1, 1, 1, 1, 0, 0],
     [0, 0, 1, 0, 0, 0, 1, 0, 0],
     [0, 0, 0, 0, 0, 0, 0, 0, 0],
     [0, 0, 0, 0, 0, 0, 0, 0, 0],
     [0, 0, 0, 0, 0, 0, 0, 0, 0]])
    """
    matrice = np.zeros(shape=(SHAPE, SHAPE), dtype=int)
    matrice[9:11, 7:12] = 1
    matrice[10, 8:11] = 0
    return matrice

def empty_matrix():
    matrice = np.zeros(shape=(SHAPE, SHAPE), dtype=int)
    return matrice


def get_neigbour(matrice, i, j) -> int:
    compteur = 0
    if i-1 > 0:
        if j+1 < 19:
            if j-1 > 0:
                if i+1 < 19:
                    for neig1 in matrice[i-1][j-1:j+2]:
                        if neig1 == 1:
                            compteur+=1
                    if matrice[i][j-1] == 1:
                        compteur+=1
                    if matrice[i][j+1] == 1:
                        compteur+=1
                    for neig3 in matrice[i+1][j-1:j+2]:
                        if neig3 == 1:
                            compteur+=1
    return compteur


def get_status(matrice, i, j) -> Union['alive', 'death']:
    if matrice[i][j] == 0:
        status = 'dead'
    else:
        status = 'alive'
    return status


def get_new_matrice(matrice):
    next_m = empty_matrix()
    for i, nbline in enumerate(matrice):
        for j, nbcol in enumerate(nbline):
            nbr_neibourg = get_neigbour(matrice, i, j)
            status = get_status(matrice, i, j)
            next_stat = RULES[status][nbr_neibourg]
            next_m[i,j] = next_stat
    return next_m


def main(matrice):
    next_m = matrice
    i = 0
    while True:
        i +=1
        next_m = get_new_matrice(next_m)
        print_game(i, next_m)
        sleep(SLEEP_DURATION)


def get_by_id(id_line, id_columns):
    return START[id_line][id_columns]


def print_game(idx, gen):
    header = f"=====================\nGen n°{idx}\n"
    lines = []
    for i in gen:
        aa = ['x' if l == 1 else ' ' for l in i]
        lines.append(' '.join(aa))
    print(header)
    for line in lines:
        print(f'{line}\r')


if __name__ == "__main__":
    matrice = get_init_matrix_01()
    next_m = main(matrice)
