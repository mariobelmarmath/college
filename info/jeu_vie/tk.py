import tkinter as tk

window = tk.Tk()

greeting = tk.Label(text="Hello, Tkinter")

label = tk.Label(
    text="Hello, Tkinter",
    fg="white",
    bg="black",
    width=10,
    height=10
)

button = tk.Button(
    text="Click me!",
    width=25,
    height=5,
    bg="blue",
    fg="yellow",
)


greeting.pack()
label.pack()
button.pack()

window.mainloop()
