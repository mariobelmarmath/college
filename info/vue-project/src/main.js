import './assets/main.css'

import { createApp } from 'vue'
import App from './App.vue'


      // const app = new Vue({
      //   data: () => ({images: null}),
      //   template: `
      //     <div>
      //       <input type="file" @change="uploadFile" ref="file">
      //       <button @click="submitFile">Upload!</button>
      //     </div>
      //   `,
      //   methods: {
      //     uploadFile() {
      //       this.Images = this.$refs.file.files[0];
      //   },
      //     submitFile() {
      //       const formData = new FormData();
      //       formData.append('file', this.Images);
      //       const headers = { 'Content-Type': 'multipart/form-data'  };
      //       axios.post('https://httpbin.org/post',
      //       formData, { headers  }).then((res) => {
      //         res.data.files; // binary representation of the file
      //         res.status; // HTTP status
      //       });
      //       }
      //     }


      createApp({
        data() {
          return {message: 'Hello World'}
        }
      }).mount('#app')

      createApp(App).mount('#app')
