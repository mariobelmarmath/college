# I. Scrutin à un tour

def creer_dico_resulats(vote):
    res = {}
    for l in vote:
        if l in res.keys():
            res[l] +=1
        else:
            res[l] = 1
    return res


def cle_valeur_max(dico):
    v_max = 0
    for k, v in dico.items():
        if v > v_max:
            v_max = v
            k_max = k
    return k_max


def scrutin_un_tour(vote):
    dico = creer_dico_resulats(vote)
    v_max = cle_valeur_max(dico)
    return v_max

# II. Scrutin à deux tour


def candidat_second_tour(dico):
    vainq1 = cle_valeur_max(dico)
    vainqs = [vainq1]
    dico.pop(vainqs[0])
    vainq2 = cle_valeur_max(dico)
    vainqs.append(vainq2)
    return vainqs


def vote_second_tour(liste, dico):
    for cand in liste:
        if cand in liste:
            elu = cand
            break
    return elu


def scrutin_deux_tours(bulletins):
    vote = [bulletin[0] for bulletin in bulletins]
    dico = creer_dico_resulats(vote)
    return vote_second_tour(vote, dico)

# IV. Scrutin par élimination

def liste_sans(liste, a):
    idx = liste.index(a)
    liste.pop(idx)
    return liste


def enlever_candidat(bulletins, a):
    for vote in bulletins:
        vote = liste_sans(vote, a)
    return bulletins


def cle_valeur_min(dico):
    v_min = float('inf')
    for k, v in dico.items():
        if v < v_min:
            v_min = v
            k_min = k
    return k_min


def dernier(bulletins):
    dico = {}
    for i in bulletins:
        for cand in i:
            if cand in dico.keys():
                dico[cand] += 1
            else:
                dico[cand] = 1
    k_min = cle_valeur_min(dico)
    return k_min


def scrutin_elimination(bulletins):
    while len(bulletins[0]) > 1:
        vote = [bulletin[0] for bulletin in bulletins]
        dico = creer_dico_resulats(vote)
        k_min = cle_valeur_min(dico)
        bulletins = enlever_candidat(bulletins, k_min)
    return bulletins[0]





if __name__ == '__main__':
    # bulletins = [['julie', 'julie', 'jule', 'jule', 'jule', 'romain'],
    #              ['romain', 'julie', 'jule', 'jule', 'jule'],
    #              # ['julie', 'julie', 'jule', 'jule', 'jule', 'romain'],
    #              # ['julie', 'julie', 'jule', 'jule', 'jule', 'romain'],
    #              # ['romain', 'julie', 'jule', 'jule', 'jule', 'romain'],
    #              ['jean', 'julie', 'jule', 'jule', 'jule', 'romain'], ]
    bulletins = [['Bob','Dave','Charlie'],['Charlie','Dave','jean']]
    vote = bulletins[0]

    # dico = creer_dico_resulats(vote)
    # print(dico)
    # elu = scrutin_deux_tours(bulletins)
    # print('élu: ', elu)

    # n_bulletins = enlever_candidat(bulletins, 'romain')
    # print(n_bulletins)

def enlever_candidat(T,p):
    R=[]
    for l in T:
        l2 = liste_sans(l,p)
        R.append(l2)
    return R
