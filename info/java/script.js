/*
 * to print 
console.log(100);

a = [100, "i"]

console.log(a);
*/


// console.group();
//   console.error('Alert');
//   console.warn('Alert');
//   console.table({name: "Brad", email: 'ds'})
// console.groupEnd();

// const styles = 'padding: 10px';
// console.log('%Hello word', styles);

//// let can be reassign
let firstName = 'bonj';
firstName = 'our';
// console.log(firstName, lastnime);
console.log(firstName);

//firstName = 'au'
//lastnime = 'revoir'
//console.log(firstName, lastnime);


//// const can't be reassign
//const firstName1 = 'bonj'
//const lastnime1 = 'our '
//console.log(firstName1, lastnime1);

//// firstName1 = 'au'
//// lastnime1 = 'revoir'
//// console.log(firstName1, lastnime1);

const listec = {
  fruit: 'pomme',
  jus: 'orangee',
  légumes: 'poivron',
}

console.log(listec)

listec.fruit = 'fraise'

console.log(listec)

console.log(typeof listec)
console.log(typeof firstName)


// 6 primitives types:
// boolean : 0 or 1
// null:  
// undefined: variable or unction not specified
// number: 
// bglnt: big number
// string: '...'
// symbol: 

// other than primitive => it's Object types
// function: 

console.log(!!(listec)) // like bool() in python, alias of 'not(not())'
console.log("'hello':", !! 'hello') // -> false
console.log("'':", !! '') // -> false
console.log("{}:", !! {}) // -> true ???
console.log("[]:", !! []) // -> true ???
console.log("[]:", !! []) // -> true ???

const x = true && false // logical and
const y = true || false // logical or
console.log(x, y) // -> fals true

//if and else

const d = true;

if (d) {
    v = 1 // so true
} else {
    v = 2 // so false
}
console.log(v);

// compress mode

// const d = true ? 1 : 2;
// name12 = 'Mario'
name12 = 'jo'
const x1 = name12.length > 3 ? 'long' : 'short';
//        |----condition----| |-----|   |-----|
//                             ^if true  ^if false
console.log(name12, ':' , x1); // -> short (with jo)

let value = 'dog'
switch (value) {
  case 'cat':
    console.log(value, 'un Chat')
    break;
  case 'dog':
    console.log(value, 'un chien')
    break;
}

//generate error

let age = 12; console.log(age)
// throw new Error(); //-->> error and stop at this step
age = 14; console.log(age)

//try if error but not stop process

try {
  console.log(Ssdfsf) // impossible action: Ss.. not defined
} catch (error) {     // mean don't stop process
  console.log('|| broke')// and say broke
} finally {
  console.log('this was the try section ||') // broke or not
}// --> broke \n this was the try section

// function

// type: objec

function hello(input) {
  const output = input + ' 🧁 haha !!';
  return output
}

console.log(hello('welcome'))

// compress version; do not use for long funct
const hello2 = (input) => input + '🧁 haha2 !!';

console.log(hello2('welcome'))

print = (msg) => console.log(msg); // create function print

print('my own print funct')

function say() {
  const a = 12
  const b = 'hello'
  return `${a} ${b}` // like in python: f'eg: {egvariable}' 
}

console.log(say()) // --> 12 hello
print(say())

// closure 

function outer() {
  const fish  = 'goldfish';
  let count = 0;

  function inner() {
    count++;                  // add one to count like += 1 in python
    return `${count} ${fish}` // fstring
  }
  return inner // to create memory for function
}

const fun = outer();
print(fun()) // --> 1 goldfish
print(fun()) // --> 2 goldfish
print(fun()) // --> 2 goldfish
print(fun()) // --> 3 goldfish
print(fun()) // --> 4 goldfish
// object


const obj = {
  // like dict
  age: 12,
  name: 'clown',
  hello: function() {
    console.log(this)
  },
  hello2: () => {console.log(this)},
}

obj.hello() // --> the dict
obj.hello2() // nothing because this form is a little diff

// dot notation

const dog = {
  name_dog: "fido",
  age_dog: 4,
  bark() {
    ocnsole.log('woof') 
  },
}

// const name = dog.name;  // | 
//                         // | --> exist an another way of doing 
// const age = dog.age     // |                               

// let's see it

const { name_dog, age_dog } = dog; // put in name dog.name and age dog.name

console.log(name_dog, age_dog)

const { name_dog: namedog, age_dog: agedog } = dog; // put in name dog.name and age dog.name
//
console.log(namedog, agedog)

const arr = ['item 1', 'item 2', 'item 3']
print(arr)
const [i1, i2, i3] = arr 
console.log(i1, i2, i3) // --> item 1, item 2, item 3
const [,, i34] = arr // same as const i34 = arr[3]
console.log(i34) // --> item 3

// Spread, How to merge object


//const dog = {
//  name_dog: "fido",
//  age_dog: 4,
//  bark() {
//    ocnsole.log('woof') 
//  },
//}

// const listec = {
//   fruit: 'pomme',
//   jus: 'orangee',
//   légumes: 'poivron',
// }

const mergeobj = Object.assign({}, dog, listec);

const mergeobj2 = {...dog, ...listec};

const listec1 = {
   fruit: 'pomme',
   jus: 'orangee',
   légumes: 'poivron',
 }

const dog1 = {
  ...listec, // put listec1 in start of dog1
  name_dog: "fido",
  age_dog: 4,
  bark() {
    ocnsole.log('woof') 
  },
  // ...listec, // put listec1 at the start of dog1
}

console.log(dog1)

const personnn = { 
  namex: 23,
}

const xxx = personnn.xxx
print(xxx)


/*
 * Exo 1: return the sum of an array
 */

sum_list = [1, 2, 3, 4, 5]

function mysum(sum_list) {
  let sum = 0;
  for (let i = 0; i < sum_list.length; i++) { 
    sum = sum + sum_list[i]
    }
  return sum
}

print(mysum(sum_list))


/*
 * Exo 2: 
 * input: [2, 3, 4, 5], 4 => 2
 * input: [2, 3, 4, 5], 6 => -1
 */

// sum_list = [1, 2, 3, 4, 5]

function get_index(array, value) {
  console.log('we search:', value, 'in: ',array, 'array index is:')
  // let answer
  for  (let i = 0; i < array.length; i++) {
    if (value === array[i])
    // { answer = i } // NO
    { return i } // more simple
    else { return -1 }
  }
}

out = get_index(sum_list, 41)
print(out)

out2 = get_index(sum_list, 1)
print(out2)

