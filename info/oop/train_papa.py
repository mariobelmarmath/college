# # se ``super()`` for constructors
# # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# To access inherited methods that you did overwrite in a class:

# .. code:: python

class Shape():

  def __init__(self, shapename):
      self.shapename = shapename

  def draw(self):
      print('Drawing.  Setting shape to:', self.shapename)

  def __repr__(self):
      msg = f'Shape: {self.shapename:12} [{str(id(self))[6:]}]'
      return msg


class ColoredShape(Shape):

  def __init__(self, color, **kwds):
      self.color = color
      super().__init__(**kwds)  # to call the Shape __init__

  def draw(self):
      print('Drawing.  Setting color to:', self.color)
      super().draw()

  def __repr__(self):
      msg1 = super().__repr__()
      msg2 = f'Color: {self.color:5} '
      return msg2 + msg1

  def __lt__(self, other):
      # assert type(other) == ColoredShape
      assert isinstance(other, ColoredShape)
      if len(self.shapename) < len(other.shapename):
          return True
      return False

# Drawing.  Setting shape to: cube

print('aaaaaaa')
cs = ColoredShape(color='blue', shapename='square')
cc = ColoredShape(color='red', shapename='triangle')

print(cs)
print(cc)
print(cc.__lt__(cs))
print(cc > cs)
# cs.draw()
# Drawing.  Setting color to: blue
# cs >= cc
# Drawing.  Setting shape to: square
