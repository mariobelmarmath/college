from dataclasses import dataclass
from pydantic import BaseModel
from typing import NamedTuple

# Normal
class Quadrilateral1:
    sides: list[int]
    name = 'quadrilateral'

    def __init__(self, sides):
        assert len(sides) == 4
        assert all([bool(i>0) for i in self.sides])
        self.sides = sides

    def show(self):
        return(f'Hi, I am a {self.name}, I have four sides of length {self.sides}')

    @property
    def perimeter(self):
        return sum(self.sides)


class Rectangle1(Quadrilateral1):
    sides: list[int]
    name = 'rectangle'

    def __init__(self, l):
        super().__init__(l)
        assert self.sides[0] == self.sides[2] and self.sides[1] == self.sides[3]

    @property
    def area(self):
        return self.sides[0] * self.sides[1]


def main1():
    l = [16, 13, 13, 34]
    q1 = Quadrilateral1(l)
    print(q1.show())
    print(q1.perimeter)
    q1.sides[0] += 2
    print(q1.perimeter)
    print('__repr__(): ', q1.__repr__())

    l2 = [14, 13]*2
    r1 = Rectangle1(l2)
    print(r1.show())
    print(r1.perimeter)
    print(r1.area)

# Use dataclass

@dataclass(frozen=True)
class Quadrilateral2:
    sides: list[int]
    name: str = 'quadrilateral'

    def __post_init__(self):
        assert all([bool(i>0) for i in self.sides])

    def show(self):
        return(f'Hi, I am a {self.name}, my sides are: {self.sides}')

    @property
    def perimeter(self):
        return sum(self.sides)


@dataclass(frozen=True)
class Rectangle2(Quadrilateral2):
    name = 'rectangle'

    def __post_init__(self):
        assert (self.sides[0] == self.sides[2]) and (self.sides[1] == self.sides[3])

    @property
    def area(self):
        return self.sides[0] * self.sides[1]

def main2():
    l = [18, 19, 41, 36]
    q2 = Quadrilateral2(l)
    print(q2.show)
    print(q2.perimeter)
    q2.sides[0] -= 10
    print(q2.sides)
    print(q2.perimeter)
    print('__repr__(): ', q2.__repr__())

    l2 = [15, 14]*2
    r2 = Rectangle2(l2)
    # (if frozen=False) r2.sides[1] = -14  not verified by the __post_init__ method
    print(r2.show(), r2.area, r2.perimeter)



# Use NamedTuple from typing

# Quadrilateral3 = namedtuple('Quadrilateral3', ['c1', 'c2', 'c3', 'c4'])
# def main3()
#     r3 = Quadrilateral3(1, 3, 1, 4)

# Use BaseModel from pydantic

if __name__ == '__main__':
    # main1()
    # main2()
    main3()
