# Classes
class Container:
    def __init__(self, volume):
        self._volume = volume
        self._contents = {}


    def __repr__(self):
        infos = ({'volume': f"{self._volume} ml"}, {'contents': self._contents})
        return infos


    def volume(self):
        return self._volume


    def is_empty(self):
        # return bool(self._contents) == False
        return self._contents == {}


    def is_full(self):
        return self.volume_filled() == self.volume


    def volume_filled(self):
        return sum(self._contents.values())


    def volume_available(self):
        return self.volume() - self.volume_filled()


    def empty(self):
        pre_contents = self._contents.copy()
        self._contents = {}
        return pre_contents


    def _add(self, substance, volume):
        if substance in self._contents.keys():
            self._contents[substance] += volume
        else:
            self._contents.update({substance: volume})


    def add(self, substance, volume):
        if self.volume_available() >= volume:
            self._add(substance, volume)
        else:
            raise Exception('not enough place in container')

verre = Container(100)
