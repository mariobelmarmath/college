Doc
====

- first: https://www.udemy.com/course/html-and-css-for-beginners-crash-course-learn-fast-easy/learn/lecture/2311804#overview
- only bootstrap: https://www.udemy.com/course/html-css-bootstrap-build-your-first-website-today/learn/lecture/7138126#overview


- FastAPI service + Ngnix

  - https://www.slingacademy.com/article/deploying-fastapi-on-ubuntu-with-nginx-and-lets-encrypt/
  - https://www.vultr.com/docs/how-to-deploy-fastapi-applications-with-gunicorn-and-nginx-on-ubuntu-20-04/
  - upload file with fastapi

    - https://stackoverflow.com/questions/73442335/how-to-upload-a-large-file-%E2%89%A53gb-to-fastapi-backend
  - https://subscription.packtpub.com/book/web-development/9781803231822/13/ch13lvl1sec62/deploying-fastapi-on-digitalocean-or-really-any-linux-server
