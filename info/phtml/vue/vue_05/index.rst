Vue3
======

Install nodejs (who provide npm)
--------------------------------
NodeJS 18.x and npm 8.x::

  # first upgrade your system
  sudo apt-get update
  sudo apt-get upgrade

  curl -sL https://deb.nodesource.com/setup_18.x | sudo -E bash -
  sudo apt-get install -y nodejs build-essential

Install ``mmdc`` cli for `Mermaid-CLI <https://mermaid-js.github.io/mermaid/>`__ a
powerful programmatic diagram tool mmdc::

  $ # install above node first
  $ sudo npm -g install @mermaid-js/mermaid-cli
  $ which mmdc
  /usr/bin/mmdc
  $ ls -la /usr/bin/mmdc
  /usr/bin/mmdc -> ../lib/node_modules/@mermaid-js/mermaid-cli/src/cli.js
  $


Create a SFC vue project with ``npm init vue@latest``::

  $ npm init vue@latest
  (web) luis@spinoza:~/proj/g/alien/college/info/phtml/vue/vue_04/test$ npm init vue@latest
  
  Vue.js - The Progressive JavaScript Framework
  
  ✔ Project name: … test-vue
  ✔ Add TypeScript? … No / Yes
  ✔ Add JSX Support? … No / Yes
  ✔ Add Vue Router for Single Page Application development? … No / Yes
  ✔ Add Pinia for state management? … No / Yes
  ✔ Add Vitest for Unit Testing? … No / Yes
  ✔ Add an End-to-End Testing Solution? › No
  ✔ Add ESLint for code quality? … No / Yes
  ✔ Add Prettier for code formatting? … No / Yes
  
  Scaffolding project in /home/luis/proj/g/alien/college/info/phtml/vue/vue_04/test/test-vue...
  
  Done. Now run:
  
    cd test-vue
    npm install
    npm run format
    npm run dev
  $

