
export default {
  name: "mycomponent",
  data() {
    return { count: 0 }
  },
  template: `<div>count is {{ count }}</div>`
}
