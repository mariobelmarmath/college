// PetList.js
export default {
    data() {
        return {
            isLoading: false,
            pets: [],
        }
    },
    async mounted() {
        await this.fetchPets();
    },
    methods: {
        async fetchPets() {
            this.isLoading = true;
            // /findByStatus?status=available&status=pending&status=sold",
            const url = "https://petstore.swagger.io/v2/pet/findByStatus?status=available"
            const req = await fetch(url, {"headers": { "accept": "application/json", },
                                          "method": "GET", });
            this.pets = await req.json();
            this.isLoading = false;
        },
    },
    template: `<div>
        <p v-if="isLoading">loading</p>
        <div v-else>
          <ul>
            <li v-for="pet in pets">
              <p>{{pet.name}}, {{pet.status}}, {{pet.id}}</p>
            </li>
          </ul>
          <pre>{{pet}}</pre>
        </div>
    </div>`
}
