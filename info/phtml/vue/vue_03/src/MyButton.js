// MyButton.js
export default {
  data() {
    return { count: 0 }
  },
  methods: {
    increase() { this.count++; },
    decrease() { this.count--; },
  },
  template: `<div>
    count is {{ count }}
    <button @click="decrease">decrease - </button>
    <button @click="increase">increase + </button>
  </div>`
}
