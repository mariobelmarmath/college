efirstcomponentxport default {
  data() {return {count: 1}},
  methods: {
    increase() {this.count++;},
    decrease() {this.count--;}
  },
  template: `<div>
    count is {{ count }}
    <button @click="increase"> More One </button>
    <button @click="decrease"> One Less </button>
  </div>`
}
