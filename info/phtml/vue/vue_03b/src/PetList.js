// PetList.js
export default {
    data() {
        return {
            title: "",
            description: "",
            title_put: "",
            description_put: "",
            id_delete: "",
            id_put: "",
            tasks: [] ,
        }
    },
    async mounted() {
        await this.fetchTasks();
    },
    methods: {
        async fetchTasks() {
            const url = 'https://anime-api.in-girum.net/tasks'
            const req = await fetch(url, {"headers": {"accept": "application/json", },
                                          "method": "GET", });
            this.tasks = await req.json();
        },

        async postTasks() {
            const requestoptions = {
                "method": "POST",
                "headers": {"accept": "application/json",
                            "Content-Type": 'application/json'},
                body: JSON.stringify({
                    "title": this.title,
                    "description": this.description, })
            };
            const url = "https://anime-api.in-girum.net/tasks/"
            const req = await fetch(url ,requestoptions);
        },

        async delTasks() { 
            const url = 'https://anime-api.in-girum.net/tasks/' + this.id_delete
            fetch(url, { method: 'DELETE'  })
        },

        async delTasks_button(task) { 
            const url = 'https://anime-api.in-girum.net/tasks/' + task.id
            fetch(url, { method: 'DELETE'  })
        },

        async putTasks() {
            const requestoptions = {
                "method": "PUT",
                "headers": {"accept": "application/json",
                            "Content-Type": 'application/json'},
                body: JSON.stringify({
                    "title": this.title_put,
                    "description": this.description_put, })
            };
            const url = "https://anime-api.in-girum.net/tasks/" + this.id_put
            const req = await fetch(url ,requestoptions);
        },
    },
template: `<div>
    <h2>Get</h2>
    <h4>{{tasks.length}}</h4>

      <ul>
        <li v-for="task in tasks">
          <p><strong>{{task.title}}</strong>, {{task.status}}, {{task.description}},  {{task.id}}</p>
          <input type="checkbox">
          <button @click="delTasks_button(task)">X</button>
        </li>
      </ul>

        <li v-for="task in tasks">
          <p><strong>{{task.title}}</strong>, {{task.status}}, {{task.description}},  {{task.id}}</p>
          <input type="checkbox">
          <button @click="delTasks_button(task)">X</button>
        </li>

    <h2>Post</h2>
      <input type="text" placeholder="title" name="title" v-model="title"> {{title}}<br>
      <input type="text" placeholder="description" name="description" v-model="description"> {{description}}<br><br>
      <button v-on:click="postTasks()">add tasks</button><br>
    <h2>Put</h2>
      <input type="text" placeholder="title" name="title" v-model="title_put"> {{title_put}}<br>
      <input type="text" placeholder="description" name="description" v-model="description_put"> {{description_put}}<br><br>
      <input type="text" placeholder="id" name="id_put" v-model="id_put"> <br><br>
      <button v-on:click="putTasks()">update tasks</button><br><br><br>
    <h2>Delete</h2>
      <input type="text" placeholder="id" name="id_delete" v-model="id_delete"> <br><br>
      <button v-on:click="delTasks()">delete tasks</button><br><br><br>
    </div>`
}
