from fastapi import FastAPI

app = FastAPI()

@app.get('/')
async def root():
    """
    If we call http://localhost:port/
    that will return (GET) {'message': 'Hello Marito'}

    $ unvicorn main:app --reload
    Uvicorn running on http://127.0.0.1:8000 (Press CTRL+C to quit)
    ...
    """
    return {'message': 'Hello Marito'}

if __name__ == '__main__':
    print(root.__doc__)
