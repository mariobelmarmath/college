// console.log to print stuff
a=42; console.log(a);
// console.error("OUps");

/*  // to comment blocks
console.group('error');
  console.log("Hello", 22, 44, a);
  console.log("H a h ah a");
console.groupEnd();
*/

// make a dict
dd = {name: "Luis", Age: 33};
console.log(dd);

const styles = 'padding: 10px;';
console.log(styles, dd);

// let can be reassign
let name = 'Luis';
let age = 33;
console.log(name, age);
age = 52;
console.log(name, age);
age = 53;
console.log(name, age);

// const can not be reassign
const revolution_fr = 1789;
// revolution_fr = 2023;   // trigger an error

// list
ll = [2, 'toto', 88]
console.log(ll)
// Array(3) [ 2, "toto", 88 ]

// dict
const address = {nom: 'Rivoli'};
address.numero = 23;
address.nom = 'Louvre'
console.log(address, typeof address, typeof address.numero);

// Primitive types
console.log("Boolean\nNull\nUndefined\nNumber\nBigInt\nString\nSymbol")

// Object types
console.log("Boolean\nNull\nUndefined\nNumber\nBigInt\nString\nSymbol")

// undefine
let xx;
console.log('xx:', xx) // undefined

function f1() {};
console.log('f1:', f1()); // undefined

s = 'Hello';
console.log(s === String('Hello'));  // true === test both type and value
si = '22'
console.log('ii == 22:', si == 22);  // true  == first cast the type then test value


// !!x is like bool(x) in Python, ! is not

console.log("'':", !! '');     // false
console.log("0:", !! 0);     // false
console.log("1:", !! 1);     // true
console.log("'hi':", !! 'hi'); // true
console.log("{}:", !! '{}');   // true
console.log("[]:", !! '[]');   // true

// control logic: if then else or ternary operator

// x = if (expression) { exp_if_true } else { exp_if_false };
// x = expression      ? exp_if_true   : exp_if_false ;
if (true ) {
  console.log('in if')
} else {
  console.log('in else')
};
'in else'
const nom = 'Bob';

name = nom.length > 3 ? 'long' : 'short';
console.log("name = name.length > 3 ? 'long' : 'short':", name)

// switch case break;

let value = 'dog'
switch (value) {
  case 'cat':
    console.log(value, 'un Chat')
    break;
  case 'dog':
    console.log(value, 'un Chien')
    break;
}

const animal = {
  dna: 'ATCG',
};

const dog = {
  face: '🐺',
};

console.log(4444, dog.face, dog.dna);
Object.setPrototypeOf(dog, animal);
console.log(4445, dog.face, dog.dna);

// create an exception/error  throw new Error();

let myage = 33; console.log(myage)
// throw new Error();
myage = 34; console.log(myage)

// try except
// try {} catch (error)  {};
// try {} catch (error)  {} finaly {};

try {throw new Error(); console.log('works')
     } catch (error) {console.log('we got error')} // we got error
try {console.log('works')
     } catch (error) {console.log('we got error')} // works
try {throw new Error(); console.log('works')
     } catch (error) { console.log('we got error')} finally {console.log('ha')}
// we got error
// ha
//

// functions
function add_cake(input) {
  const output = input + ' 🧁'
  return output
}
console.log(add_cake('Bonjour fiston'))

const add_cake2 = (input) => {return input + ' 🧁'}
console.log(add_cake2('re-Bonjour fiston'))

const add_cake3 = (input) => input + ' 🧁'
console.log(add_cake3('re-re-Bonjour fiston'))

function print(msg){
  console.log(msg)
}
print('prout')

// f-string like with backquote and ${var}
age = 33
print(`mon age: ${age}`) // mon age: 33

// Closure function inside a function, like a class instance

let c = 1;
print(c);
c++;
print(c);

function outer() {
  const fish = ' fish';
  let count = 0;

  function inner() {
    count ++;
    return `${count} ${fish}`
  }
  return inner
}

const fun = outer();
print(fun()) // 1
print(fun()) // 2
print(fun()) // 3


// Object  like Pyhton dict, this is local context

const obj = {
  name: 'Clown',
  face: 'Clown face',
  age: 99,
  hello: function() {console.log(this)},
  hello2: () => {console.log(this)}
}

obj.hello()
//  {
//   name: 'Clown',
//   face: 'Clown face',
//   age: 99,
//   hello: [Function: hello],
//   hello2: [Function: hello2]
// }
//

obj.hello2()  // so () => {} is not exactly the same as function () {}
//{}

const person = {
  name3: 'John',
  age3: 32,
  city3: 'New York',
  country3: 'USA'
};

// dot not notation
const age1 = person.age3;
const name1 = person.name3;
console.log('dot notation:', age1);

// Object destructuring
const {age3, name3} = person;
console.log(age3, name3);

// Object destructuring
const {age3:age33, name3:name33} = person;
console.log('rename on destructuring:', age33, name33);

// 1990: internet Tim burton Lee
// 1993: Mosaic first browser Illoona
// 1994: Netscape
// 1995: Mocha, javascript Déc1995
// 1996: IE JScript
// 1997: ECMA ECMA Script
// 2000: ES3 ECMA Script V3, === Strict Equals Operator, try except
// 2000: IE 90% shares, Ajax, ES3.1 Vs ES4
// 2006: jQuery
// 2008: Chrome V8
// 2009: Node, ES3.1==ES5:
// 2010: ES5:  JSON, strict mode, [3, 5].map(v => v*2)
// 2010: Angular
// 2015: ES6 Promise, class constructor, let, const, React, VueJS
// 2015: Bundlers: WebPack, Vi.., Typescript

// for
// for (initialization; condition; afterthought)
//      {statement}
for (let i = 0; i < 5; i++) {console.log('i:', i)};


/*
 * Return the sum of an array values:
 *
 * arr: [ 2, 3, 5, 7, 9 ]
 * i: 0 arr[i]: 2 cum: 2
 * i: 1 arr[i]: 3 cum: 5
 * i: 2 arr[i]: 5 cum: 10
 * i: 3 arr[i]: 7 cum: 17
 * i: 4 arr[i]: 9 cum: 26
 * my_sum: 26
 */
function my_sum(arr) {
  console.log('arr:', arr)
  let res = 0
  for (let i = 0; i < arr.length; i++) {
    res = res + arr[i]
    console.log('i:', i, 'arr[i]:', arr[i], 'cum:', res)
  }
  return res
}

out = my_sum([2, 3, 5, 7, 9])
console.log('my_sum:', out)


/*
 * input: [2, 3, 6, 7, 9], 3 => 1
 * input: [2, 3, 6, 7, 9], 5 => -1
 * input: [2, 3, 6, 7, 9], 7 => 3
 */
function get_index(arr, value) {
  console.log('arr:', arr, 'value:', value)
  for (let i = 0; i < arr.length; i++) {
    if (arr[i] === value) { return i }
  }
  return -1
}

// Test Driven Developpment
console.log('// ===== get_index')
out = get_index([2, 3, 6, 7, 9], 7)
console.log('get_index:', typeof out, out)

out = get_index([2, 3, 6, 7, 9], 5)
console.log('get_index:', typeof out, out)


