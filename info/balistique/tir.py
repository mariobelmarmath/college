from math import sin,cos, pi
import numpy as np
import matplotlib.pyplot as plt
import sys

G = 9.81

def pos(t:int, sx0, sy0, v, angle):
  Vx0 = int(v) * cos(angle)
  Vy0 = int(v) * sin(angle)
  sx, sy, vx, vy = 0, 0, 0, 0
  for t in range(t):
      sx = Vx0*t
      sy = (-G/2)*(t**2) + Vy0*t
      vx = Vx0
      vy = -G*t + Vy0
  return sx, sy, vx, vy

def main(v0, angle):
  Vx0 = int(v0)*cos(angle)
  Vy0 = int(v0)*sin(angle)
  tmax = int(2 * Vy0 / G)
  plt.title(f'{Vx0}, {Vy0}')
  for t in range(tmax+2):
      sx, sy, vx, vy = pos(t, 0, 0, v0, angle)
      plt.scatter(sx, sy, color = 'blue')
      plt.quiver(sx, sy, vx, vy, color = 'red', angles = 'xy', scale_units = 'xy', scale
                 = 1.4)
  plt.grid()
  plt.show()



if __name__ == '__main__':
  args = sys.argv
  assert len(args) == 3
  _, v0, angle = args
  main(v0, int(angle) * pi / 180)
