USB to TTL
===========
::

  sudo usermod -aG dialout $USER

  (doc) luis@spinoza ~/g/ (master)$ id |tr ',' '\n'
  uid=1000(luis) gid=1000(luis) groupes=1000(luis)
  4(adm)
  20(dialout)  <== needed
  24(cdrom)
  27(sudo)
  30(dip)
  46(plugdev)
  113(bluetooth)
  122(lpadmin)
  134(lxd)
  135(sambashare)
  142(libvirt)
  999(docker)
  (doc) luis@spinoza ~/g/ (master)$


On some other console::

  - sudo dmesg -W
  - usb-to-ttl out/in we get dev name dev/ttyACM0

With minicom::

  $ cat .minirc.zero
     pu port             /dev/ttyACM0
     pu baudrate         1500000
     pu bits             8
     pu parity           N
     pu stopbits         1
     pu rtscts           No
  luis@spinoza ~ $
  $ minicom -w -t xterm -l -R UTF-8
  luis@spinoza ~ $ minicom -w -t xterm -l -R UTF-8 zero

with cu::

  luis@spinoza ~ $ sudo chmod 666 /dev/ttyACM0  # needed sometimes
  luis@spinoza ~ $ sudo cu -s 1500000 -l /dev/ttyACM0
  Connected.
  orangepi@orangepi3b:~$ ls
  orangepi@orangepi3b:~$ # to exit Enter and then ~.Enter
  orangepi@orangepi3b:~$ ~.

with screen (the best so far)::

  luis@spinoza ~ $ screen /dev/ttyACM0 1500000

https://hallard.me/raspberry-pi-read-only/
https://k3a.me/how-to-make-raspberrypi-truly-read-only-reliable-and-trouble-free/

Radxa Zero 3E
==============

- with 16GB GigaStone blkid
- https://docs.radxa.com/en/zero/zero3/getting-started/install-os?Platform=Linux
- only xfce image working ;(

  - https://github.com/radxa-build/radxa-zero3/releases/tag/b6
  - radxa-zero3_debian_bullseye_xfce_b6.img.xz
  - check the microSD with f3, write the microSD with balena-etcher
  - root@spinoza:~# f3probe /dev/sda
  - boot, (run screen /dev/... upfront)
  - time nmap -sP 192.168.3.0/24 to discover IP
  - ssh radxa@192.168.3.30
  - apt-get upgrade, update
  - apt-get install git tig gdu plocate nmap tree console-data
  - to fix the FS, mount microSD and fsck.ext4 -y /dev/sda3


Orange Pi 3B
=============

- http://www.orangepi.org/html/hardWare/computerAndMicrocontrollers/details/Orange-Pi-3B.html

- https://wiki.radxa.com/Rock3/Debian
  https://wiki.debian.org/ArmPorts
  https://wiki.debian.org/
  https://sd-card-images.johang.se/boards/orange_pi_3.html
  Rockchip RK3566 quad-core 64-bit processor
  https://community.home-assistant.io/t/installing-home-assistant-supervised-on-orange-pi-3b/634021
  https://forums.raspberrypi.com/viewtopic.php?t=249449
  https://github.com/w568w/u-boot-orangepi-3b/issues/1

SSD
====

- Lex 32GB: Debian12 Rpy3B luis: luis
- Lex 32GB: Rapsbian Rpy3B luis: luis
- Lex 32GB: D12 radxa Zerro3E luis: sshkey IP:xx
- xxx: ubuntu20.04 neo3pi  luis: sshkey IP: 192.168.1.77
- Scandisk 32GB raspberrypi4 zero:zero, luis:luis, Rapsbian


Hardware
=========
$ cat /sys/firmware/devicetree/base/model
- RaspberryPi3mB Cortex A53 4x38 bogomips, RAM: 1 GBq
- RaspberryPi4 Cortex  4x bogomips, RAM: x GBq


Raspberry Pi3B
==================

Zero3e
- https://docs.radxa.com/en/zero/zero3/getting-started/download?model=zero-3e
  - https://docs.radxa.com/en/rock3
  - https://github.com/radxa-build/radxa-zero3/releases/tag/b6

- deb11: radxa-zero3_debian_bullseye_xfce_b6.img.xz

Fix ssh file_system
--------------------
Identify filesystem::

  root@spinoza:~/fix_ssd# file -sL /dev/sda*
  /dev/sda:  DOS/MBR boot sector; partition 1 :
  /dev/sda1: DOS/MBR boot sector, code offset 0x58+2, OEM-ID "mkfs.fat", sect
  /dev/sda2: DOS/MBR boot sector, code offset 0x58+2, OEM-ID "mkfs.fat", sect
  /dev/sda3: Linux rev 1.0 ext4 filesystem data, UUID=e4dc7fb3-d6e7-4a53-8267-0b5688ac4ff3, volume name "rootfs"
  root@spinoza:~/fix_ssd#

Fix ext4::

  root@spinoza:~/fix_ssd# fsck.ext4 /dev/sda3 -y

Check if errors on vfat::

  # fsck.vfat -n /dev/sda2
  errors ...
  #

Fix error on vfat::

  # fsck.vfat -l -v -a -t /dev/sda2

Fixed::

  # fsck.vfat -n /dev/sda2
  fsck.fat 4.2 (2021-01-31)
  /
    Has a large number of bad entries. (11/25)
    Not dropping root directory.
  /dev/sda2: 25 files, 1/76643 clusters
  #


