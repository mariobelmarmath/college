Todo
====

- secure API
- faire fonctionner le devops de mt_appli clone_copy_systemd_nginx.sh
- start new gol in test driven development

Done
====

- merger tous les sphinx en un ou deux sphinx unique
- gol use numpy.array instade of P, S, ...
- fix main gol algo overflow left and top
- put basicauthentification on host/private subfolders page nginx
- faire un jeux de la vie en python add tkinter
oo
  - load a file https://www.pythontutorial.net/tkinter/tkinter-open-file-dialog/
  - https://www.pythontutorial.net/tkinter/tkinter-button/
- faire l'UX de mtodo Cf. todp app Mario.pdf en PJ en vueJS ou en HTMX
- to execute BASH code (default is only SH) on Makefiles e.g. {1..10..2} use in Makefile SHELL := /bin/bash
- (maybe unless) Setup shellinabox to have access to RPi without the need of putty from Windows

  - e.g. ``sudo apt-get install shellinabox`` NOT Needed
-(maybe unless)  Alternative solution `python-webssh
<https://www.digitalocean.com/community/tutorials/how-to-connect-to-a-terminal-from-your-browser-using-python-webssh>`__

  - not needed
  - e.g. ``sudo pip3 install webssh``

- install route to mario@pixel to deploy
- install pyenv
- Configure Ngnix to have college/sphinx online
- Configure Ngnix to have a https://...

  - use `install SSL certificate
    <https://www.digitalocean.com/community/tutorials/how-to-secure-nginx-with-let-s-encrypt-on-ubuntu-22-04>`__
  - test with e.g.::

    $ curl https://anime.in-girum.net
    ...
    <h1>Hello world</h1>
    $
    $ curl --verbose https://anime.in-girum.net |& grep 'SSL cer'
    *  SSL certificate verify ok.
    $

- configure new repo git: https://gitlab.com/mariobelmarmath/rpiweb.
- Configure Ngnix to have a very simple webpage online

  - test with e.g.::

    $ curl http://anime.in-girum.net
    ...
    <h1>Hello world</h1>
    $
  - use `install ngnix <https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-ubuntu-22-04>`__
  - configure Freebox to forward 80, 443 to Raspberry Pi

- setup DNS sub domain on gandi

  - test with e.g.::

    $ dig anime.in-girum.net | grep -A1 "ANSWER SECTION"
    ;; ANSWER SECTION:
    anime.in-girum.net.	246	IN	A  78.193.211.111
    $
