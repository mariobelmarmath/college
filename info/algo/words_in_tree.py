from dataclasses import dataclass
from pprint import pprint
import ipdb
import logging
from make_tree import l, add_node

@dataclass
class Node:
    word: str
    child: list["Node"]

tree = Node('', [])
for i in l:
    tree = add_node(i, tree)

def words_in_tree(tree: Node = tree) -> list[str]:
    queue = []
    list_of_nodes = []
    words = []
    for child in tree.child:
        queue.append(child)
    for subnode in queue:
        for child in subnode.child:
            word = Node(word=subnode.word,
                        child=[child])
            list_of_nodes.append(word)
    for word_node in list_of_nodes:
        word = rec_get_str(word_node, '')
        words.append(word)
    return words


def rec_get_str(node: Node, start: str) -> str:
    if node.word == '.':
        # print(start)
        return start
    start = start + node.word
    node = node.child[0]
    return rec_get_str(node, start)


def show_word_per_word(word: str, source: Node = None) -> list[str]:
    if not source:
        source = Node('', [])
    tree_word = add_node(word, source)
    print(words_in_tree(tree_word))
    return tree_word
