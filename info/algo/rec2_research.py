from dataclasses import dataclass
from pprint import pprint
import ipdb
import logging

@dataclass
class Node:
    word: str
    child: list["Node"]


def adjust_word(word, word_node, is_leaf):
    """
    adjust_word('oshi', 'oshi no ko') --> True
    adjust_word('oshi  ', 'oshi no ko') --> False
    adjust_word('one piece r', 'one piece') --> False
    adjust_word('oshi', 'one piece')) --> False
    """
    s = ''
    if len(word) > len(word_node) and is_leaf:
        return False
    for idx, i in enumerate(list(word)):
        if idx < len(word_node):
            s += i
    if word_node.startswith(s):
        return True
    return False


def rec2_research(word: str, tree: Node) -> str:
    if not word:
        logging.warning('Empty word')
        return None
    if tree.child == []:
        logging.warning('Empty tree')
        return None
    return __rec2_research(word, tree)


def __rec2_research(word: str, tree: Node) -> str:
    is_find = False
    if tree.child == []:
        return tree.word
    for i, child in enumerate(tree.child):
        branch = tree.child[i]
        is_leaf = False if branch.child else True
        if adjust_word(word, branch.word, is_leaf):
            new_tree = branch
            is_find = True
    if not is_find:
        logging.warning('Word does not found')
        return None
    return __rec2_research(word, new_tree)
