from pprint import pprint
import pickle
from main import Node
from make_tree import add_node
import sys

source = Node('', [])

if __name__ == "__main__":
    lines = sys.stdin.readlines()
    for line in lines:
        line = line.replace('\r', '')
        line = line.replace('\n', '')
        print(repr(line))
        source = add_node(line, source)
    pprint(source)
    with open('mypicklefile', 'wb') as f1:
        pickle.dump(source, f1)
