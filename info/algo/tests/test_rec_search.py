from ..rec_search import rec_search, Node

tree = Node(word='',
         child=[Node(word='bleach', child=[]),
                Node(word='a',
                     child=[Node(word='akame ga kill', child=[]),
                            Node(word='astro boy', child=[])]),
                Node(word='o',
                     child=[Node(word='oshi no ko', child=[]),
                            Node(word='one piece',
                                 child=[Node(word="one piece", child=[]),
                                        Node(word='one piece red', child=[]),
                                        Node(word='one piece blue', child=[])])])])


# def test_classic():
#     assert rec_search(tree, 'akame') == 'akame ga kill'
#     assert rec_search(tree, 'one piece r') == 'one piece red'
#     assert rec_search(tree, ' one piece r') == None
#     assert rec_search(tree, 'z') == None
#     assert rec_search(tree, '134 Tz') == None
#     assert rec_search(tree, 'qdfmqdkfsmqkjm') == None
#     assert rec_search(tree, 'qdfmqdkfsmqkjm') == None

# def test_corner_case():
#     assert rec_search(tree, '') == None
#     assert rec_search(Node(word='', child=[]), 'one ') == None
#     assert rec_search(Node(word='', child=[]), '') == None
