from ..rec_search import rec2_research, Node

tree = Node(word='',
         child=[Node(word='bleach', child=[]),
                Node(word='a',
                     child=[Node(word='akame ga kill', child=[]),
                            Node(word='astro boy', child=[])]),
                Node(word='o',
                     child=[Node(word='oshi no ko', child=[]),
                            Node(word='one piece',
                                 child=[Node(word="one piece blue", child=[]),
                                        Node(word='one piece red', child=[]),
                                        Node(word='one piece', child=[]),
                                        ])
                            ])
                ])


def test_classic():
    assert rec2_research('akame', tree) == 'akame ga kill'
    assert rec2_research('one piece r', tree) == 'one piece red'
    assert rec2_research(' one piece r', tree) == None
    assert rec2_research('one piece', tree) == 'one piece'
    assert rec2_research('z', tree) == None
    assert rec2_research('134 Tz', tree) == None
    assert rec2_research('qdfmqdkfsmqkjm', tree) == None
    assert rec2_research('qdfmqdkfsmqkjm', tree) == None

def test_corner_case():
    assert rec2_research('', tree) == None
    assert rec2_research('one ', Node(word='', child=[])) == None
    assert rec2_research('', Node(word='', child=[]), ) == None
