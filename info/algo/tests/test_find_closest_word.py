from ..find_closest_word import word_completion

repertoire = {
    "bleach": {"leaf": 1},
    "a": {"leaf": 0,
          "child": {"akame ga kill": {"leaf": 1},
                    "astro boy": {"leaf": 1}},
          },
    "o": {"leaf": 0,
          'child': {"oshi no ko": {"leaf": 1},
                    "one piece": {"leaf": 1,
                                 "child": {'one piece red': {"leaf": 1},
                                           'test': ('test', 133),
                                           'one piece blue': {"leaf": 1},}
                                 },
                    }
},
}

def test_classic():
    assert word_completion('akame', repertoire) == 'akame ga kill'
    assert word_completion('one piece r', repertoire) == 'one piece red'
    assert word_completion(' one piece r', repertoire) == None
    assert word_completion('z', repertoire) == None
    assert word_completion('134 Tz', repertoire) == None
    assert word_completion('qdfmqdkfsmqkjm', repertoire) == None
    assert word_completion('qdfmqdkfsmqkjm', repertoire) == None

def test_corner_case():
    assert word_completion('', repertoire) == None
    assert word_completion('one ', {}) == None
