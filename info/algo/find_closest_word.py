import ipdb
import logging

def word_completion(word: str, rep: dict) -> str:
    if not rep:
        logging.warning('empty dict')
        return None

    if not word:
        logging.warning('empty word')
        return None

    str_start = ''
    for key in rep.keys():
        if key.startswith(word[0]):
            str_start = key

    if not str_start:
        logging.info('word not found')
        return None

    n = 1
    print(' a')
    while rep[str_start]['leaf'] != 1 or (len(word) > len(str_start)):
        n += 1
        # ipdb.set_trace()
        rep = rep[str_start]['child']

        if not word:
            logging.warning('empty word')
            return None

        for key in rep.keys():
            if len(key) >= len(word):
                if key.startswith(word):
                    str_start = key
            elif len(key) < len(word):
                if key.startswith(word[:n]):
                    str_start = key

        if not str_start:
            logging.info('word not found')
            return None

    return str_start
