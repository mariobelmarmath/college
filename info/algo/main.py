from find_closest_word import word_completion
import logging
from ipdb import set_trace
from pprint import pprint
from dataclasses import dataclass
# recherche dans une liste


@dataclass
class Node:
    # word: str = ""
    # child: list["Node"] = []
    word: str
    child: list["Node"]

l = [3, 5, 10, 13, 2, 4, 12]

def search(l: list, number: int) -> int:
    for idx, num in enumerate(l):
        if num == number:
            return idx

l[search(l, 12)] == 12

def search2(l: list, number: int) -> int:
    length = len(l)//2
    if number in l[:length]:
        search2(l[:length, number])
    else:
        search2(l[length:], number)


repertoire = {
    "bleach": {"leaf": 1},
    "a": {"leaf": 0,
          "child": {"akame ga kill": {"leaf": 1},
                    "astro boy": {"leaf": 1}},
          },
    "o": {"leaf": 0,
          'child': {"oshi no ko": {"leaf": 1},
                    "one piece": {"leaf": 1,
                                 "child": {'one piece red': {"leaf": 1},
                                           'one piece blue': {"leaf": 1},}
                                 },
                    }
},
}


tree = Node(word='',
         child=[Node(word='bleach', child=[]),
                Node(word='a',
                     child=[Node(word='akame ga kill', child=[]),
                            Node(word='astro boy', child=[])]),
                Node(word='o',
                     child=[Node(word='oshi no ko', child=[]),
                            Node(word='one piece',
                                 child=[Node(word="one piece", child=[]),
                                        Node(word='one piece red', child=[]),
                                        Node(word='one piece blue', child=[])])])])

