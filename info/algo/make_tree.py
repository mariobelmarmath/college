from pprint import pprint
import ipdb
import pickle
from main import Node
import sys

def word_to_node(word: str) -> Node:
    s = list(word + '.')
    s.reverse()
    for idx, letter in enumerate(s):
        if idx == 0:
            new_node = Node(letter, [])
        else:
            new_node = Node(letter, [new_node])
    return new_node


def add_node(word: str, source: Node) -> Node:
    print(word)
    tree = source
    counter = 0
    is_old_node = False
    # ipdb.set_trace()
    while word[counter] in [n.word for n in tree.child]:
        print('33333333333')
        print(word[counter])
        print(counter)
        for node in tree.child:
            # counter += 1
            if node.word == word[counter]:
                tree = node
                old_node = tree
                # old_node = node
                is_old_node = True
                counter += 1
    if is_old_node:
        print('111111111111')
        old_node.child.append(word_to_node(word[counter:]))
    else:
        print('222222222222')
        source.child.append(word_to_node(word[counter:]))
    return source


source = Node('', [])
l = [
     "oshi no ko",
     "akame ga kill",
     "one piece",
     "one piece red",
     "one piece blue",
     "bleach",
     "astro boy"
     ]


if __name__ == "__main__":
    # lines = sys.stdin.readlines()
    # for line in lines:
    #     line = line.replace('\r', '')
    #     line = line.replace('\n', '')
    #     print(line)
    #     source = add_node(line, source)
    # with open('mypicklefile', 'wb') as f1:
    #     pickle.dump(source, f1)
    for i in l:
        source = add_node(i, source)
    pprint(source)

