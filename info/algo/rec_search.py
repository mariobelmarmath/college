from pprint import pprint
from dataclasses import dataclass
from main import Node
import ipdb
import pickle
import logging

@dataclass
class Node:
    # word: str = ""
    # child: list["Node"] = []
    word: str
    child: list["Node"]

with open('mypicklefile', 'rb') as f1:
    source = pickle.load(f1)


def end_word(source):
    s = ""
    while not source.child[0].word == ".":
        for child in source.child:
            s += child.word
            source = child
            break
    return s


# fonction chapeau
def rec_research(word, source):
    if not word:
        logging.warning('Empty word')
        return None
    if source.child == []:
        logging.warning('Empty tree')
        return None
    return __rec_research(word, source)


def __rec_research(word, source, n=0):
    if source.child[0].word == ".":
        if len(word) == n:
            return word
    if n >= len(word):
        return word + end_word(source)
    is_find = False
    for child in source.child:
        if word[n] == child.word:
            is_find = True
            source = child
    if not is_find:
        logging.warning("didn't find")
        return None
    n += 1
    return __rec_research(word, source, n)
