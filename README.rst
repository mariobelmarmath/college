Main Mario repo
test from y13 2 
===============

How to copy commits from some other repository ? (e.g. a local one)
-------------------------------------------------------------------

Local repo with 7 commits::

  (zen) luis@spinoza:~/data/anime$ git log --format=oneline
  63d6e6e4d3265b121c03e4f4594308ab89230d74 (HEAD -> master) mv all files into anime
  d6d1b982359fe4675a1fc1062235339112cc53f6 add fandom URL
  d1cd42d77832e0ccbc74114fb0157f4164bbc2b3 add anim pyenv virtualenv
  7cec3468ef2b6198b38fb5f2fedf7126275b1d3f clean txt and Makefile
  d688b76479825adf58c27ecf98da65231c9f6eb0 add Makefile
  5a9924a507d4706afc32f0e213cddba9f1952e15 txt version with html2markdown
  158c383b080e0e421d6f3998512c4bbc9e2e0916 initial commit with raw html
  (zen) luis@spinoza:~/data/anime$ git ls-files
  anime/.python-version
  anime/Makefile
  anime/a.html
  anime/a.txt
  (zen) luis@spinoza:~/data/anime$

Use ``git remote add <name> <URI>`` to add them in repo::

  (doc) luis@spinoza:~/g/alien/college$ git remote add anime_local ~/data/anime
  (doc) luis@spinoza:~/g/alien/college$ git fetch anime_local
  remote: Énumération des objets: 21, fait.
  remote: Décompte des objets: 100% (21/21), fait.
  remote: Compression des objets: 100% (18/18), fait.
  remote: Total 21 (delta 5), réutilisés 0 (delta 0), réutilisés du pack 0
  Dépaquetage des objets: 100% (21/21), 223.73 Kio | 11.19 Mio/s, fait.
  Depuis /home/luis/data/anime
   * [nouvelle branche] master     -> anime_local/master
  luis@spinoza:~/g/alien/college$

  (doc) luis@spinoza:~/g/alien/college$ git checkout -b anime_local anime_local/master
  La branche 'anime_local' est paramétrée pour suivre la branche distante 'master' de 'anime_local' en rebasant.
  Basculement sur la nouvelle branche 'anime_local'
  luis@spinoza:~/g/alien/college$

  luis@spinoza:~/g/alien/college$ git branch -a
  * anime_local
    master
    remotes/anime_local/master
    remotes/origin/master
  luis@spinoza:~/g/alien/college$

  (doc) luis@spinoza:~/g/alien/college$ LANG=en git merge anime_local
  fatal: refusing to merge unrelated histories
  (doc) luis@spinoza:~/g/alien/college$ LANG=en git merge anime_local --allow-unrelated-histories
  Merge made by the 'ort' strategy.
   anime/.python-version |     1 +
   anime/Makefile        |    11 +
   anime/a.html          | 14856 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
   anime/a.txt           |  3365 +++++++++++++++++
   4 files changed, 18233 insertions(+)
   create mode 100644 anime/.python-version
   create mode 100644 anime/Makefile
   create mode 100644 anime/a.html
   create mode 100644 anime/a.txt
  (doc) luis@spinoza:~/g/alien/college$

  (doc) luis@spinoza:~/g/alien/college$ git ls-files | grep anime
  anime/.python-version
  anime/Makefile
  anime/a.html
  anime/a.txt
  info/html/html5/img/anime.jpg
  (doc) luis@spinoza:~/g/alien/college$

  (doc) luis@spinoza:~/g/alien/college$ git log --pretty=oneline --abbrev-commit --all --decorate --graph | head -11
  *   4879e77 (HEAD -> master) Merge branch 'anime_local'
  |\  
  | * 63d6e6e (anime_local/master, anime_local) mv all files into anime
  | * d6d1b98 add fandom URL
  | * d1cd42d add anim pyenv virtualenv
  | * 7cec346 clean txt and Makefile
  | * d688b76 add Makefile
  | * 5a9924a txt version with html2markdown
  | * 158c383 initial commit with raw html
  * 6e249ac (origin/master, origin/HEAD) use openai's api project works
  * f41bcb4 change conf.py of portfolio
  (doc) luis@spinoza:~/g/alien/college$ 

