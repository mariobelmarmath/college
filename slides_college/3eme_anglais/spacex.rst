:css: .my.css

.. footer::

  Mario

.. title:: SpaceX

----

SpaceX
======

.. figure:: img/first_img.jpg
   :width: 1000px

----

Introduction
------------

- Name also **Space Exploration Technologies Corp**:

  - American:

    - aerospace manufacturer
    - space transportation services

- Founded on 6 May 2002 by **Elon Musk** and **Tom Mueller** (curent CTO of Propulsion
  division).

- HDs: Hawthorne, California, United States (west).

- Already developed launch vehicles and rocket engines.

----

Goal and Mission
----------------

- Didn't find cheap space craft and he realized that he can start a company to realize
  his dream
- Musk wants to make possible for humans to live on other planets and first Mars.
- For now SpaceX develop space transportations to make it faster and cheaper.
- And also create russable aircraft never did successfuly.

----

Rockets
-------

.. figure:: img/spacecraft.jpg
   :alt: Text


----

Rockets and spacecrafts
-----------------------
Falcon 9
~~~~~~~~

- Two stage rocket
- Reliable & safe transport of satellites
- Carried Dragon spacecraft
- First reache orbit **and** recover spacecraft

----


Rockets and spacecrafts
-----------------------
Falcon Heavy
~~~~~~~~~~~~

- Ability to lift into orbit over 53 metric tons
- Carry humans into space and restores flying mission with crew
- Carry the Dragon spacecraft
- Most Powerful

----

Rockets and spacecrafts
-----------------------
Dragon
~~~~~~

- Free-flying spacecraft
- Deliver both cargo and people to orbit
- First commercial spacecraft to deliver cargo to the ISS

.. ----

.. Business model
.. --------------

.. - Mass producing spacecraft company.
.. - Designs, manufactures, deliveries spacecraft.
.. - So they did all the themself: from the concept until the sale.
.. - The Motto is: Simplicity, low cost and reliability go hand in hand.
.. - Simple proven design with a  primary focus and reliability.
.. .. trad: une conception simple et éprouvée, axée sur la fiabilité

----

Innovation and Achievment
-------------------------

- First privately funded company to successfully launch, orbit, and **revocer** a
  spacecraft.
- First company to send a spacecraft to the ISS
- First landing of an orbital rockets's first stage on land on an ocean platform.
- First relaunch and landing of a used orbital rocket.
- Reduce the cost of spacecraft.

---- 

Don't speak about
-----------------

We don't speak about:
~~~~~~~~~~~~~~~~~~~~~

 - Process manufacturing
 - Challenge for SpaceX
 - Benefit
 - Etc...

---- 

Conclusion
----------

- **First privately funded company to successfully launch, orbit, and revocer a
  spacecraft.**
- First company to send a spacecraft to the ISS
- Musk wants to make possible for humans to live on other planets and first Mars.

