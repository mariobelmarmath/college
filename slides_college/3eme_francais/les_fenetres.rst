:css: .my.css

.. footer::

  Mario

.. title:: Les fenêtres

Les fenêtres - Charles Baudelaire
=================================

.. figure:: img/front_cover2.jpeg
   :width: 400px

-----

Sommaire
--------

 #. Lecture du poème
 #. Introduction
 #. Analyse littéraire
 #. Analyse des figures de styles
 #. Oeuvre choisie

-----

Lecture du poème
----------------

::

    Celui qui regarde du dehors à travers une fenêtre ouverte, ne voit jamais autant de
  choses que celui qui regarde une fenêtre fermée. Il n’est pas d’objet plus profond, plus
  mystérieux, plus fécond, plus ténébreux, plus éblouissant qu’une fenêtre éclairée d’une
  chandelle. Ce qu’on peut voir au soleil est toujours moins intéressant que ce qui se
  passe derrière une vitre. Dans ce trou noir ou lumineux vit la vie, rêve la vie, souffre
  la vie.
    Par delà des vagues de toits, j’aperçois une femme re, ridée déjà, pauvre, toujours
  penchée sur quelque chose, et qui ne sort jamais. Avec son visage, avec son vêtement,
  avec son geste, avec presque rien, j’ai refait l’histoire de cette femme, ou plutôt sa
  légende, et quelquefois je me la raconte à moi-même en pleurant.
    Si c’et été un pauvre vieux homme, j’aurais refait la sienne tout aussi aisément.
    Et je me couche, fier d’avoir vécu et souffert dans d’autres que moi-même.
    Peut-être me direz-vous : « Es-tu sr que cette légende soit la vraie ? » Qu’importe
  ce que peut être la réalité placée hors de moi, si elle m’a aidé à vivre, à sentir que
  je suis et ce que je suis ?

-----

Introduction
------------

.. figure:: img/charles_baudelaire.jpeg
   :width: 200px

Les fenêtres de Charles Baudelaire, 1869, Petits poèmes en prose

- Précurseurs du symbolisme et de la modernité poétique.
- Connu pour le recueil de poèmes "Les Fleurs du mal" et pour "Petits poèmes en prose".
- Peu connu à son époque.

Poème en prose. Il n'y a donc que des rimes internes


-----

Analyse littéraire
------------------

Structure du poème
~~~~~~~~~~~~~~~~~~

Poème en prose composé de cinq courts paragraphes. Deux paragraphes monophrastiques
entre les deuxième et cinquième paragraphes.

- Dans le premier paragraphe l’auteur part du réel : une fenêtre éclairée d’une bougie.

- Le paragraphe suivant nous fait découvrir « une femme re » et « la légende » de sa
  vie. Le poème devient plus personnel.

- Le dernier paragraphe s’adresse au lecteur et le fait participer.

-----

Analyse littéraire
------------------

Premier paragraphe
~~~~~~~~~~~~~~~~~~

- Le sujet du tableau, forme du cadre d'un tableau: cinq adjectifs à valeur descriptive.
- Présentation de l'observateur et du sujet: avec une forme impersonnelle, affirmation
  forte
- Les antithèses en abondance
- La fenêtre du poème est un objet « mystérieux » et « fécond ».
- Les antithèses nous font entrevoir un mystère.

-----

Analyse littéraire
------------------

Deuxième paragraphe
~~~~~~~~~~~~~~~~~~~

- Le poète-peintre passe au "je":  présent d'énonciation ou présent d'habitude
- Le mot « fenêtre » n’apparat plus.
- Toute l’attention se porte sur la « femme mure ».
- Les caractéristiques de la femme constituent une légende et nourrissent l’imagination du
  poète.
- Il décrit ce qui se passe devant ses yeux: "vague de toits"
- Précision des détails sur le sujet.

-----

Analyse littéraire
------------------

Dernier paragraphe
~~~~~~~~~~~~~~~~~~

- Le lecteur intervient. Ce dernier se pose la question de la véracité de la légende.
- La réponse du poète fait encore appel au contraste, à l’antithèse.
- C’est ce qui importe et non la véracité de la légende.
- Retour à la vie quotidienne du poète : "je me couche"

-----

.. Représentation du paysage 1/2
.. -----------------------------

.. Paysage représenté 
.. ~~~~~~~~~~~~~~~~~~

.. - Premier paragraphe: fenêtre ouverte.
.. - Deuxième paragraphe: derrière la fenêtre, paysage de la ville, une femme:
..   Il décrit ce qu'il voit.
.. - Puis retour à la vie quotidienne.
.. - La fenêtre, le poème: devient une toile, un cadre.


.. Marques de l'énonciation
.. ~~~~~~~~~~~~~~~~~~~~~~~~

.. - Forme impersonnelle: 'celui qui', 'il', 'on'.
.. - Deuxième paragraphe: il passe à la première personne, il parle de sa propre expérience.

.. -----

.. Représentation du paysage 2/2
.. -----------------------------

.. Présentation du poète ?
.. ~~~~~~~~~~~~~~~~~~~~~~~

.. - Observateur attentif et passioné par les fenêtres l'atmosphère.
.. - Décrit sa propre fascination

.. .. Liens entre le paysage et le poète
.. .. ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. .. - Regard neutre mais mélancolique.
.. .. - Liens étroits: le paysage inspire beaucoup le poète.
.. .. - Les fenêtres sont un refuge pour son imagination et un échappatoire aux réalités de la
.. ..   vie.

.. Sentiments exprimés
.. ~~~~~~~~~~~~~~~~~~~

.. .. fenêtres

.. - **Fascination** , **émerveillement**, curiosité, mélancolie et solitude.
.. - Observe le monde avec une distance mais il est toujours capable de s'immerger dans les
..   scènes qu'il observe.
  
.. .. -----

.. .. Représentation du paysage 3/3
.. .. -----------------------------

.. -----

Analyse des figures de styles
-----------------------------

::

    Celui qui regarde du dehors à travers une fenêtre ouverte, ne voit jamais autant de
  choses que celui qui regarde une fenêtre fermée. Il n’est pas d’objet plus profond, plus
  mystérieux, plus fécond, plus ténébreux, plus éblouissant qu’une fenêtre éclairée d’une
  chandelle. Ce qu’on peut voir au soleil est toujours moins intéressant que ce qui se
  passe derrière une vitre. Dans ce trou noir ou lumineux vit la vie, rêve la vie, souffre
  la vie.
    Par delà des vagues de toits, j’aperçois une femme re, ridée déjà, pauvre, toujours
  penchée sur quelque chose, et qui ne sort jamais. Avec son visage, avec son vêtement,
  avec son geste, avec presque rien, j’ai refait l’histoire de cette femme, ou plutôt sa
  légende, et quelquefois je me la raconte à moi-même en pleurant.
    Si c’et été un pauvre vieux homme, j’aurais refait la sienne tout aussi aisément.
    Et je me couche, fier d’avoir vécu et souffert dans d’autres que moi-même.
    Peut-être me direz-vous : « Es-tu sur que cette légende soit la vraie ? » Qu’importe
  ce que peut être la réalité placée hors de moi, si elle m’a aidé à vivre, à sentir que
  je suis et ce que je suis ?

- **métaphore**: Les fenetres à des yeux ouverts sur le monde.
- **antithèse**: Oppose de la fiction et de la réalité.
- **énumération**: Décrit différents types de personnes que l'on peut voir à travers les
  fentres
- **assonnance**: Afin de donner de la musicalité au poème.
- **oxymore**: Décrit la lumière présente comme ténébreuse
- **répétition**: Décrit la vie à travers une fenetre fermée.

-----

Oeuvre choisie
--------------

La Chambre de Van Gogh à Arles
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. figure:: img/tableau.jpg

-----



.. Un poème construit comme un tableau
.. ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. **Le premier élément qui encadre le tableau**

.. - Présentation de l'observateur et du sujet : de façon générale, sur le ton d'une
..   affirmation forte : présent de vérité générale, termes péremptoires forts, répétition
..   "vit, vie, vie...vie".
.. - Le sujet du tableau : la fenêtre, meme forme que le cadre d'un tableau :
..   cinq adjectifs; mais aussi peut-être valeur esthétique et morale.
  

.. ---- 

.. Analyse littéraire
.. ------------------

.. Un poème construit comme un tableau
.. ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. **L'anecdote (2ème paragraphe)**

.. - Le poète-peintre passe au "je": présent d'énonciation au présent d'habitude,
..   il décrit ce qui se passe devant ses yeux : "vague de toits"
.. - Précision des détails sur le sujet 
.. - Retour à la vie quotidienne du poète : "je me couche".

.. ---- 

.. Analyse littéraire
.. ------------------

.. Une réflexion sur la condition humaine du poète ; un art poétique
.. ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. **Une définition de la poésie**

.. - Travail sur les lumières et les contrastes.
.. - Elle est expression d'une sensibilité tournée vers les autres, les pauvres ; poésie de
..   la souffrance, de sublimation de la souffrance.
.. - La poésie romanesque : Baudelaire écrit une "légende", une fiction et peut
..   faire varier ses personnages.
.. - La poésie est un dialogue, elle tisse des liens avec le lecteur, "mon
..   frère".

.. ---- 

.. Analyse littéraire
.. ------------------

.. Une réflexion sur la condition humaine du poète ; un art poétique
.. ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. **Un plaidoyer pour le poème en prose ?**

.. - Une progression proche de celle d'un sonnet: comme deux quatrains et presque deux
..   tercets.

.. - Mais un dépouillement, une fluidité qui ne doit rien aux rimes, à la régularité du
..   vers, mais au rythme intérieur de chaque phrase.


.. Analyse littéraire
.. ------------------

Conclusion
~~~~~~~~~~

Ce poème en prose est donc moderne d'abord par sa forme non versifiée, et également par
son sujet qui traite d'ce aussi au lyrisme de cette réflexion du poète sur son art et
sur lui-même.

Le poète peintre, ici, arrive a faire naitre un cadre de peinture grace aux contrastes
et aux figures de styles.

Le poète se définit ici comme un créateur de légendes qui prend en charge la misère du
monde pour la transformer : c'est bien le rôle que Baudelaire s'attribuait déjà dans
l'épilogue pour la 2ème édition des Fleurs du Mal : « Tu m'as donné ta boue et j'en ai
fait de l'or » disait-il au monde : en effet, comme un alchimiste, il transforme la boue
en or, le mal en beauté (<< fleurs >>).

