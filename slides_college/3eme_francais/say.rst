Analyse littéraire
------------------

Structure du poème
~~~~~~~~~~~~~~~~~~

Poème en prose composé de cinq courts paragraphes. Deux paragraphes monophrastiques
entre les deuxième et cinquième paragraphes.

- Dans le premier paragraphe l’auteur part du réel : une fenêtre éclairée d’une bougie.

- Le paragraphe suivant nous fait découvrir « une femme re » et « la légende » de sa
  vie. Dans ce deuxième paragraphe et les trois qui suivent le poème est écrit à la
  première personne. Le poème devient plus personnel. Ce n’est plus quelqu’un d’anonyme
  qui regarde « dans ce trou noir ou lumineux » mais un narrateur homodiégétique.

- Le dernier paragraphe s’adresse au lecteur et le fait participer à l’expérience, à la
  réflexion de l’observateur.

----

Poème en prose composé de cinq courts paragraphes. Deux paragraphes monophrastiques
entre les deuxième et cinquième paragraphes.

- Dans le premier paragraphe l’auteur part du réel : une fenêtre éclairée d’une bougie.

- Le paragraphe suivant nous fait découvrir « une femme re » et « la légende » de sa
  vie. Le poème devient plus personnel.

- Le dernier paragraphe s’adresse au lecteur et le fait participer.

Premier paragraphe
~~~~~~~~~~~~~~~~~~

.. - Premier paragraphe: fenêtre ouverte.
.. - La fenêtre, le poème: devient une toile, un cadre.

- Le sujet du tableau : la fenêtre, objet qui a la forme du cadre d'un tableau :
  cinq adjectifs, présentation emphatique ("plus"), à valeur descriptive et aussi
  affective mêlées ("mystérieux, fécond"), d'autres impressions visuelles
  ("ténébreux, éblouissant"), mais aussi peut-être valeur esthétique et morale.

- Présentation de l'observateur et du sujet : de façon générale ("celui qui"), avec
  une forme impersonnelle, sur le ton d'une affirmation forte : présent de vérité

Les antithèses abondent dès le premier paragraphe : fenêtre ouverte/fenêtre fermée
; ténébreux/éblouissant ; ce qu’on peut voir au soleil/ ce qui se passe derrière une
vitre ; trou noir ou lumineux. La fenêtre du poème est un objet « mystérieux » et
« fécond ». Les antithèses nous font entrevoir un mystère qui se cache derrière la
fenêtre. Dans « trou noir ou lumineux », l’antithèse est marquée au moyen de la
coordination « ou ».

----

- Le sujet du tableau, forme du cadre d'un tableau: cinq adjectifs à valeur descriptive.
- Présentation de l'observateur et du sujet: avec une forme impersonnelle, affirmation
  forte
- Les antithèses en abondance
- La fenêtre du poème est un objet « mystérieux » et « fécond ».
- Les antithèses nous font entrevoir un mystère.

Deuxième paragraphe
~~~~~~~~~~~~~~~~~~~

A partir du deuxième paragraphe le mot « fenêtre » n’apparat plus. Toute l’attention se
porte sur la « femme mure ». L’observateur est à présent celui qui aperçoit cette femme,
qui refait son histoire et se la raconte à lui-même. La vieillesse, la pauvreté et la
solitude d’une femme ou d’un « pauvre vieux homme » constituent une légende et
nourrissent l’imagination du poète. La légende de la femme mre fait pleurer le poète ;
il participe à sa souffrance.

Le texte offre un contraste entre le « moi » ou « moi-même » du poète et « d’autres que
moi-même », entre la réalité placée hors de lui et son être intime.

- ("j'aperçois" régulièrement) ? il décrit ce qui se passe devant ses yeux : "vague
  de toits" métaphore évocatrice ("vague de toits" => toits = mer)
- Précision des détails sur le sujet : un gros plan sur "femme mre", "ridée", sur
  son attitude ("penchée"), sur le "visage"

- Le poète-peintre passe au "je":  présent d'énonciation ou présent d'habitude
  ("j'aperçois" régulièrement) ?

----

- Le poète-peintre passe au "je":  présent d'énonciation ou présent d'habitude
- Le mot « fenêtre » n’apparat plus.
- Toute l’attention se porte sur la « femme mure ».
- Les caractéristiques de la femme constituent une légende et nourrissent l’imagination du
  poète.
- Il décrit ce qui se passe devant ses yeux: "vague de toits"
- Précision des détails sur le sujet.

Dernier paragraphe
~~~~~~~~~~~~~~~~~~

Dans le dernier paragraphe le narrateur fait intervenir le lecteur. Ce dernier se pose
la question de la véracité de la légende. La réponse du poète fait encore appel au
contraste, à l’antithèse. Cette légende placée hors du poète l’aide pourtant à vivre, à
savoir qu’il existe et ce qu’il est. C’est ce qui importe et non la véracité de la
légende.

----

- Le lecteur intervient. Ce dernier se pose la question de la véracité de la légende.
- La réponse du poète fait encore appel au contraste, à l’antithèse.
- C’est ce qui importe et non la véracité de la légende.
- Retour à la vie quotidienne du poète : "je me couche"

.. Représentation du paysage
.. =========================

.. paysage représenté
.. ------------------

.. .. - Premier paragraphe: fenêtre ouverte.
.. .. - Deuxième paragraphe: derrière la fenêtre, paysage de la ville, une femme:
.. ..   Il décrit ce qu'il voit.
.. .. - Puis retour à la vie quotidienne.
.. .. - La fenêtre, le poème: devient une toile, un cadre.

.. - Le sujet du tableau : la fenêtre, objet qui a la forme du cadre d'un tableau :
..   cinq adjectifs, présentation emphatique ("plus"), à valeur descriptive et aussi
..   affective mêlées ("mystérieux, fécond"), d'autres impressions visuelles
..   ("ténébreux, éblouissant"), mais aussi peut-être valeur esthétique et morale.

.. - ("j'aperçois" régulièrement) ? il décrit ce qui se passe devant ses yeux : "vague
..   de toits" panoramique, métaphore évocatrice ("vague de toits" => toits = mer)
.. - Précision des détails sur le sujet : un gros plan sur "femme mre", "ridée", sur
..   son attitude ("penchée"), sur le "visage"
.. - Retour à la vie quotidienne du poète : "je me couche"

.. Marques de l'énonciation
.. ------------------------

.. .. - Forme impersonnelle: 'celui qui', 'il', 'on'.
.. .. - Deuxième paragraphe: il passe à la première personne, il parle de sa propre expérience.

.. - Présentation de l'observateur et du sujet : de façon générale ("celui qui"), avec
..   une forme impersonnelle, sur le ton d'une affirmation forte : présent de vérité
..   générale, termes décisifs forts; "toujours" "ne (...) jamais", répétition "vit,
..   vie, vie...vie"

.. - Le poète-peintre passe au "je":  présent d'énonciation ou présent d'habitude
..   ("j'aperçois" régulièrement) ?

.. présentation du poète
.. ---------------------

.. .. - Observateur attentif et passioné par les fenêtres l'atmosphère.
.. .. - Décrit sa propre fascination

.. sentiments exprimés
.. -------------------

.. .. - **Fascination** , **émerveillement**, curiosité, mélancolie et solitude.
.. .. - Observe le monde avec une distance mais il est toujours capable de s'immerger dans les
.. ..   scènes qu'il observe.


.. Analyse littéraire

.. I. Un poème construit comme un tableau

.. 1. Le premier élément qui encadre le tableau

.. - Présentation de l'observateur et du sujet : de façon générale ("celui qui"), avec
..   une forme impersonnelle, sur le ton d'une affirmation forte : présent de vérité
..   générale, termes décisifs forts; "toujours" "ne (...) jamais", répétition "vit,
..   vie, vie...vie"
.. - Le sujet du tableau : la fenêtre, objet qui a la forme du cadre d'un tableau :
..   cinq adjectifs, présentation emphatique ("plus"), à valeur descriptive et aussi
..   affective mêlées ("mystérieux, fécond"), d'autres impressions visuelles
..   ("ténébreux, éblouissant"), mais aussi peut-être valeur esthétique et morale.

.. 2. L'anecdote (2ème paragraphe)

.. - Le poète-peintre passe au "je":  présent d'énonciation ou présent d'habitude
..   ("j'aperçois" régulièrement) ? il décrit ce qui se passe devant ses yeux : "vague
..   de toits" panoramique, métaphore évocatrice ("vague de toits" => toits = mer)
.. - Précision des détails sur le sujet : un gros plan sur "femme mre", "ridée", sur
..   son attitude ("penchée"), sur le "visage"
.. - Retour à la vie quotidienne du poète : "je me couche" => le présent est ici
..   clairement un présent d'habitude.

.. II. Une réflexion sur la condition humaine du poète ; un art poétique

.. 1. Une définition de la poésie

.. - "La poésie est comme une peinture" (Horace) : travail sur les lumières et les
..   contrastes ("ténébreux"/ "chandelle, éclairée" : sorte de clair obscur)
.. - Mais elle dépasse la réalité superficielle : elle est expression d'une
..   sensibilité tournée vers les autres, les pauvres ; poésie de la souffrance,
..   de sublimation de la souffrance.
.. - La poésie est aussi romanesque : à partir du réel, le poète invente,
..   comme un romancier, Baudelaire écrit une "légende", une fiction et peut
..   faire varier ses personnages : une "vieille femme", un "pauvre vieux
..   homme" ; satisfaction (orgueil ?) de cette faculté qu'il matrise, il peut
..   refaire le monde, la vie des hommes "tout aussi aisément".
.. - La poésie est un dialogue, elle tisse des liens avec le lecteur, "mon
..   frère".


.. 2. Un poète symboliste

.. - il y a une valeur symbolique de la fenêtre qui prend une portée
..   philosophique.
.. - Un moyen de corriger, de modifier notre conception habituelle du
..   monde : fenêtre fermée mieux que fenêtre ouverte.
.. - Un moyen de passer de l'extérieur de la réalité à une réalité
..   intérieure, celle de la condition humaine, du mystère des êtres.
.. - Un moyen pour le poète de mieux se connatre, de lutter contre le
..   spleen : cette fenêtre, paradoxalement, ouvre aussi sur le monde
..   intérieur du poète : "sentir [...] ce que je suis".
.. - Un moyen de prendre conscience de sa propre existence : "sentir
..   que je suis".


.. 3. Un plaidoyer pour le poème en prose ?

.. - Une progression proche de celle d'un sonnet, dont on a ici la
..   fragmentation : comme deux quatrains et presque deux tercets
..   avec chute de la dernière interrogation
.. - Mais un dépouillement, une fluidité qui ne doit rien aux
..   rimes, à la régularité du vers, mais au rythme intérieur de
..   chaque phrase : fréquentes répétitions, énumérations,
..   parallélismes, oppositions ; importance aussi des ruptures :
..   entre le premier et le deuxième paragraphe, dernier
..   paragraphe, changements de mode d'énonciation, de situation,
..   mélange entre généralisation et situation personnelle,
..   dialogue...







