:css: .my.css

.. footer::

  Mario, Léo, Maximilien et Come

.. title:: Earth song 


----

Earth Song ?
============


.. image:: img/img.jpeg
  :width: 400

- Earth Song est une musique apparut en 1995, par le célèbre Michael Jackson "AGE".


- Elle est connue pour être engagée et dénoncatrice de la déforestation, de la
  guerre et des atrocités commises par les humains sur la planète et eux
  mêmes.

----

:data-rotate: 90



Michael Jackson, Biographie
===========================

.. image:: img/michael_jackson.jpeg
  :width: 400

- Michael Jackson, né le 29 août 1958 à Gary (Indiana) et mort le 25 juin 2009
  à Los Angeles (Californie), est un auteur-compositeur-interprète,

- Huitième d'une famille de dix enfants (dont un meurt à la naissance)  il

- Dans les années 1980, il devient une figure majeure de la musique pop. Ses

----

:data-rotate: 180



Naissance de la musique 
=======================

.. image:: img/history.jpeg
  :width: 400

- Troisième single de l'album History, neuvième album.

- Sorti en novembre 1995.

- Michael Jackson y refléchissait depuis la tournée *Bad*, fin années 80.

- C'est le rêve qu'il a eu: la terre détruit par ce qu'on fait les humains.
 
- Questions environnementales commencent a prendre de l'importance:
  - Tchernobyl Ukraine 
  - tournée du chef Raoni avec le chanteur Sting qui dénoncent la déforestation.

- Single pas finalisé a temps(pour *Dangerous*), il apparaitra en 1995 dans *History*
  
----

:data-rotate: 90



Une musique engagée
===================

- *Earth song* accuse les hommes de la destruction de la planète.

- L'album "History", le titre "Earth Song" aborde le sujet de la déforestation
  des forêts tropicales, de la biodiversité en péril et de la pollution.

- Chanson écologiste Michael Jackson essaie de faire entendre la voix de la
  planète.
- Sortie en 1995, elle reste cependant toujours, et malheureusement,
  cruellement d’actualité ! 

----

:data-rotate: 90

Analyse de l'oeuvre musicale
============================


Musique et Parole 
-----------------

.. class:: substep 

  Click for music `Earth song <https://www.youtube.com/watch?v=XAi3VTSdTxU>`__

- Cette chanson fut la dernière à être enregistrée pour l'album history. 

- Incorpore des éléments de blues, de gospel et d'opéra.

- A été enregistrée entre 1988 et 1995. Origininalement "What about us" et fut retravaillé en 1989.

- Certaines retouches faites par David Foster et  Bill Bottrell   

- Voici quelques exemples de modifications : 

   - Des notes de guitare et de basse furent ajoutées

   - Michael modifia des paroles et changea la hauteur de sa voix durant la partie finale.

- Incorpore des éléments de blues, de gospel et d'opéra.

- A été enregistrée entre 1988 et 1995. Origininalement "What about us" et fut retravaillé en 1989.

- Certaines retouches faites par David Foster et  Bill Bottrell   

- Voici quelques exemples de modifications : 

   - Des notes de guitare et de basse furent ajoutées

   - Michael modifia des paroles et changea la hauteur de sa voix durant la partie finale.

---- 

Video 
------

- Le clip fut tourné en Croatie, en Tanzanie, au Brésil et à New-York, pour
  illustrer le thème et réalisé par Nicholas Brandt et fut nomminé aux Grammy awards en 1997.
