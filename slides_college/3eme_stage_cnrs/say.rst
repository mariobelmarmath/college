Comment utiliser l'intelligence pour repérer des mots dans un texte ?
=====================================================================

.. figure:: img/front-cover.png

----

Introduction
============

NLP para

  - **N** atural
  - **L** anguage
  - **P** rocessing

y en español:

  - **P** rocesamiento de
  - **L** enguaje
  - **N** natural

El idioma natural es el conjunto de los lenguajes hablados por los humanos.


El NLP es una rama de la inteligencia artificial desarollo hace diez años(il y a une dizaine d'années).
Su objectivo  es que los maquinas pueden comprender, generar y manipular las idiomas
humanas pero sobre todo que pueden analizar y tratar los textos para interactuar con
nosotros.

.. Son but est que les machines puissent comprendre, générer et manipuler les langues humaines mais
.. surtout qu'ils puissent les analyser et de traiter des textes afin d'intéragir avec
.. nous, l'humain.

----

Sommaire
========

1. NLP - Fonctionnement
-----------------------
Nous parlerons de la manière dont fonctionne le NLP

2. NLP - Travaux
----------------
Nous parlerons des différents projets que j'ai pu faire lors de mon stage en décembre.


----

NLP - Fonctionnement
====================

Méthode d'apprentissage, théorie
--------------------------------

Il existe plusieurs familles d’apprentissages utilisées pour les
librairies de NLP qui ont évolué avec le temps. Cependant celle qui a le plus
contribué est celle du machine learning (dont fait partie le Deep learning).

- Méthodes basées sur des règles

    - Résout des problèmes spécifiques (supprimer les spam des boites mail à l'aide de
      mot clés 'promo')
    - Rapidement inefficace face à la complexité du langage humain.
- Modèles de Machine Learning

    - Une compréhension avancée du langage
    - Utilise d'autre procédés mathématique et statistiques(longueur des phrases,
      occurrence de mots spécifiques)
- Modèles de Deep Learning

    - Beaucoup plus complexes
    - Intègrent une énorme quantité de données pour essayer de créer un système proche
      de notre système neuronal

----

NLP - Fonctionnement
====================
Librairies et capacités
-----------------------

J'utilise la librairie NLP appelés Spacy, qui est assez récente mais plus rapide.
Voici ce dont est capable Spacy:

- La **tokenisation** ou **word segmentation**: (qui est aussi considérer comme une
  pré-phase) découper une phrase en plusieurs pièces, tokens.

 - Ex: 'bonjour les amis' -> 'bonjour', 'les', 'amis'

- **lemmatization**: donner la forme canonique du mot, celle de base.

  - Ex: 'trouvaient' -> 'trouver'

- **P.O.S tagging**: à partir de l'endroit où se trouve le verbe dans la phrase on
  assigne au mot(token) sa nature.

  - Ex: 'l'enfant mange une pomme' -> l'enfant : sujet | mange : verbe | etc..

- **dependency parsing**: dépendance à d'autre mots dans la phrase (c'est aussi le
  contexte, un mot peut changer le sens d'un autre mot).

  - Ex: le mot 'que' peut signifier plein de choses

Grace à toutes ces étapes nous serons capables de produire un code qui, par exemple,
trouve le nombre de fois qu’apparaît un mot dans un texte.

----

NLP - Travaux
=============

Projets
-------

Détection de verbes
  - Ce programme consiste a retrouver les verbes dans un texte. Cet exercice sert
    d'exercice afin de de se familiariser avec le NLP

J'utiliserai le language de programmation python, et la librarie spacy pour utiliser le
NLP

----

NLP - Travaux
=============

Premier projet: Détection des verbes
---------------------------------------

Les différentes étapes de ce programme sont:

  - étape 1: récupérer le fichier et le rendre compréhensible pour l'ordinateur
  - étape 2: récupérer les verbes du texte

Etape 1: Récupérer le fichier et le rendre compréhensible
-------------------------------------------------------------

Je crée une **fonction** qui transforme le fichier ".txt" en texte lisible par le language de
programmation.

 - Je lui donne le 'path' (le chemin) pour récupérer le fichier texte.
 - Et il me renvoie le texte lisible par la librairie spacy.
   Voici la fonction qui fait cela:

.. code:: python

  def get_text_from_file(files):
      with open(files, 'r') as myfile:
          text = myfile.read()
      return text

J'utilise cette autre commande afin que la librairie nlp l'analyse c'est la tokenisation
**tokenisation**.
TEXTE

.. code:: python

  doc = nlp(text)

----

NLP - Travaux
=============

Premier projet: Détection des verbes
---------------------------------------

Etapes 2: récupérer les verbes du texte
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Après récupérer le texte je dois créer une fonction qui doit repérer les verbes.

Voilà comment elle fonctionne:

- étape 1: faire la **word segmentation du texte** (découper le texte en mots)...
- étape 2: regarder uniquement la **fonction** des mots (POS tagging) si la fonction est "verbe"...
- étape 3: ajouter le **lema** du mot (forme canonique, ici l'infinitif) a la liste

.. code:: python

  verbes = [token.lemma_ for token in doc if token.pos_ == "VERB"]

  def get_text(doc):
      verbes = []
      for token in doc:
          if token.pos_ == "VERB"
              verbes.append(token;lemma_)
          return verbes

Après ces deux étapes, il ne reste plus qu'a éxécuter le code.

