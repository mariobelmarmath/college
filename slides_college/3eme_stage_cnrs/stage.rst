:data-transition-duration: 700
:css: .my.css

.. footer::

  Mario

.. title:: Oral de Stage

----

Comment utiliser l'intelligence artificielle pour extraire les verbes ?
=======================================================================

.. figure:: img/front-cover.png

----

:id: p-pad

Introduction
============

NLP pour

  - **N** atural
  - **L** anguage
  - **P** rocessing

soit

  - **T** raitement du
  - **L** angage
  - **N** aturel


----

Sommaire
========

1. Fonctionnement
--------------------

2. Projet
---------

----

NLP - Fonctionnement
====================

Méthode d'apprentissage, théorie
--------------------------------

Il existe plusieurs familles d’apprentissages utilisées pour les
librairies de NLP qui ont évolué avec le temps.

- Méthodes basées sur des règles

- Modèles de Machine Learning

- Modèles de Deep Learning

----

NLP - Fonctionnement
====================
Librairies et capacités
-----------------------

J'utilise la librairie NLP appelés Spacy, qui est assez récente mais plus rapide.
Voici ce dont est capable Spacy:


- La **tokenisation** ou **word segmentation** ( bonjour | les | amis )
  
  .. - Ex: 'bonjour les amis' -> 'bonjour', 'les', 'amis'

.. class:: substep 

- **lemmatization** (trouvaient -> trouver)

  .. - Ex: 'trouvaient' -> 'trouver'
    
.. class:: substep 

- **P.O.S tagging** (nature du mot)

  .. - Ex: 'l'enfant mange une pomme' -> l'enfant : sujet | mange : verbe | etc..

.. class:: substep 

- **dependency parsing** ("que" peut avoir plusieurs signification)

  .. - Ex: le mot 'que' peut signifier plein de choses



----

:id: slide6

Premier projet: Détection des verbes
---------------------------------------

.. Etape 0: initialisation
.. ~~~~~~~~~~~~~~~~~~~~~~~

**Etape 0: initialisation**


.. code:: python

   import spacy
   nlp = spacy.load("en_core_web_sm")


.. class:: substep 

**Etape 1: Récupérer le fichier et le rendre compréhensible:**

.. Etape 1: Récupérer le fichier et le rendre compréhensible
.. ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. class:: substep 

Pour  le language de programmation.

.. class:: substep 

.. code:: python
   
  def get_text_from_file(files):
      with open(files, 'r') as myfile:
          text = myfile.read()
      return text

.. class:: substep 

Pour la librairie de NLP

.. class:: substep 

.. code:: python

  doc = nlp(text)

----

.. :id: li-pad

Premier projet
--------------

Etapes 2: Récupérer les verbes du texte
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#. Définir un **liste**
#. Prendre chacun des tokens...
#. Regarder uniquement la **nature** des mots, si la nature est
   "verbe"...
#. Ajouter forme canonique a l a liste.

----

Premier projet
--------------

Etapes 2: Récupérer les verbes du texte
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: python

  def get_text(doc):
      verbes = []
      for token in doc:
          if token.pos_ == "VERB"
              verbes.append(token.lemma_)
          return verbes

Il ne reste plus qu'a éxécuter les fonctions a la chaine.

.. code:: python

  def main(files):
    texte = get_text_from_file("Téléchargement/alice.txt")
    doc = nlp(texte)
    verbes = get_text(doc)
    print(verbes)


