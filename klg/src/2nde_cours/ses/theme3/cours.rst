Comment se forme un prix sur le marché  
========================================

Qu'est-ce qu'un marché ?
------------------------

A l'origine, c'est le lieu de rencontre **physique** entre l'offre et la demande
(brocante de Lille). Avec le dev des technologies, le marché n'est plus un lieu physique
mais un lieu accédable depuis tous les parties du monde par internet, il est
immatérialisation.
Historiquement, le capitalisme était tout d'abord commerciale (route de la soie, des
épices), cela commence avec les cités états d'Italie comme Venise avec son port. Mais le
marché n'organise pas la société.

A travers l'exemple de la braderie de Lille, le marché dans son expression la plus
simple est un lieu physique, de proximité où des personnes vont ammener une production,
des objets soit qu'ils ont produit eux-mêmes soit qu'ils ont acheté par le passé. Face à
eux des personnes vont être interressé par ces objets, ils vont demander ces objets donc
la braderie de lille est un lieu de rencontre physique entre une offre et un demande les
différents acteurs vont négocier les prix.
La bourse de Paris, aujourd'hui, n'est plus un lieu physique la bourse de Paris est un
lieu immatériel via des ordinateurs, des logiciels sur lesquels chacun peut se connecter
il y a bien rencontre entre une offre et une demande de capital, le marché des capitaux
est immatériel et globalisé. A travers ces deux exemples, on comprend l'évolution de
concept de marché, Fernand Braudel, historien français, a dréssé une histoire du
capitalisme:

- Le premier temps est le capitalisme commercial, c'est la route de la soie, des
  épices le commerce entre l'orient et l'occident par l'intermdédiaire de villes comme
  Florence au 15eme et 16eme siècle, puis on assistera des foires commerciales, suite
  aux découvertes maritimes, le commerce triangulaire se developpe ce sera un moment
  clé de l'histoire de nos sociétés. A travers ces quelques éléments, on comprend que
  peu à peu, le marché va devenir une réalité sociale et politique au sens où la
  société va être le marhcé va structurer l'organisation de la société, on parle
  d'économie de marché voir de société de marché. Les economistes sur le plan
  théorique, définissent le marché comme le lieu physique ou abstrait de la rencontre
  de l'offre et la demande qui aboutit à la formation d'un prix.


**Comment a évolué le concept de marché dans notre société ?**

Tout d'abord le marché etait le lieu de rencontre entre la demande et l'offre les plus
grands marchés se trouvaient en Italie ou sur la route de la soie. A ce moment la
commerce et les marché ne jouent pas un role décisif dans la société.
Certains économiste parlent société de marché.

**Comment les économistes, sur le plan théorique, définissent-ils un marché ?**

**Pourquoi aujourd'hui, parle-t-on d'économie de marché ?**

Toute l'économie est structuré autour du marché, avant 89 (chute berlin) on avait les
économies socialistes, tout était planifié et il n'y avait pas de marché.

La loi de l'offre et de la demande
----------------------------------

Le marché est un lieu ou s'échange un bien entre vendeur et acheteur, le prix résulte de
la confrontation entre l'offre et la demande(voir video)

schéma 1: aug + de prix ==> aug + de quantité

D'après le schéma 1 on constate que l'offre est une fonction croissante du prix, pour
expliquer, suppposons que le prix d'une baguette tradition est de 1€30, supposons que le
cout de production du boulanger est de 0.5€ par baguette cela signifie que sur chaque
baguette le boulanger gagne 0.8€ il est donc interessant de produire car il va gagner de
l'argent. Sur notre schéma il produit une quantité q1, supposons que le prix de la
baguette tradition passe à 3€ (raisons politiques et économiques), alors que le cout de
production reste identique donc sur chaque baguette vendue le boulanger gagne 2.5€, par
conséquent il est très intéressant pour lui de produire plus il va donc en profiter et
produire une quantité Q2, Q1 < Q2. Dans la vidéo il y a une nouvelle et forte demande
d'eau de coco donc le prix de la tonne de noix de coco(matière première) évolue a hausse
fortement il devient très intéressant de produire plus de noix de coco donc cela incite
des nouveux producteurs de noix de coco a investir ce marché, ainsi on constate que des
producteurs vont planter 300 ha de noix de coco hybride. Ainsi on peut donc dire que
l'offre est une fonction croissante du prix.

**Pourquoi pouvons-nous dire que l'offre est une fonction croissnante du prix ?**

Plusieurs exemples nous montre que lorsque le prix augmente alors l'offre produite suit
la progression, c'est la définition mathématique d'une fonction croissante.
C'est normal car il est profitable de produire plus lorsque objet a plus de valeur pour
faire plus de profit.

Selon la théorie écnomique, la demande est une fonction décroissante du prix. D'après le
schéma 2, quand le prix d'un bien est élevé alors la quantité demandé est faible. Si le
prix du bien demandé décroit alors la quantité demandé croit. Ce raisonnment semble
cohérent et logique. Si  la quantité d'un bien régresse, quantité demandée augmente.

La confrontation entre la demande et l'offre forme le prix d'équilibre et la quantité
d'équilibre (pt d'intersection de la droite de la demande et offre)

D'après  la constrction graphique, si la demande et l'offre régresse alors le prix
d'équilibre augmente et la quantité baisse. On retrouvre les fondamentaux de la loi des
deux droites. Le prix et la demande d'équilibre sont le résultat de la confrontation
entre la force de l'offre et la demande.

Un auteur libéral considère que les prix sont des signaux qui tansmettent l'infoà chaque
instant aux offreurs (les producteurs) et aux demandeurs (les consommatuers). Ainsi les
agents économiques, en fonction des prix vont modifier leur comportement économique et
l'équilibre va se modifier on parlera d'une réallocation optimale des moeyens de
production. Selon Hayek c'est une force du capitalisme de pouvoir à chaque fois trouver
un nouvelle équilibre.

Quels sont les effets d'une taxe et d'une subventions sur l'équilibre du marché
-------------------------------------------------------------------------------

Le lendemain de la seconde guerre mondiale avec les accords du GATT de 1948, on assiste
à un moment de libéralisation de l'économie et du commerce mondial qui se traduit par
une ouverture international importante l'idée est de favoriser la circulation des
marchandises des capitaux à travers le monde notamment en abaissant fortement les
droits de douanes. A partir des années 80 ce mouvement d'ouverture international prend
de l'ampleur on parle de mondialisation, de globalisation. Les libéraux sont favorables
à cette politique car ils pensent que l'ouverture international, l'étendu des marchés, 
sont source de croissance économique. A partir des années 90 on assiste au developpement
du concept d'entreprise sans usine, ainsi les entreprises occidentales tel Apple
effectue des recherches et developpement (R&D) de leur produit dans les pays développés
où il y a un fort niveau d'éducation par contre il font fabriquer leurs productions en
Asie (chine, inde), pays dans lequels le cout du travail est très faible puis dans un
troisième ils vendent leurs productions à fort pouvoir d'achat.

Ce mouvement d'ouverture internationale s'est traduit par une croissance du PIB mondiale
importante, mais on a assisté, durant cette époque, à un mouvement de
désindustrialisation dans les pays développés, ainsi la france aurait perdu 1 million
d'emploi industriel. Face à une telle sitution, certains politiques proposent un retour
au protectionisme, l'idée est de protégé l'industrie nationale en érigeant des droits
de douane importants ainsi dans notre document, Donald Trump propose des droits de
douanes de 25% sur les importations venant de chine.

Dans notre exemple fictif, une taxe est instauré sur la production de ciment car cette
production pollue, on constate qu'après l'instauration de la taxe le point d'équilibre
passe de e1 en e2, on constate que la quantité d'équilibre passe de q1 en q2, elle
régresse alors que le prix d'équilibre passe de p1 en p2 et progresse. Donc l'objectif
de la taxe est atteint, on produit moins de ciment donc on pollue moins.

D'après le document 4 on constate qu'une subevention recu par les producteurs moblise
l'équilibre du marché la droite se déplace de o1 en o2 avec une augmentation des prix
d'équilibre et une réduction 

Documentaire: la guerre des cotons
----------------------------------

exploitations de cotons très rentables
jusqu'a 200 livres/jour
on pouvait en vivre, de petites exp
mnt impossible, grande entreprises avec machines
ils sont partis ou devinir des industriels
pls milliers hectares mais concurrence pays main d'oeuvre - cher et pas respect terre
USA subvention 3 milliard leur production de cotons
afrique de l'ouest 2eme prod
main d'oeuvre (sans machine) mal payé et pas de subvention
USA ne respecte pas les lois
afrique bat la plus grande puissance du monde(coton de qualité)
fondent coopératives et syndicats pour lutter contre USA
2004 pres burkina faso, malie pointent du doigt l'USA
coton = 12 M d'africain
l'Afrique demande justice
cout prod 100 fois inférieur qu'une exploitation USA
coton finance tout à l'Afrique -> sans coton misère totale
Origine: france cherche continuer d'avoir main mise sur l'Afrique
agronome on pris le coton
richesse rurale aug depuis le coton
asso africaine pas assez de force donc demande une aide financière
en USA coton = seul metier qui donne du travail au gens à la campagne
coton des USA plus aussi puissant qu'avant
600000 tonnes meilleure prod du mali
qualité haute: ramasser à la main
mais pas assez de machine, qualité baisse
agronome africains proposent meilleur qualité aux acheteur
début année prix s'envole mais baisse après
COPACO: recueuillent le plus d'info possible
spéculation coton -> fortune
USA fixe les critères de tous les prod mondiale
USA = référence
Logistique = talon d'achille du coton africains
besoin d'investissemt colossaux
aucun cargo -> entreprises privé

Les marchés des matières premières sont mondiaux
origine: USA , ramène des Millions d'africains 
coton USA pas compétitif, plus cher à produire(main d'oeuvren)
donc subvention de l'état
africains pas de capitaux = 2eme acteur
cout prod très faible mais probl d'infrastructures + instabl pol
USA définissent norme
3acteur: entreprises francaise qui font le commerce du coton
qui ont dév le coton après la décolonisation pour garder contact
chine grand conso de coton
