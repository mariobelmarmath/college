Comment creé-t-on des richesses et comment les mesure-t-on ?
============================================================

Production marchande et produtction non marchande
-------------------------------------------------

**Document 1 p.34**

Produire = créer des biens et des services qui n'éxistent pas dans la nature

**y = f(k, l)**:

- y: production
- k: capital
- l: travail

Cela signifie que pour produire nous avons besoin du facteur travail et capital.
A l'origine le facteur capital c'est de l'argent, capital financier, qui va se transformer en capital
techniques avec l'achat de machines.
On distingue les biens des services:

- Un bien est un produit matériel. ex: chaise
- Un service est produit immatériel ex: cours de math


Dans un premier temps distinguons la production non marchande, cet un service non vendue
sur un marché et offert quasi-gratuitement à un prix inferieur au cout de production.
L'objectif n'est pas de réaliser un profit ainsi le cout d'une scolarité publique est
d'environ 10000 €/an mais un étudiant paye en moyenne 140 €/an la scolarité est
financé par l'impot.

Interessons nous maintenant la notions de production marchande: ce sont des biens et
des services vendues sur un marchand à  un prix permettant de réaliser un bénéfice.

Ainsi le prix total pour 3 annés d'école de commerce après les classes préparatoires est
en moyenne de 43k €, pour HCE le prix est 61k €.

Prenons un exemple pour illustrer la production marchande, ainsi dans la fast-fashion
l'entreprise Zara produit et vend un chemisier à 25 €:

- Matières premières: 3,22 €
- Assemblage, main d'oeuvre, marge fournisseurs: 1,99 €

Cout de production: 5,21 €, donc la **majoration de la marque** est de 15 € (4 € de TVA)
elle
couvre: 

- frais de boutique
- salaire des vendeurs
- marketing
- **profit**

Question: L'enseignement est-il un service marchand ou non marchand ?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Dans cette question, on peut distinguer l'enseignement public et privé.

L'enseignement public n'a pas de but lucratif, son but est de former les futurs citoyens
et son capital provient de l'Etat qui dirige l'éduction au travers du minstère de
l'éducation. On observe que les frais de scolarité ne sont pas représentatifs du cout de
l'éducation. On peut affirmer que l'enseignement public est un service non marchand.

Doc 2 La prod marchande et non marchande en France
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

85% de la production est **marchande**
15% de la production est **non marchande**

La production marchande en france d'après l'INSEE en 2018 représente 85% de la
production totale exprimé en milliard d'euros.
La production non marchande représente environ 15% de la production totale.
Notons que par convention la production non marchande est évaluée a son cout de
production. Ansi la valeur réalisé par l'université de la Sorbonne est évalué a son cout
de production. Alors que l'année N+3 et N+5 les étudiants sur le marché du travail vont
créer une richesse importante. Ansi ce mode d'évaluation de la production non marchande
sous-estime son poids dans la production totale.

Doc 3 L'évolution de la structure de la production en France
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

En 1950 la productionde biens représentait 63% de la production totale, les services
marchands a cette date représentait 29% et les services non marchands 8%. D'après
l'INSEE, en 2016,  la production de biens représente 32% de la production totale, les
services marchands 53% et les services non marhands 15%, ainsi les services en 2016
représente 68%. L'économiste Colin Clark propose une classification de l'économie on
distingue 3 secteurs:

- Secteur primaire: agriculture, la pêche
- Secteur secondaire: l'industrie
- Secteur tertiare: les services

On obtient le schéma suivant:

- le secteur primaire représentait 60% de la pop active en 1860, aujourd'hui 4% notons
  qu'aujourd'hui nous produisons bcp plus grace aux progrès technique.
- Le secteur secondaire en france se developpe jusqu'en 1970 environ puis à partir de
  cette date il connais un relatif mouvement de désindustrialisation.
- Le secteur tertiare se developpe fortement, aujourd'hui il représente près de 70% de
  la population acitve, on parle de tertiarisation de l'économie, certains éconmistes
  avancent le concept de capitalisme cognitif


**Selon vous, est-il résonnable des services non marchands à leur cout de prodction ?**

Si l'on étudie en profondeur le méchanisme de certains services non marchands on
s'appercoient qu'ils ont des conséquences sur la production totale, ansi on peut se
demander s'il ne serait pas important d'ajouter un coefficient "conséquences sur la
production totale" au calcul du pourcentage des cout des services non marchands.

**Expliquez le concept de tertiairisation**

Le tertiare occupe de  plus en plus de place dans l'économie. Ainsi alors que les
services représentaient 37% de la population active en en 1950, c'est 68% de la population
active qui travaille aujourd'hui dans le secteur tertiaire. Cette tertiarisation de
l'économie se développe surtout depuis les années 1970, et notamment dans certains
secteurs tel que le tourisme.

Diversité des producteurs
-------------------------

**doc 1 p. 36: 3 producteurs aux objectifs differents**

On distingue 3 types de producteurs aux objectifs différents:

- les associations: regroupent près de 80k bénévoles (travaillent gratuitement), ces
  associations sont financés par les dons, les legs (héritages), et les subvention de la
  puissance publique. L'objectif des associations est d'offrir un service non marchand,
  leur logique n'est pas de réaliser un profit.
- les entrepises: L'objectif de cet entreprise est de réaliser un profit.
  La recherce du profit est le moteur du systeme capitaliste, on parle d'entreprise
  privé car le capital, pour créer l'entreprise, est amené par des personnes privées: le
  propriétaire de l'entreprise, des actionnaires, les banques
- l'administration publique: l'éducation nationale, cette administration qui dépend de
  l'Etat emploi 800k enseignants, son objecitif est de fournir un service à la
  population à titre quasi-gratuit. L'éducation nationale n'a pas pour objectif de
  réaliser un profit

On comprend la différence entre service marchand et non marchand, une association
organisé par des bénévoles propose un service d'aide aux personnes agés gratuitement,
par contre une entreprise privée le meme service d'aide mais le fait payé (14€/h)

**Parmi les trois producteurs, lesquels ne recherchent pas de l'argent ?**

Parmi les 3 producteurs tel que: association, entreprises et l'administration publique,
seuls les associations et l'administration publique ne cherchent pas de profit.

**Doc 2: Diversité des producteurs**

Capitalisme: majorité de la production totale est assurée par des entreprises privées.
Interressons-nous à la diversité des producteurs:

- Les entreprises: organisations produisant des biens et des services marchands pour
  réaliser des profits, l'objectif des entreprises est de réaliser des profits en
  vendant des biens et des services sur le marché. Les entreprises représentent 75% de
  la production totale en 2017. Ex: Dacia
- Les administrations publiques: organisations gérées par la puissance publique dont
  la fonction principale est la production de services non marchands, elles recherchent
  l'interet général. Elles représentent 12% de la production totale en 2017. Ex: éducation nationale.
- Les associations: ce sont des groupes de personnes autour d'un projet commun sans
  chercher a réaliser un profit, des bénéfices. Elles représentent 1.4% de la
  production totale. Ex: resto du coeur

**Video: la marque Dacia**

Prenons la marque Renault-Dacia pour illustrer notre exemple. Renault-Dacia, au Maroc
les ouvriers travaillent 6/7 jours, l'Etat marocains a developper une structure
portuaire. Cette entreprise cherche à minimiser ses couts, il n'y a pas de frais de
recherchent et la conception de la voiture cherche la simplicité avec des matériaux de
base. L'objectif est de vendre une voiture neuve à  faible prix à travers une
minimisation des couts. La marque Renault-Dacia est un succés industriel et commercial.

Ressources nécessaires à la production
--------------------------------------

**Doc 1 p38**

A l'aide du document 1 et de la vidéo projeté, on comprend que la manière de pratiquer
l'agriculture à fortement évoluer du 19eme au 21 siècle. Au 19eme on utilise pas de
machine et beaucoup de travailleurs, aujourd'hui on utilise beaucoup d'outils, de
technologies, de machines et peu de travailleurs. Ainsi on peut définir une fonction de
production y = f(k, l) \| y: production \| k: capital(machines) \| l: travail cela
signifie que pour produire il faut une certaine quantité de capital et de travail. On
définit ainsi une combinaison productive, on comprend qu'entre le 19eme et aujourd'hui
la combinaison productive dans l'agriculture à fortement évolué. Notons qu'aujourd'hui
nous produisons des quantités agricoles beaucoup plus importante qu'au 19eme.

**Comment expliquer le succés de Dacia ?**

L'entreprise Dacia a réussi à vendre des voitures pas cher grace à certaines méthodes.
Premiérement elle s'est délocalisé pour s'installer au Maroc, ou les salaires sont
beaucoup plus bas, 300€/mois c'est à dire 4 fois moins que le SMIC, l'entreprise est
exempté de taxe pendant 5 ans, elle bénéficie de la proximité avec le 2eme plus grand
port de l'afrique. La conception en elle-même est facilité car ils réutilisent des
composant et des moteurs qui ont déjà servis, ainsi les dépense de la recherche sont
drastiquement réduit.

**Comment à évoluer la combinaison productive dans l'agriculture entre le 19eme et
aujourd'hui ?**


**Doc 4: L'impact des changements technologiques sur l'emploi**

productivité de k = Y/K = 100 produits/ 10 machines = 10
productivité de L = Y/L = 100 produits/ 10 salaires = 10

Richesse suppl. ->: 

- plus de profits pour l'entreprise
- hausse des salaire
- baisse des prix pour le consomateur

Dès le début de la révolution industrielle (1776), les ouvriers des usines de Lyon,
dans le textile (canuts de lyon) se sont inquiétés de l'inovation de J.Kay, la navette
volante, les canuts s'inquiètent de cette machine et des conséquences sur leur emploi.
Selon les économistes, à court terme et dans une optique micro-économique, se traduit
par une substitution capital/travail. Mais selon les économistes, à long terme, le
progrès technique est source d'emploi, il convient d'expliquer ce méchanisme. Dans un
premier temps, définissons le concept de productivité, ce concept traduit l'éfficacité
des facteurs de productions. On distingue un productivité du travail (Y/L) et du capital
(Y/K) illustrons notre propos :

- à l'époque 1 je fabrique 100 produits avec 10 machines donc j'ai une productivité de
  10.
- à l'époque 2 j'introduis du progrès techniques et 1 mahcine produit 150 produits j'ai
  donc une productivité de 150. L'entreprise beaucoup plus et pour moins cher,
  intervient le concept de repartitiondes gains de productivité: cf Richess suppl.

Selon un auteur autrichien, Joseph Schumpeter (1883-1950): "le progrès technique est un
ouragan perpetuel de destruction créatrice". Ainsi selon lui le progrès technique
perime des secteurs entiers de l'économie mais il va en créer d'autres.

**Pourquoi peut-on dire qu'à cours terme le progrès technique détruit de l'emloi**

**Pourquoi peut-on dire qu'à long terme le progrès technique créer de l'emloi**
**Quel regard portez-vous sur l'intelligence artificielle**


Les principaux indicateurs de la richesse
-----------------------------------------

La notion de valeur ajouté est fondamentale en économie prenons un exemple pour
illustrer notre propos, l'entreprise Michelin produit des pneux c'est ce qui sort de
l'usine, on les nomme output, la quantité de pneux produite multiplié par le prix d'un
pneu est égale au chiffre d'affaire, on note p*q = CA, cela correspond a la valeur
de la production estimé par le prix du marché. Mais pour produire des pneux Michelin a
eu besoin de caoutchouc, ce sont les ressources qui rentrent dans l'usine et qui sont
nécéssaire à la production, on les nomme les consomations intermediaire CI.
Si je calcul la différence entre la valeur qui sort de l'usine(CA) moins la valeur qui
entre dans l'usine (CI) j'obtiens la valeur ajouté de l'entreprise, Michelin transforme
du caoutchouc en pneux: CA - CI = VA
Attention la VA ne correspontd pas aux profits de l'entreprise, en effet sur cette VA
l'entreprise doit rémunérer ses salariés et payé les impots, on obtient:

- VA - salaire(w) - impot(t) = EBE(éxédent brut d'exploitation)

Si on raisonne à partir d'une fonction de production, y = f(k,l), on comprend que la VA
se répartit de la manière suivante: une partie pour les salaire (60%), une partie pour l'état (5%)
et une partie pour l'entreprise (30%).

On distingue Trois époques dans le partage de la valeur ajouté:

- Capitalisme sauvage: l'essentiel va vers K(capital) -> *Germinal*, révolution
  industrielle. Les entrepreneurs ont de l'argent pour créer des usines mais les
  travailleurs sont dans la misère mais le système va buter sur une **crise de
  surproduction** en 1929, les entreprises ont de quoi investir mais pas de
  consommateurs en face (salaire bas)
- Fordisme, trente glorieuse: Ford propose de doubler les salaires, ce système va
  permettre une articulation production de masse et consomation de masse mais il vient
  buter sur une **crise de réalisation** on investit pas assez dans le capital et cela
  freine le developement.
- La VA se redirige vers K mais pour éviter une crise de surproduction, cet argent va
  etre preter au facteur travail sous forme de crédit pour pouvoir consommer et acheter
  des logements.

**Comment peut-on définir la VA ?**

La VA 

**La VA correspond-t-elle au profit de l'entreprise ?**
**En quoi la répartition de la valeur ajouté a une influence sur la dynamique du
capitalisme ?**


La répartition de la VA a une influence sur le dynamisme, 
rapport villermé: medecin fait un rapport sur la santé des enfants, suite à ce rapport
on réglemente le travail des enfants.

Le PIB, définition et limite
----------------------------

Le PIB, est un indicateur qui permet de mesuré l'ensemble des richesses créer par une
économie, le PIB correspond a la somme des VA créé par les agents économiques: PIB =
somme des VA.
Le critère retenue est le critère de territorialité. En 2022 le PIB de la france est de
2290 Mds€ ce qui fait un PIB par habitant de 33000/hab.
D'après le critère retenu on constate que les services marchands représentent 56% du
PIB, les non-marchands 23%, ainsi le tertiaire représente 79% du PIB en France. On parle
de tertiarisation de l'économie, nous serions dans une nouvelle époque du capitalisme
cognitif, lié à la gestion des données, l'intelligence des situations. L'industrie en
France ne représente aujourd'hui que 14% du PIB. aujourd'hui l'agriculture ne représente
que 2% du PIB mais on produit 150 fois plus de production agricole qu'en 1850.

coef. multiplicateur = Etat final / Etat initial
cf. m. = 2798/300 = 9,3 

D'après le document projeté le PIB en Mds € constant en france, la hausse du PIB
constant entre 1950 et 2023 a été multiplié par 9,3, certe durant cette époque la pop.
est passée de 40 millions à 68 mais nous avons assisté à une hausse du PIB/hab
considérable. Durant les Trentes Glorieuses en France et le monde occidental se
developpe une équation productiviste, le PIB devient l'indicateur pertinant pour
comprendre l'évolution de la société, en effet chaque fois que le PIB progresse durant
cette époque le niveau de vie augmente et la population accède à la consomation et au
confort. Et ces éléments se traduisent par une hausse du bien-être. A parir de 1973 ce
mode de developpement  économique et social entre progressivement en cris et l'équation
ne fonctionne plus, aujourd'hui le système économique vient buter sur la limite
écologique et d'autre part, le niveau de bien-être ne progresse plus. On asssite 
Durant les 30 glorieuses le taux croissance annuel du PIB était de 5,3%, aujourd'hui il
est toujours positif mais en 2023 il est de 0.9%.

**Pourquoi après la seconde guerre mondiale le PIB s'impose comme un indicateur
pertinant pour comprendre l'évolution de la société ?**

Après la seconde guerre mondial nous entrons dans une période de production de masse,
c'est pourquoi il devient important de mesurer à quel point l'on produit et c'est
pourquoi on met en place le PIB qui un système de comptage qui se base sur la somme des
valeurs ajoutés.

**Commentez l'évolution du taux de croissance annuel du PIB entre 1950 et aujourd'hui.**

Après la 2GM l'occident 

Comprendre le role du facteur travail dans la croissance
--------------------------------------------------------

**Vidéo 1ere partie de "mémoire d'immigrés les pires"**

- Raison/condition de la venue des immigrés
- les enfants ne croient pas les parents (ils ont pas vécus les memes choses)
- pls pays du maghrébe, apprenent lire/écrire -> s'intégrer,comprendre, moyens de
  défenses
- année 40, recrutement des immigrés pour la déficit démographique
- OS: ouvrier spécialisé: un seul mouv.
- principalement d'algérie (colonie fr)
- pas de non-francais, ils étaient citoyens fr(algé. = protectorat fr)
- OS que célibataire (moins d'attache)
- recrutement par géographie/famille
- ex: berger (peu d'argent) ->  vient en fr pour travail (mieux payé) + apprend la
  langue en mm tps
- test médical/reflexe et on les envoie a la chaine
- cadence et indifférence de la capacité intelectuelle => peu d'évolution
- ne revient plus chez lui.
- pensais que tt serai bien, + de solitude => choc dès l'arrivée
- aime cult/langue fr => devenir fr
- selectioneur: passer entretien pour les envoyer vers les société
- au début réticent a prendre main-d'oeuvre marocaine mais obligé
- quota par régiions
- ruraux > urbains
- très peu de mauvais ouvriers(marocains): 2%
- test dès le début ex: regarde les mains travailleurs ou pas
- recoive tampon vert/rouge (au début)
- test physique -> conditions de travail dur (poids, fumée)
- logés dans braquement: 6 pers/baraque
- organisé pour rester enfermer: car, test, avion, france. but: ne se developpe pas
- reste dans la misère
- 56: congrès du travail -> faut-il arreter cet immigration ? reponse non (besoin
  main-d'oeuvre)
- pdt guerre d'algé.: rapports selon lequel ouvriers y a des alliés des FLN, mais
  entreprise ne font rien car c'est ce n'est pas police
- indép. Algérie: début de la discrimination
- apr. ind. Alg.: négo. pour accord sur la main d'oeuvre. FR besoin de md -- AL envoie
  de célibataire
- louent des baraques ~~ misérable  aux propri. -> meilleures maison + cher

Les limites écologiques de la croissance
----------------------------------------

La première prise de conscience des limites écologiques de notre modèle de croissance a
été formulé par le club de Rome en 1972, ses experts publient un rapport qui se nomme
halte a la croissance à travers des études scientifiques ils comprennent que notre
modèle économique fondé sur la croissance n'est pas soutenable. En 1973, suite à la
guerre du Kippour le prix du pétrole est multiplié par 4. En 1979, suite à  la crise
iranienne le prix du pétrole est de nouveau multiplié par 2. Aussi la crise économique,
le chomage se développent dans le monde occidental, pour résoudre le chomage il faut de
la croissance économique et donc la question écologique passe au second plan, néanmoins
le débat progresse en 1986 le rapport Brundtland propose le concept de developpement
durable: C'est notre capacité a faire de la croissance aujourd'hui sans remettre en
cause la capacité des générations futures à connaitre la croissance, ce concept s'est
imposé comme un compromis politique.
En 1998 création du GIEC, Groupe International des Experts sur le Climat. Des
scientifiques du monde entier vont produire des données et montrer l'importance du
réchauffement climatique suite à la croissance économique 

**Etes-vous sensible au débat sur la question écologique ?**

Le changement climatique n'est pas une opininon ni un courant idéologique c's 'est une
réalité. Lorsque l'on voit les chiffres scientifiques et études il ne s'agit pas d'etre
d'accord ou contre.

**Pensez-vous que nous devons réformer notre système économique pour répondre au enjeux
climatique ?**



j 
