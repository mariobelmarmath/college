Comment les économistes réfléchissent-ils ?
==============================================

Une approche en terme de circuit (vide)
---------------------------------------

Une approche en terme d'équilibre (vide) 
----------------------------------------

Une approche en terme historique (vide)
---------------------------------------

Exercices polycopiés
--------------------

**1) Commentez la phrase du texte suivant:**
   -  **par bien des aspects c'est l'ensembles des rapports dans la société qui: se trouve
      influencés par les données économiques ?**

Dans notre société actuelle, les données économiques ont une incidence forte et décisive
sur nos choix quotidiens et sociétaux.

On contaste que la dimension économique envahi l'espace sociale et influence les
rapports sociaux. Tout d'abord la logique de profit de sa maximisation, cela renvoie à
une logique utilitariste c'est l'idée: maximiser de l'utilité sous contrainte, de
revenu, ou temporel. Un auteur, Becker a fait une analyse économique du fonctionnement
du couple, meme l'affection est influencé par l'économie.
Au total, l'économie influence l'ensemble des rapports sociaux à des degrés divers.
La logique capitaliste envahi la société.

**2) Pourquoi la compréhension de l'éconmie est un enjeux politique ?**

La politique consiste en partie à faire des choix, et ces choix s'appuient notamment sur
l'économie et ses données. En ayant une bonne compréhension de l'éconmie, un dirigeant
sera plus apte à prendre les bonnes solutions.

C'est prendre des décisions ,les décisions économiques ont des conséquences
majeures,bien sur en terme de répartition de revenu sur les citoyens, en terme de
fiscalité, et il est important que les citoyens aient une formation écon. si on veut que
la démocratie fonctionne, sinon il y aura une impulsivité au niveau du vote.
Il y a un enjeux démo. fort à la compre. des phenomèmes économiques. Il faut aborder la
complexité du débat, en science sociales la notion de vérité est complexe. Il y a divers
approche d'éclairé le débat et il est important que chq citoyen est une culture en
science sociale.

**3) Pourquoi faut il comprendre l'économie dans ses rapports a la société ?**

Puisque l'économie est intrinsèquement liée à la société, il est important de comprendre
ces liaisons qui construisent la société.

L'économie se noue de relation sociale, est encastréedans l'espace sociale, la quesion
de l'organistaion du travail, de l'aménagement de la ville.

**4) Pourquoi est-il important de comprendre les méchanisme économiques ?**

Comprendre les mécanisme économiques permet de résoudre des problèmes sociétaux.
Pour cela il faut apprendre la méthode de l'économiste, celle de la rigueur.

**5) Doit-on avoir une approche quantitative de l'économie ?**

Quanitifiez l'économie revient à traiter seulement des nombres, il faut donc etre
prudent et ne pas mal les interprétes et les analyser avec précision.

**6) Comment maitrisez lez problèmes économiques ?**

Pour maitriser il faut avant tout comprendre, et cela ce fait par une éducation aux
méthodes et méthodologies propre à cette discipline mais aussi par la lecture d'ouvrage
d'économie.


Correction contrôle

Harvey découvre sang irigue corp, Puis medecin applique ce phénomène à l'économie et
définit un tableau avec 3 agents où le sang irigue ce systeme.

Analyse dev par adam smith, la force de la demande et de l'offre se rencontre dans un
marché pour formé un prix d'équilibre

Marx pense avoir trouvé le principe explicatif du déroulement historique: a chaque
époque ce conflit influence le cours de l'histoire. Les prolétaires n'ont que la force
de leur bras comme force. Les bourgeois possèsent les moyens de production

Il est important de comprendre l'économie pour les enjeux politiques, il faut que les
electeurs aient une connaissance de l'économie.

Quelques éléments sur le travail d'esther duflot prix nobel d'économie (pauvreté)
------------------------------------------------------------------------------------

Esther Duflot économiste francaise travaillant au USA à obtenu le prix nobel d'économie.
Elle dev une approche pragmatique de l'économie en effet elle teste des politiques
publiquesà petite échelle en s'occupant de tous les détails de l'application de ces
politiques et en fonctions des résultats on peut généralisé ces politiques publiques:

- Une de ses équipes a testé l'impact d'une heure d'orientation sur un groupe de
 terminale dans un lycée défavorisé. Son équipe a constaté que des résultats
 scolaires des élèves s'amèliorait et que leur orientation aussi.
- De même en france on teste la politique des territoires 0 chomeur
- Conditionalité du RSA (15h de formation pour toucher le RSA)


Quelques éléments sur la démarche en sciences sociales
==========================================================

Théories et faits
--------------------

Question: L'observation permet-elle d'éviter toutes les erreurs ?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

a) Les deux amis ont une approche rationelle et logique de la question: ils s'appuient
tous les deux sur leurs observations de la nature. 

b) Ils ont tort car les observations, et les théories qu'ils ont utilisés ne s'applique
à ce problème. 

c)

Pendant des siècles les hommes se sont fiés à l'évidence de leurs sens pour comprendre
le réel, ainsi depuis Aristote qui se fondait sur les trabaux de Ptolémée il pensait que
que le système etait géocentrique les religieux de l'époque adhéraient a cette thèse car
pour eux la terre est au centre de l'univers car elle est création de dieu. Or au 17eme
siècle, Galilée démontrera que le système est héliocentrique, il construit un cadre
théorique pour montrer que la terre tourne autour du soleil. Ainsi commence la démarche
scientifique:
- les chercheurs dans un premier temps, vont construire un cadre de pensée, à l'aide de
  cette théorie ils vont lire la réalité. De la théorie ils vont déduire une lecture du
  réel, on parlera de déductivisme
- Quelques fois, pour comprendre le réel, l'oberver, les chercheurs vont élaborer des
  statistiques, et à partir des celles-ci ils vont developper une analyse, une
  construction intelectuelle de la réalité. Ainsi en partant des faits ils vont induire
  une analyse théoriques on parlera d'inductivisme.

L'économie se propose d'adopter une démarche scientifique.

Corrélation et causalité
---------------------------

Lorsqu'on observe une corrélation il n'y a pas toujours de causalité direct. Dans ce cas
on trouve un **facteur confondant** qui est la cause des deux statistiques:

Dans un pays on constate qu'il y a une grande concentration de chocolat et dans le meme
temps on constate que dans ce pays le nombre de prix nobel est élevé.
On effectue des comparaisons internationales et on constate que plu la concentration de
chcolat est élevé (facteur A), plus il y a de prix nobel (facteur B). Dans un premier
temps on peut conclure hativement que facteur A est cause de facteur B et donc dans ce
pays il y a des prix nobel. En fait il y a bien un lien, entre A et B, de corrélation et
non de causalité A n'est pas la cause de B en fait un troisième facteur C intervient le
facteur confondant dans notre exemple il s'agit de la richesse d'un pays en question, en
effet plus un pays est riche plus il peut consacrer de l'argent a l'éduction et a la
recherche donc la recherche progresse et le nombre de prix nobel augmentent d'autre part
les habitants des pays riches peuvent consommer des produits élaborés (chocolat). Il est
donc important de distinguer causalité et corrélation, quand A' et B' sont corrélé cela
signifie pas forcément que A' et cause de B' il peut exister un troisième facteur C qui
est la cause de A' et B'
