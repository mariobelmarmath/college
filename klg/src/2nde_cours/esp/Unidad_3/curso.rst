Vivir Juntos
============

convivencia: coexistence
promover -> promueve
pisos: appartements/étages
edificio: immeuble

En que Espana es una buena ilustracion de la convivencia

- Saber vivir juntos
- Los puntos positivos de una buena convivencia
- Los limites de una convivencia

Se trata de un cartel publicitario para promover la exposicion colectiva en la que
participan los vecinos del barrio madrileno Malasana . Los habitantes estan en los
balcones de sus pisos, en el restaurante y en la plaza estan contentos y orgullosos
porque comparten momentos agradables. Es una iniciativa realizada por los habitantes del
barrio que es una ilustacion del saber vivir juntos buena convivencia.

