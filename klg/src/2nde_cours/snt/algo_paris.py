chambre = [[-1 for i in range(10)] for l in range(10)]
# print(chambre, len(chambre), len(chambre[0]))
# chambre[1][0] = 1

def allumée(tableau):
    for idx, ligne in enumerate(tableau):
        for idy, ch in enumerate(ligne):
            if ch == 1:
                nb_ch = len(tableau[0])*idx + idy + 1
                print(idx, idy, nb_ch)

def passage(tableau, n):
    for idx, ligne in enumerate(tableau):
        for idy, ch in enumerate(ligne):
            nb_ch = len(tableau[0])*idx + idy + 1
            if nb_ch % n == 0:
                tableau[idx][idy] = tableau[idx][idy] * (-1)
                # print(idx, idy)
    return tableau


def roger(tableau):
    for i in range(1, 101):
        tableau = passage(tableau, i)
    return allumée(tableau)


# allumée(chambre)
# print(passage(chambre, 5))
# allumée(passage(chambre, 5))
print(roger(chambre))

