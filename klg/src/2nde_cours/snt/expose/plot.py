import matplotlib.pyplot as plt
from random import randint
from numpy import array
import numpy

x = numpy.arange(20)
y = x*2 + 2
y_f = x*4 + 2
x2 = [i + randint(-10,10)*0.1 for i in x]
y2 = [i + randint(-10,10)*0.1 for i in y]

plt.axis([0, 20, 0, 40])
plt.yticks([])
plt.xticks([])
plt.xlabel("Surface d'une maison")
plt.ylabel("Prix de la maison")

# plt.scatter(x2, y2)
# plt.title("Données")
# plt.savefig("table1.png")

# plt.scatter(x2, y2)
# plt.plot(x, y_f, 'r', label='a = 4; b = 2 ̀\n f(x) = 4x + 2')
# plt.legend(fontsize="14", loc='lower right', shadow=True)
# plt.title("Essai n°1")
# plt.savefig("table2.png")

# plt.scatter(x2, y2)
# plt.plot(x, y, 'g', label='a = 2; b = 2 \n f(x) = 2x + 2')
# plt.legend(fontsize="14", loc='lower right', shadow=True)
# plt.title("Essai n°2")
# plt.savefig("table3.png")

plt.scatter(x2, y2)
plt.plot(x, y, 'g', label='a = 2; b = 2 \n f(x) = 2x + 2')
plt.scatter(9, 20)
plt.legend(fontsize="14", loc='lower right', shadow=True)
plt.title("Extrapolation")
plt.savefig("table4.png")

plt.grid()
# plt.show()
