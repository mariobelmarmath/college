=============
Deep Learning
=============


Machine learning
================

Nous allons voir comment un modèle de machine learning est entrainé ?

aussi appelé apprentissage automatique, le but d'un modèle est de prédire des données à
partir de données d'entrées, nous verrons un exemple juste après.

Algorithme qui prends des données étiquetés et va chercher à faire un lien entre elles afin
d'être capable de généraliser/prédire.

Préparer un Modèle de machine learning

**étape 1**
Préparer un ensemble de données pour l'entrainement (étiquetées ou non) qu'on appelle le
set training.
plus il y a de données plus le modèle sera  perfomant.

**étape 2**
entrainement en faisant varier les constantes pour finir par avoir des réponses correctes.
on lui donnes les variables de base puis il compare avec ce qu'il aurait du obtenir
on répète ce processus plusieurs fois

**étape 3**
extrapolation: on teste le modèle sur de nouvelle données
Autrement dit le but de l'algorithme est d'extrapoler comme nous en controle sur

cas plus dure: fil instagram, reconnaissance d'image 
besoin de réseau de neurones (artificiel): mime des neurones, empilement de neurones

phase entrainement: faire varier les poids

3 couches: entrée, sortie et couche neurone + ou - dense

défaut: nb limites d'infos de départ => pré-algo d'extraction de caractéristiques

Deep learning
=============

réseau de neurones **profond** plus performant pls couches


10aines couches possible grace à:

- puissances des cartes graphique
- algo plus dev. architecture meeilleure
- **dispo. des données** (+15M images classifié images.net)

==> trouve cara. tout seul, comme s'il avait algo extraction

alpha go =  deep learning (apprentissage supervisé) analyse d'abord 100aines M de
parties.
2 réseau: choix des branches de l'abre et évaluation des points 

Reinforcement learning
======================

apprentissage par renforcement: seulement avec les règles pas d'humains
donc diff de non-supervisé(doit trouver un lien entre les données tout seul) et
supervisé (on lui donne des données étiquetées)
encore plus autonome
essaie-erreur => + - récompensé

alpha zero entrainé contre lui-meme 140 M de parties
gagne 100-0 contre alpha go


Pb: créativité ? 

https://www.youtube.com/watch?v=kopoLzvh5jY
Deux équipes, les rouges doivent atteindre les bleus.

Ils peuvent bouger et bloquer les cubes et peuvent monter sur les rampes.

Après des milliers de parties, les bleus comprennent qu'ils faut utiliser les cubes pour
bloquer les trous, ensuite les rouges apprennent à monter sur les rampes, mais les bleus
devinent qu'il faut garder les rampes hors de porter des rouges.

On simule dans de nouveaux environnements où ils doivent bouger et construire leur abris
à partir de murs déplacables. Puis des millions de parties plus tard il y arrivent
cependant les rouges découvrent un bug qui, grace aux rampes, leur permet d'outrepasser
les murs. Les bleus sont donc forcés d'immobiliser les rampes avant de constuire leur
abris.

Il est important de noter que ces "évolutions" ont eu lieu après de milliards de parties
jouées entre eux .
