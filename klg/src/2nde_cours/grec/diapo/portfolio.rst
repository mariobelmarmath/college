:css: .my.css

.. footer::

   Mario

.. title:: Les Travaux et les jours - Vinland saga

.. figure:: ./img/vs_12.jpg 
   :width: 400px


.. figure:: ./img/vs_t6.jpg
   :width: 400px


.. figure:: ./img/vs_t9.jpg
   :width: 400px


.. figure:: ./img/vs_t11.jpg
   :width: 400px

Les Travaux et les jours - Hésiode
==================================

Auteur
------

Se nomme ainsi au vers 22 de la Théogonie

- Les Travaux et les Jours
- Théogonie
- Le bouclier d'Héraclès (combat Heraclès -- Kycnos)

hexamètre dactylique (langue traditionnelle de l'épopée épique)

influence béotienne / continentale

datation: VIII - VII siècle

----


Place de l'auteur
------------------

Hésiode: "confessions hésiodiques":

- se nomme vers 22 Th
- parle d'experience personnelle
- mentionne son frère Persès
- 1ere personne

Déstiné a son frère Persès et aux rois

- marginalité dans TJ -> lui permet d'obtenir le statut de "maitre de vérité" 
- scénario autobiographique TJ -> donne un role précis à chaque personnage 
- Perses n'est qu'un instrument autobiographique

----

Oeuvre
-------


**Composition:**

#.  v.1-10 : proème/invocation aux Muses
#.  v.11-26 : dev. de l'existence des 2 Luttes
#.  v.27-41 : exhortation à Persès poru régler leur différends
#.  v.42-105: mythe de Pandore
#. v.106-201: mythe des races
#. v.202-285: mise en garde de l'injustice (fables, 2 cités)
#. v.286-334: exhortation au travail
#. v.335-385: conseils sociétaux, économiques et religieux
#. v.383-617: calendrier des travaux des champs
#. v.618-694: conseil sur la navigation
#. v.695-764: à nouveaux des conseils
#. v.764-828: almanach des jours
#. v.286-334: exhortation au travail


----

1. Invoacation au muses différentes.

2. Il y a 2 Eris (filles de Nuit = lutte) une qui motive l'homme et l'autre à
   s'améliorer pousse à a se disputer avec son voisin.

4. Pandore étiologie de la nécéssité de l'homme à travailler.

   - Travail --> accumulations des biens --> inégalité --> instauration d'une justice

5. mythe des races raconte l'histoire progressive de notre éloignement avec les dieux.
   Leur sort dépend du respect de la justice dans leur société respective.
   Nous sommes la race de fer et devons faire un choix: justice ou injustice

6. La justice, fille de Zeus sauve les hommes. l'auteur cherche a montrer aux rois le
   chemin à prendre.

7. Le travail et la paresse amène soit l'abondance, soit le malheur.

8. Conseils sur les rituelles religieux et  comportement avec autrui.

10. La navigation est un complément pour écouler ses productions et Hésiode
    met en garde des dangers de la mer

11. L'idéal est l'autarcie. Cela passe par la possession d'outils et animaux de traits

12. Dates importantes de la moissons et de la semaille

13. Persès doit faire ce choix écouter ou ignorer les paroles de son frère pour quitter
    la mendicité.

Persès -> περθω = "destructeur d'une ville".
La violence laisse place à l'agriculture, l'opulence et l'accumulation des biens. La
gloire héroïque est remplacer par la réputation d'un homme

----

Vinland Saga - Makoto Yukimura
==============================

Auteur, présentation et oeuvres antérieures
-------------------------------------------

Vinlande Saga est un manga publié en avril 2005 par Makoto Yukimura qui compte
aujourd'hui 27 tomes et 2 saison animés sorties en 2019 et 2023.

Prologue de 8 tomes
-------------------

- Gore et violente au début

Prologue de 8 tomes / 24 épisodes

----

Arc des esclaves
----------------

.. figure:: ./img/vs_t6.jpg
   :width: 400px

.. figure:: ./img/vs_t9.jpg
   :width: 400px

----

Arc des esclaves
----------------

.. figure:: ./img/vs_12.jpg 
   :width: 400px

.. figure:: ./img/vs_t11.jpg
   :width: 400px

----

Arc des esclaves
----------------

But de cet arc sera de lui trouver un nouvel objectif, tous les personnages et
évenements seront des mirroirs et des lecons pour lui.


ferme est l'unique lieu cela permet de se concentrer sur Thorfinn.  `Carte <https://pbs.twimg.com/media/FIRhjA3XoAg-ZtF.jpg:large>`_

Einar (esclave) deteste guerre, seul ami et frere de thorfinn.

Arneis (esclave): maitresse du chef

Famille du propriétaire:

- chef famille: homme bon
- fils ormar: reve d'etre guerrier
- fils thorgeis: excellent guerrier 
- père du chef: très vieux mais continu de travailler la terre


----

Comparaison
===========

- **Lieu**
- **Guerre fléau de l'agriculteur**
- **portée initiatique vs didactique**
- **Nombres de personnages et visions du monde**
- **Agriculture**
- **Esclavage**
- **Monde miniature**
- **Accumulation de richesse**
- **Roi**


