Les Travaux et les jours - Hésiode
==================================

Auteur
------

Se nomme ainsi au vers 22 de la Théogonie

Anciens attribuent à Hésiode:

- Les Travaux et les Jours
- Théogonie
- Le bouclier d'Héraclès (combat Heraclès -- Kycnos)

Ecrit comme Homère: hexamètre dactylique (langue traditionnelle de l'épopée épique)

influence béotienne / continentale

datation: VIII - VII siècle

Homère ou Hésiode d'abord ?

Culte attribuer à Hésiode


Place de l'auteur
------------------

Homère: narrateur discret

Hésiode: présent par ses "confessions hésiodiques":

- se nomme vers 22 Th
- parle de son experience à la navigation
- proème pour son frère Persès -> autobiographique
- 1ere personne

Déstiné a son frère Persès pour qu'il travaille et aux rois

Question de l'éxistence et la véracité de l'auteur pas importante car la raison de cette
posture autobiographique plus importante:

- marginalité dans TJ -> lui permet d'obtenir le statut de "maitre de vérité" selon
  Marcel Détienne car le but de TJ est de convertir son auditeur à la sagesse qu'il
  délivre
- scénario autobiographique TJ -> donne un role précis à chaque personnage (le sage, les
  naïfs, un père)
- le frère Perses n'est qu'un instrument autobiographique, un auditeur fictif, on le voit quand les conseils
  donnés s'applique pour tout le monde

Oeuvre
-------


1er poeme didactique

TJ suite de Th: après les dieux -> la vie des mortels sous le règne incontestable de
Zeus "assis en son palais très haut" v.8

recommandation religieuses, morales, sociales, agricoles

nécéssité du travail -> première parties

jours -> deuxième parties

Thèmes principaux: traviail/labeur et la justice(dikèœ)

caractère hétéroclite (fait de parties): construction de TJ très détachés récits
mythiques, allégories, fables etc...

cohérence apparait à la fin

**Composition:**

-  v.1-10 : proème/invocation aux Muses
-  v.11-26 : dev. de l'existence des 2 Luttes
-  v.27-41 : exhortation à Persès poru régler leur différends
-  v.42-105: mythe de Pandore
- v.106-201: mythe des races
- v.202-285: mise en garde de l'injustice (fables, 2 cités)
- v.286-334: exhortation au travail
- v.335-385: conseils sociétaux, économiques et religieux
- v.383-617: calendrier des travaux des champs
- v.618-694: conseil sur la navigation
- v.695-764: à nouveaux des conseils
- v.764-828: almanach des jours286-334: exhortation au travail

Invoacation au muses différentes: célèbre la grandeur de Zeus, prévient qu'il va
annoncer des paroles véridiques.

2 Eris: filles de Nuit (dans la Th il n'y en a qu'une), qui représentent la lutte, celle
qui motive l'homme à s'améliorer et à plus travailler et celle qui le pousse à a se
disputer avec son voisin ce qui conduit à la guerre.
Commencement du dédoublement de tous les concept(honte, espoir, etc..)

Pandore sert à expliquer la nécéssité de l'homme à travailler -> formate la condition
biologique des humains, nous devenons des "ventres", condammer à mourrir tandis qu'avant
nous vivions sans peines.
Le travail entraine l'inégalité des richessse par l'accumulation des biens et il est
donc nécéssaire d'instaurer une justice pour ne pas tomber dans son contraire l'hybris

mythe des races raconte l'histoire progressive de notre éloignement avec les dieux. Mais
éloignement non linéaire, l'avant dernière race (héros) est meilleure que celle
d'argent. Leur sort dépend du respect de la justice dans leur société respective. Ainsi
c'est à nous, la race de fer, de faire le choix entre une société juste qui apporte le
bonheur ou une société malsaine pleine de démesure qui s'éteindra rapidement, ce choix
s'applique aussi à Perses. C'est un histoire qui s'étend sur des siècles à la différence
du mythe de pandore qui ne compte que deux périodes: avec ou sans la Femme.

La justice, fille de Zeus sauve les hommes, exemple avec les 2 cités gouvernés par un
bon et un mauvais roisn, l'une pleine de malheur et l'autre paisible, l'auteur cherche a
montrer aux rois le chemin à prendre.

Tout comme la dikè et l'hybris, le travail et la paresse amène soit l'abondance, la
félicité soit le malheur c'est le cas de Perses.

conseils sur les rituelles religieux, comportement avec autrui: accepter ou non les dons
d'autrui, savoir les rendre, pour ne pas attiré les autres hommes et ne pas enerver les
dieuxL'auteur dit aussi d'etre méfiant envers les femme (qu'il associe à Pandore).

L'idéal est l'autarcie, n'etre dépendant de rien au contraire du mendiant paresseux.
Cela passe par la possession d'outils et animaux de traits, il conseille d'avoir 2
araire, de savoir fabriquer ses propres outils. Les grecs cultivaient essentiellement du
blé de l'orge, il n'y est pas mentionné de l'élevage de bovins ou autre.

Les dates importantes sont celle de la moissons et de la semaille

La navigation est vue commme un complément pour écouler ses productions et Hésiode met
en garde des dangers de la mer

Persès doit faire ce choix écouter ou ignorer les paroles de son frère pour quitter la
mendicité.

Persès -> περθω = mettre à sac une cité, destructeur d'une ville alors que dans TJ, la
guerre et le guerrier sont les fléaux, dans cette société la violence laisse place à
l'agriculture, l'opulence et l'accumulation des biens. La gloire héroïque est remplacer
par la réputation d'un homme
