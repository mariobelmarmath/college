Vinland Saga - Makoto Yukimura
==============================

Auteur, présentation et oeuvres antérieures
-------------------------------------------

Vinlande Saga est un manga publié en avril 2005 par Makoto Yukimura qui compte
aujourd'hui 27 tomes et 2 saison animés sorties en 2019 et 2023.

L'auteur était déjà connu par son oeuvre *Planètes*, parcours initiatique dans l'espace
qui nous questionne sur la place de l'homme

Prologue de 8 tomes
-------------------

L'oeuvre est présenté comme une saga gore et sanguinaire avec les personnages clichés
des histoires vikings où la violence est omniprésente.

Prologue de 8 tomes / 24 épisodes:

Tout commence en islande, thorfinn a 6 ans rêve de faire la guerre tandis que son père,
ancien guerrier essaie de lui enseigner le pacifisme. Mais son père se fera tué par
Askeladd sous les yeux de Thorfinn qui cherchera à le tuer pendant toute son enfance
mais en cherchant à atteindre son but il deviendra le chien de guerre d'Askeladd, tuant
sans réfléchir et décimant des villages entiers.

Askeladd soutient le prince Knut, mais ce dernier finit par tuer Askeladd et lorsque
Thorfinn essaie de tuer Knut devenu roi il devient esclave.

Arc des esclaves
----------------

Thorfinn est vide sans raison de vivre donc le but de cet arc sera de lui trouver un
nouvel objectif, tous les personnages et évenements seront des mirroirs et des lecons
pour lui.

1 ans et demi a passé on retrouve Thorfinn, esclave dans une ferme, accompagné de son
camarade qui lui vient d'arriver, ancien fermier dont la famille a été ravagé par la
guerre et la lutte de pouvoir

ferme unique lieu --> centrer sur Thorfinn
plus réaliste
Einar deteste guerre / guerrier = seule ami/frere de thorfinn

Famille ketil:
chef famille: homme bon, travailleur, juste envers ses esclaves(acceptait de libérer
thorfinn et einar)
fils ormar: lache, reve d'etre guerrier mais n'y connait rien et est faible, doit
hérité du domaine.
fils thorgeis: contraire de son frere, excellent guerrier ancien garde du roi
père du chef: très vieux mais continu de travailler la terre

esclave:
Arneis: maitresse du chef

organisation de la ferme comme une société, différentes classes sociales: chef, servant,
guerrier, esclave

la ferme représente donc un monde miniature qui permet à thorfinn de choses qu'il n'a jamais vu
étant guerrier.

Thorfinn et Einar vont cultiver pour racheter leur liberté, en reconstruisant ce qu'il
a détruit auparavant, Thorfinn regagnera le droit de vivre

Serviteur supporte pas esclaves aussi bien traité donc détruire leur champ. Thorfinn le
frappe a la place de Einar, reflexe de son passé mais aussi pour éviter à Einar de faire
du mal. Cet épisode montre la difficulté à ne pas avoir recours à la violence et
Thorfinn cherchera encore longtemps où placer le curseur.

Cet scène révèle un thème majeur de l'oeuvre celui du cycle de haine.
Les servants vont violenter les esclaves pour flatter leur égo et Thorfinn
répondront par la violence, c'est pourquoi il a empecher Einar d'entrer dans ce cylce
perpetuel.

Cet confrontation qui a peu de conséquence est reproduite à bien plus grande échelle
entre ketil, propriétaire d'une grande terre mais minuscule face au roi Knut et dont les
conséquences seront toutes aussi décuplées.

Lorsque ketil et ses fils iront offrir un présent au roi Knut (exterieur de la ferme).
Knut ayant besoin de la ferme de Ketil va provoquer un conflit: Ormar se ridiculisera
devant le roi et pour laver son honneur Ormar finira par tuer un garde du roi. Ormar
commence à comprendre le manque de sens de son geste.

Ketil et sa famille rentre à la ferme et se prépare à la guerre, qui se transforme en
carnage mais le maitre de la ferme refuse de cesser le combat. Ormar comprend
que le courage est aussi d'endurer la violence subie et de ne pas répondre encore plus
violement et évite qu'il y ait plus de mort en arretant la guerre. Ormar, par son
parcours, est le mirroir de Thorfinn.

Pendant l'absence de Ketil un esclave, Gardar, s'échappe, il est l'ancien mari d'Arneis
devenu esclave après etre parti à la guerre pour le profit. Puis lorsqu'il apercoit sa
femme il devient fou et essaie de s'enfuir et tue plusieurs mercenaires Serpent va
tenter de  tuer Gardar, a ce moment là Thorfinn jugera l'usage de la violence nécéssaire
mais Gardar finira par mourir.

Mais lorsque Ketil apprendra la tentative de fuite d'Arneis en rentrant il sera furieux
et la battra alors meme qu'elle portait son fils. Ayant perdu toute envie de vivre elle
mourra. Einar voudra se venger en tuant à son tour Ketil mais Thorfinn lui suppliera de
ne pas faire comme lui enfant.

Afin de mettre fin à la guerre, Thorfinn ira parler roi, mais pour que les solddats le
laisse obtenir cette entrevue il doit se batte, cependant il refuse d'avoir recours à la
violence et préfère faire un pari, s'il reçoit 100 coups alors ils le laisseront voir le
roi. Thorfinn remet en question l'usage de la violence pour parvenir à ses fins,
pratique omniprésente dans cette société.
