Comparaison
===========

**Guerre fléau de l'agriculteur**

La guerre ravage les cultures, comme l'a fait Thorfinn plus jeune, comme les serviteurs
ont détruit leurs champs et comme Knut a ravagé et massacré la population de la ferme
lors de sa guerre. Dans cet arc le guerrier lui-meme est mal vue, Thorgeis en est un bon
exemple, il est montré comme une brute qui prend du plaisir à tuer et pertube paix des
champs. Dans les travaux et les jours cela correspond à la mauvaise lutte et
aux races d'argent et de bronze, l'étymologie du prénom Persès l'associe aux guerriers,
"destructeur de villes" exactement comme Thorfinn, ancien guerrier sombré dans la
misère qui doit cultiver pour retrouver sa liberté.

**portée initiatique vs didactique**

Cet arc a servi à donner un but à Thorfinn et à évoluer en renonçant à la violence, il
nous moins adressé que le récit d'Hésiode qui cherche à faire le choix entre justice et
démesure

**Nombres de personnages et visions du monde**

pluralité des personnages --> différentes visions du monde et donc une meilleure remise
en question.
1 personnage qui se propose comme maitre de la vérité presqu'unique

**Agriculture**

TJ est bien plus un manuel de l'agriculture que VS, dans ce dernier on ne trouve que
peut d'explication et de conseil agricole bien qu'on voit les personnages
travaillés.

place du travail des champs central dans TJ, permet d'obtenir la paix et dans VS il
permet de gagner leur liberté, il n'est qu'un objectif secondaire meme si la symbolique
de naissance du blé est importante dans le développement de Thorfinn

**Esclavage**

Le but final de Thorfinn est de créer un pays sans guerre ni esclavage car ce dernier
est un mal aussi nocif que la guerre tandis que pour Hésiode cette pratique n'est pas
remise en question et est très conseillé pour les corvées.

**Monde miniature**

Les deux auteurs essaient de peindre un société complète, Hésiode avec l'agriculteur au
centre, ses voisins et les rois, le tout régit par les dieux. Yukimura, expose tous les
roles d'une société dans la ferme de Ketil: roi, propriétaire, soldats/guerriers, homme
libres, esclaves etc...

**Accumulation de richesse**

Dans TJ l'accumulation de richesse est bien vue, la violence guerrière à laisser place à
sa réputation tandis que dans VS plusieurs personnages ont pour but de dénoncer cet
cupidité, le grand-père critique son fils, propriétaire du domaine, car selon lui il est
inutile de posséder un champ plus grand qu'on ne peut cultivé soit-meme et en effet cela
causera la perte du domaine. Ensuite, Gardar, l'esclave devenu fou était parti à la
guerre pour obtenir une mine de fer alors que son village en possédait déjà assez.

**Roi** 

Les Rois sont, dans les Travaux et les jours, particulièrement visés car c'est en
fonction de leurs choix que leur cité et la vie de leurs sujets sera misérable ou
paisible. Cela dépend de leur volonté d'inclure ou d'exclure la dikè fille de zeus.
Dans Vinland Saga, l'auteur oppose deux personnes l'une déjà roi et l'autre futur roi,
mais le roi Knut incarne presque le Roi que Machiavel voulait créer, il justifie les
moyens par son objectif il va ainsi perpétuer le cycle de haine tandis que Thorfinn
cherche à créer un royaume sans guerre ni esclavage.

