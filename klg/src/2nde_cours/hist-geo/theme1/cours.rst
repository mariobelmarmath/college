Thème 1: Sociétés et Environements
======================================

Etude de cas: L'arctique
------------------------

**Pourquoi ce milieu fragile et contraignant est-il attractif ?**

Le cercle polaire Arctique est le secteur ou la température moyenne pendant lemois le plus chaud et en
inférieur a +10°C.
Il comprend plusieurs pays:

  - Groenland
  - Norvège 1/2
  - Islande 1/2
  - Suede
  - Finlande
  - Nord de la Russie
  - Nord des USA

Et plusieurs ethnies:

  - 650K USA
  - 2M Russes
  - 130K Canadien
  - 57K Danois
  - 45K habitants iles feroe
  - 450K Norvégiens
  - 10K populations indigènes (inuit, samoyenne)

La zone arctique est très disputé entre les puissances de la région pour le zones
petrolières, zone de recherche, zone de pêches, zone strict pour contourner les routes.

Activité tradicionnelles: pêches, élévage et chasse.
Activité moderne: extraction de pétroles/gaz et transports maritimes.
Réchauffement climatique:

  - avantages: fonte de la banquise ouvre de nouveaux accès au ressources et de
    nouvelles routes.
  - défauts: destruction de l'écosysteme, dépopulation, augmentation du niveau de l'eau
    à cause des fontes de glaciers

**ZEE**: zone économique exclusif, elle n'appartient qu'a la personne ou au group de
personnes.

**autochtones**: originaire du lieu dont on parle.

**pergelisol**: sol geler en permanence

**calotte glacière**: Type de glacier formant une étendu de glace très grande (<50 km2)

**banquise**: amas de glace flottante formant un immense banc

Chapitre 1: Des sociétés face au risques
----------------------------------------

**sociétés**: groupe d'être humains organisés ayant des relations durable qui vivent
sous des lois communes et forme de vie commune.

**environnement**: l'ensemble des éléments naturels et artificiels au milieu duquel vit
l'Homme

Problématiques:

  - Dans quels mesures l'équilibre entre sociétés et environements est- il fragile du
    fait de leurs interaction ?
  - Comment réaliser une transition durable rétablissant un équilibre humain et la
    nécéssité de protéger l'environement ?
  - Comment rétablir un équilibre entre sociétés et l'environement fragilisé du fait de
    l'intercation entre les deux ?

Ces sociétés humaines sont soumises à des risques nombreux et variés qui peuvent avoir
de grave conséqences (en cas de catastrophes).
Ces risques peuvent découler de differents aléas mais qui varient selon les régions du
monde,  en effet toutes les régions ne présentent pas toutes les mêmes vulnérabilité
et les mêmes réaction face à ces phénomènes.
Elle varie en fonction des enjeux présents.

**Aléas**: phénomène naturel ou anthropique pouvant devinir un risque s'il menace une
poulation

**risque**: phénomène représentant un danger direct pour une population en fonction de la
nature des aléas et leur vulnérabilité.

**Vulnérablité** = f(lieu)
+ vulnérable en ville qu'en campagne
risque majeur: rare et provoque des dégats imp. + nbs victimes et la société et
les secours ont du mal a faire face ex: Maroc
La vulnérabilité des sociétés humaines dépend de pls facteurs:

  - l'environnement: plaine -, montagne +
  - la pop.: désert humain ou centre tokyo
  - dev. du pays -> construire des défenses
  - décisions pol.: choix diff.

A) Les sociétés sont exposées à de nbs risques
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1) De l'aléas aux risques comment passe-t-on de l'un à l'autre ?
````````````````````````````````````````````````````````````````

  - risques technologiques: causés par l'homme ex. centrale nucléaire
  - risques naturels: ex. tsunami, tremble. terre
  - risques combinés: les deux risques combinés

2) Des risques d'origine naturel varié aux risques anthropiques
```````````````````````````````````````````````````````````````

anthropique: lien avec l'Homme en tant qu'espcèce qui est en lien direc/indirect.
Les espaces littoraux et les reliefs sont globalement plus exposés aux risques on parle
de risques spécifiques a certains milieux.

  - risques littoraux: innondations, tsunami, proximité de zone sismique
  - risques reliefs: volcan, glissement de terrain, avalanche

a) Risques anthropiques, technologiques et industrielles
++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Les aléas technologiques sont dits anthropiques car ils sont dus direct. ou indirect. à
l'action de l'homme, il peut s'agir d'accident indus. ou techno., pratiques risqués, de
défauts d'infra. ou de certaines act. humaines.

  - Marée noire
  - catastrophe chimiques
  - accidents et catastrophe nucléaire

b) des risques aggravés par les act. humaines
+++++++++++++++++++++++++++++++++++++++++++++

Risques combinés dont les effets, en cas de cata., peuvent etre démultiplier du fait de
la présence du risque d'origine natu et techno.


Exercice 10-13
--------------


1) Prévention: digues mise en place d'alerte et de prévisions, préservation de la
mangrove(espace aquatique bordé d'abres, dont l'eau est somatre)

2) Les acteurs qui interviennent sont l'ONU, le gouv. du bangladesh, et des ONG

3) situation de départ: sous l'eau, traversé par des fleuves qui s'aggrandissent a cause
du réchauffement climatique --> pays soumis aux inondations == VULNéRABILITé
Sol imprégné d'arsenic --> eau peut etre empoisonné

Progrès: jardins flottants, digues

4) Peut d'infra. pauvreté pop.
