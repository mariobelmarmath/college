La question des discriminations
===============================

Un état des lieux
-----------------

**Rapport de l'institut montaigne**

L'institut Montaigne a réaliser une étude entre 2013 et 2014 des candidatures fictives
ont été envoyé a plus de 6200 offres d'emploi afin de comparer les taux de convacation ç
un entretien d'embauche de candidates et canditats dont les profils sont identiques en
tout point, à l'exception de la religion. Les résultats révèlent une forte
discrimination à l'égard des candidats percu comme juifs et musulmans.

La probalité des catholiques pratiquants d'être contacté par un recruteur est supérieur
de 30% à celle des candidats juifs, et est 2 fois plus important que pour les candidats
musulmans(Michel décroche un entretien d'embauche au bout de 5 envois, Dov 7 et Mohammed
20)

**Article du journal "le Monde" sur la discrimination lors de la séléction en master**

A la simple demande d'information "Valérie Leroy" est celle qui recoient le plus de
réponses 69%, 61% pour Mohamed Messaoudi et 60% Rachida Saidi

**Vidéo France 2: discriminations à l'embauche**

A l'aide de cet vidéo nous prenons conscience qu'il existe des discriminations à
l'embauche en france selon l'origine ethnique, des jeunes diplomés sont obligés de
s'exiler à Dubaï pour trouver un emploi


**Question: Pensez-vous qu'il existe des discriminations ethniques en france ?**

La réponse est certaine, il existe des discriminations ethniques. Elles sont très
présentes dans le monde du travail comme le revèle les trois documents étudiés, on y
observe des discriminations liés aux religions: des personnes catholiques ont plus de
chance d'obtenir du travail lorsqu'a l'inverse il est bien plus dure d'en trouver pour
les personne de religions musulmanes ou simplement portant un patronyme associé au
magrèbe.
