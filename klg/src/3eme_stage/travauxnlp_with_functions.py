from math import floor
from statistics import median
import pandas as pd
import spacy

nlp = spacy.load("en_core_web_sm")

FILES = [
    "ani_2000_o.txt",
     "aari_1994_o.txt",
     "abun_19952_o.txt",
     "abu_1985_o.txt",
     "abun_19952_o.txt",
     "abun_1999_o.txt",
     "aari_1990_o.txt",
     "abun_1995_o.txt"]


def get_text_from_file(files: str) -> str:
    with open(files, 'r') as myfile:
        text = myfile.read()
    return text


def test_all_files(text):
    doc = nlp(text)
    tones = [token for token in doc if token.text in ['tone', 'tones']]
    genders = [token for token in doc if token.text in ['gender', 'genders']]
    return genders, tones


def medianne(ll: list[int]) -> int:
    """
    if even: e.g. l = [1, 7, 6, 0]
                          ^  ^
                          L  R
                          1  2
      median = (L + R)/2
    else:
      # e.g. l = [2, 6, 6 , 30, 5]
                        ^
                        C idx:2
      len(l)/2 == 2.5
      floor(2.x) == 2
      median = l[floor(len(l)/2)]
    """
    liste = ll.copy()
    liste.sort()
    size = len(liste)
    if size % 2 == 0:
        R_idx = int((size / 2))
        L_idx = int((size / 2) - 1)
        median = (liste[L_idx] + liste[R_idx]) / 2
    else:
        mid = int(floor(size/2))
        median = liste[mid]
    return median


def has_key(key_diff, treash_hold=1):
    if key_diff > treash_hold:
         has_key = True
    elif key_diff < -treash_hold:
         has_key = False
    else:
         has_key = None
    return has_key


def update_dict(gram_file, key_name, mediane, key_list):
    """
    update_dict(gram_file, key_name='genders', mediane=mediane, key_list=genders)
    """
    line = {'langue': gram_file[:-4], 'median_occurence': mediane}
    key_diff = int(len(key_list) - mediane)
    line.update({'gender/tone': key_name,
                 'k.occu': len(key_list),
                 'difference': key_diff,
                 'hasornot': has_key(key_diff)})
    return line


def put_test_in_table (FILES):
    lines = []
    for gram_file in FILES:
        filename = f'grammartext/{gram_file}'
        text = get_text_from_file(filename)
        doc = nlp(text)
        genders, tones = test_all_files(text)
        words = [token.lemma_ for token in doc if token.pos_ not in ['PUNCT', 'SPACE']]
        mediane = medianne([words.count(t) for t in set(words)])

        line1 = update_dict(gram_file, 'genders', mediane, genders)
        line2 = update_dict(gram_file, 'tones', mediane, tones)
        lines.append(line1)
        lines.append(line2)
    return pd.DataFrame(lines)


if __name__ == '__main__':
    df = put_test_in_table(FILES)
    print(df)
    df = df.sort_values(by=['k.occu', 'difference'], ascending=[True, True])
    print(df)
    df.to_csv('langues_has_gender_tone.csv')
    df.reset_index().to_feather('langues_has_gender_tone.feather')
    print('==> langues_has_gender_tone csv and feather saved!')

