Conclusion
==========

Ce stage m’a permis de connaitre de l’intérieur le fonctionnement d’un laboratoire de
recherche, de voir comment se déroule une journée au travail à
travers les différentes taches que j’ai eu à accomplir (trié de documents officiels
importants et dilution d'ADN) et auquelles j'ai pu assisté (2 soutenance de thèse et 1
HDR).

J’ai été agréablement surpris par le bon accueil qui m’a été réservé et par les rapports
que j’ai pu créer avec la totalité des chercheurs qui ont pris de leur temps pour m'
expliquer en quoi consistait leur travail et pour répondre à mes questions.

Je ne pense pas vouloir travailler dans ce laboratoire mais j'espère avoir un lieu de
travail avec la meme atmosphère générale entre collègues.

Merci à mon maitre de stage, dont je n'ai jamais cité le nom par respect, Monsieur
Allasonière-Tang, à Madame Lahrem qui m'a beaucoup aidé en repondant à mes questions et en me
donnant de précieuses informations pour ce rapport de stage.

Merci aussi à tous les chercheurs qui m'ont présenté leur travail: je pense surtout à
Liliana HUET qui m'a fait visiter les collections anthropologiques, qui ne sont et seront
jamais exposées. Je remercie aussi Monsieur Debruyne qui m'a permis de diluer de l'ADN.

.. Bibliographie
.. =============

.. Article de Marc:
..     - :cite:p:`her2022defining`
..     - :cite:p:`ulrich2021identifying`
..     - :cite:p:`hammarstrom2020term`

.. Test de citation d'un article sur les nuages de mots :cite:p:`d2014recueils` pour le
.. voir dans la Bibliography de la fin du document.
.. Puis un test de citation en note de bas de page ici :footcite:p:`d2014recueils`

bibliography::

footbibliography::

Annexe
======

Annexe: Activité du laboratoire
-------------------------------

Les secteurs d'activité du laboratoire UMR7206 sont la Recherche, l'enseignement,
la gestion des collections, la diffusion et la valorisations de la recherche.

- Recherche: Découvrir, apprendre pour ensuite faire de la diffusion et de la
  valorisation.

- Enseignement: Donner des cours dans les universités, c'est essentiellement le
  travail des enseignants chercheurs

- Collections: Restaurer, entretenir, répertorier les collections du muséum

- Diffusion: Rendre le travail des chercheurs "publics", l'expliquer à la télé
  pour informer des découvertes, par exemple.

- Valorisation: Plus sérieux que la Diffusion: il s'agit de puiblier ses recherches dans
  une revue scientifique.

Annexe: Soutien administratif
------------------------------

Tri des documents avec Taoues

- Taoues s'occupe de toute l'administration du laboratoire EA UMR 7206:
  elle est la Responsable administrative et la secrétaire générale.

- Elle nous a donc, à moi et d'autres stagiaires, donner pour taches de trier les
  documents du laboratoire.

- Nous devions séparer ces documents d'abord par établissement (CNRS et MNHN),
  puis isoler les bons de commandes des ordres de missions.

  - Bon de commande: le muséum(MNHN) ou le CNRS a besoin d'un nouvel objet, il doit donc
    expliquer à la direction pourquoi il en a besoin et combien il en veut.

  - Ordre de mission: Lorsqu'un chercheur, enseignant-chercheur ou support doit partir
    quelque part il doit laisser une trace et dire a la direction ou il part cela
    justifie qu'il ne soit pas au 'bureau'.

- Cette tache nous a pris plus de 10h tout le long de la semaine de stage.

Annexe: Dilution d'ADN
------------------------

Objectif: Que mangeaient les hommes préhistoriques le long de la Loire ?

- Dilution d'ADN d'un échantillon de vertèbre de poisson à l'aide d'une
   micro-pipette

 - déposer diluant
 - déposer adn dans le diluant

- Electrophorèse capillaire


Annexe: Evolution des techniques NLP
-------------------------------------

- Méthodes basées sur des règles

    - résout des problèmes spécifiques (supprimer les spam des boites mail à l'aide de
      mot clés 'promo')
    - rapidement inefficace face à la complexité du langage humain.
- Modèles de Machine Learning

    - compréhension avancée du langage
    - utilise des données pré-traitées
    - utilise d'autre procédés mathématique et statistiques(longueur des phrases,
      occurrence de mots spécifiques)
- Modèles de Deep Learning

    - Beaucoup plus complexes
    - intègrent une énorme quantité de données pour essayer de créer un système proche
      de notre système neuronal


Annexe: Organigramme de la fonction has_key
-------------------------------------------

.. mermaid::

  ---
  title : fonction qui compare la différence à la médiane
  ---

  flowchart TB
  nb("différence")
  ex("égale à mediane - nombre d'occurences")
  nb -.- ex
  N === i(incertitude)
  subgraph Organigramme
    nb -->A{"> 1"}
    A -->|Yes| T("possède cette typologie")
    A -->|No| B{"< -1"}
    B -->|Yes| F("ne possède pas cette typologie")
    B -->|No| N("écart trop petit")
    end

Annexe: Code Extraction de typologies
-------------------------------------

.. code::

   import spacy
   import pandas as pd

   nlp = spacy.load("en_core_web_sm")

   FILES = ["ani_2000_o.txt"]
            "abun_1995_o.txt"]


   def get_text_from_file(file: str) -> str:
       with open(file, 'r') as myfile:
           text = myfile.read()
       return text


   def test_all_files(text):
       doc = nlp(text)
       tones = [token for token in doc if token.text in ['tone', 'tones']]
       genders = [token for token in doc if token.text in ['gender', 'genders']]
       return genders, tones


   def medianne(ll: list[int]) -> int:
       """
       if even: e.g. l = [1, 7, 6, 0]
         len(l)/2 == 2.5
         floor(2.x) == 2
         median = l[floor(len(l)/2)]
       """
       liste = ll.copy()
       liste.sort()
       size = len(liste)
       if size % 2 == 0:
           R_idx = int((size / 2))
           L_idx = int((size / 2) - 1)
           median = (liste[L_idx] + liste[R_idx]) / 2
       else:
           mid = int(floor(size/2))
           median = liste[mid]

       return median


   def has_key(diff):
           if diff > 1:
                has_key = True
           elif diff < -1:
                has_key = False
           else:
                has_key = None
           return has_key


   def get_line(i, name_key, list_key, mediane):
           line = {'langue': i[:-4],
                    'gender/tone': name_key,
                    'k.occu': len(list_key),
                    'median_occurence': mediane,
                    'difference': len(list_key) - mediane,
                    'hasornot': has_key(len(list_key) - mediane)}
           return line


   def put_test_in_table (files):
       lines = []
       for i in files:
           filename = f'grammartext/{i}'
           text = get_text_from_file(filename)
           doc = nlp(text)
           genders, tones = test_all_files(text)
           words = [token.lemma_ for token in doc if token.pos_ not in ['PUNCT', 'SPACE']]
           mediane = int(medianne([words.count(t) for t in set(words)]))
           gender_diff = int(len(genders) - mediane)
           tone_diff  = int(len(tones) - mediane)
           has_gender = has_key(gender_diff)
           has_tone = has_key(tone_diff)
           line1 = get_line(i, 'gender', genders, mediane)
           line2 = get_line(i, 'tons', tones, mediane)
           lines.append(line1)
           lines.append(line2)
       return pd.DataFrame(lines)


   if __name__ == '__main__':
       df = put_test_in_table(FILES)
       cc = ['gender/tone', 'hasornot', 'langue', 'k.occu', 'median_occurence', 'difference']
       df.sort_values(by=['gender/tone', 'difference'])
       df = df.loc[:, cc]
       df.reset_index().to_feather('out2.feather')
       print(df)

