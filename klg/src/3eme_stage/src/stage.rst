Introduction
=============

Je m'appelle mario Belmar Letelier -- Zhou élève de 3°2 du collège Lavoisier.

J'ai effectué mon stage dans un laboratoire de recherche appelé UMR7206.

Je n'ai pas eu de difficultés a trouver mon stage car mon maître de stage était une
personne que je connaissais bien ; nous pratiquions la même activité physique depuis
une année. Lorsque qu'il a entendu que je devais faire un stage, il m'a proposé
de faire une visite de son laboratoire (hors stage).

Cette visite m'a beaucoup plu et j'ai eu l'idée de faire mon stage dans son laboratoire,
je voulais aussi rencontrer les différents métiers qui étaient exercés dans un
laboratoire de recherche.

De plus mon père m'avais déjà initié à la programmation et cela m'avait beaucoup plu.
Ce stage était donc un moyen pour moi de découvrir les applications professionels d'un
domaine qui me plait beaucoup.


Présentation de l’entreprise
=================================

Carte d’identité de l’entreprise
--------------------------------

J'ai donc fait mon stage dans le laboratoire UMR 7206, situé dans le Muséum National
Histoire Naturelle, 57 rue Cuvier a Paris dans le 5ème arrondissement.

Le laboratoire Eco-anthropologie UMR 7206 est une unité mixte de recherche du
Centre National de la Recherche Scientifique (CNRS), du Muséum National d’Histoire
Naturelle(MNHN) et de l'université de paris. C'est donc un laboratoire public.

Le laboratoire a 4 secteurs d’activités: Recherche, Enseignement, Collections,
Diffusion et Valorisation :ref:`src/conclusion:Annexe: activité du laboratoire`

Le laboratoire produit des connaissances et des collections:

- plus de 1500 de plubications scientifiques par an.
- 68 millions de spécimens au sein des collections
- plus de 10 millions de spécimens ou lots informatisés ou numérisés
- Plus de 2 millions de documents, livres, périodiques, ressources électroniques,
  archives, dessins, objets scientifiques ou artistiques dans les bibliothèques.

Le laboratoire UMR 7206 développe des recherches interdisciplinaires sur l'Homme,
individuellement ou collectivement, et ses interactions avec l'environnement Et le
Muséum se consacre depuis des siècles à l’étude de la diversité biologique, géologique
et culturelle, et aux relations entre les humains et la nature.

Le laboratoire lui même n'a pas de client car ce n'est pas un entreprise à but
lucratif Il y a des chercheurs ou des étudiants qui viennent voir les collections
mais ce n'est pas payant et il leur faut une autorisation.

Le Muséum compte plus de 6 millions de personnes accueillies sur ses 13 sites,
dont plus de 3 millions de visiteurs payants. (Plus de 6 millions de visiteurs sur les
sites Internet).

En terme de salariés, le Muséum compte environ 2 512 agents dont 622 agents dits
hébergés, rattachés à d'autres établissements ou organismes de recherche (comme le CNRS,
Université Paris Cité) et près de 177 conventions de bénévolat

Le laboratoire agit au niveau international (par exemple mon maître de stage a un projet
avec des équipes de pays différents).

Les différents métiers
----------------------

.. list-table:: métiers exercés
   :widths: 100 100
   :header-rows: 1
   :stub-columns: 0

   * - Métier
     - études requises
   * - Personnel de support
     - Très variables en fonctions du type de support
   * - Chercheur
     - Doctorat (bac + 8)et il faut passé un concours du CNRS pour etre embauché avec un CDI
   * - Enseignant chercheur
     - Doctorat (bac + 8)et il faut passé un concours du CNRS pour etre embauché avec un CDI

Les études requises pour le personnel de support sont très variables en fonctions de
l'aide que fournis le support effectué(il peut etre informatique, génétique ou
administratif etc..). Les chercheurs et les enseignant chercheur quant à eux ont
obligatoirement besoin d'un doctorat (environ bac+8) et de passer un concours du CNRS.


Description d’un service
------------------------

Mon maitre de stage fait parite de l'équipe "Diversité et évolution
culturelle(laboratoire)". Le laboratoire tout entier s'appelle Laboratoire
d'eco-anthropologie UMR 7206 affilié au CNRS et au Musée de l'homme ainsi qu'à Paris
Université.

Il y a plus de 1200 personnes qui travaillent dans le MNHN, dans le laboratoire UMR 7206:
104 personnes (50 chercheurs, 15 personnels support, 30 doctorants, associés et
collaborateurs). Et dans l'équipe de mon maître de stage, (diversité et évolution
culturelles) il y a 26 personnes.

Concernant les relations entre les différents corps de métiers:

  - **Les Ingénieurs** sont aussi appelés 'personnel de support': leur rôle est d'aider
    les chercheurs dans leur domaine. C'est pourquoi il existe de nombreux d'Ingénieurs
    spécialisés dans différents domaines. Certains sont dans l'administration, d'autre
    s'occupe de la génétique: si un chercheur a besoin d'une analyse adn d'un
    échantillon de salive, l'ingénieur en génétique s'en occupera.
  - A l'UMR 7206 les domaines d'études **des chercheurs** sont tous différents mais
    mais ils entretiennent tous une bonne relation
  - Les **enseignants-chercheurs**, quant à eux était, sont surtout dans les universités(80%
    de leur temps de travail).

Dans le public il y a une énorme liberté de travail: il est demandé de faire 35
heures par semaine mais en réalité le chercheur peut commencé et finir
de travailler quand il veut. On lui demande simplement de produire des articles
scientifiques et de les publier (2 a 3 recherches par an).

.. mermaid::

 ---
 title: ornigramme de l'organisation des différentes branches des département du musé de l'homme
 ---

 flowchart LR
 D-HE(["Département homme <br> et environnement"])
 D-OE("Origines et évolution")
 D-AV("Département <br> adaptation du vivant")
 L-AASPE("Archéozoologie, archéobotanique: <br> sociétés, pratiques et environnements")
 L-CAK("Centre Alexandere Koyré")
 L-CESCO("Centre d'écologie et <br> des sciences de la conservation")
 L-EA(["Eco-anthropologie"])
 L-HNHP("Histoire naturelle <br> de l'homme préhistorique")
 L-PALOC("Patrimoines locaux, <br> environnement et globalisation")
 L-AASPE("Archéozoologie, archéobotanique: <br> sociétés, pratiques et environnements")
 L-CAK("Centre Alexandere Koyré")
 L-HNHP("Histoire naturelle de <br> l'homme préhistorique")
 L-PALOC("Patrimoines locaux, <br> environnement et globalisation")
 E-ABBA("Anthropologie biologique <br> et bio-archéologie")
 E-AG("Anthropologie génétique")
 E-E("Ethnoécologie: savoirs, <br> pratiques, pouvoirs")
 E-IPE("Interactions primates <br> et environnement")
 E-DV(["Diversité et <br> évolution culturelles"])
 E-BD("Biodémographie humaine")

  classDef red fill:#ACACFF
  D-HE:::red ===> L-EA:::red
  D-HE --> L-AASPE
  D-HE --> L-CAK
  D-HE --> L-CESCO
  D-HE --> L-HNHP
  D-HE --> L-PALOC
  L-EA ===> E-DV:::red
  L-EA --> E-ABBA
  L-EA --> E-E
  L-EA --> E-AG
  L-EA --> E-BD
  L-EA --> E-IPE

  subgraph Departement
  D-OE
  D-HE
  D-AV
  end
  subgraph Laboratoire
  L-AASPE
  L-CAK
  L-CESCO
  L-EA
  L-HNHP
  L-PALOC
  end
  subgraph Equipe
  E-ABBA
  E-AG
  E-E
  E-IPE
  E-DV
  E-BD
  end

.. mermaid::

  ---
  title: organigramme du laboratoire UMR 7206
  ---
  flowchart LR
   cnrs(CNRS)
   mnhn("Museum National
         d')Histoire Naturelle")
   univ(Paris Université)
   inge("Ingénieur
         soutient les chercheur")
   cherch("Chercheur
           80% chercheur, 20% enseignant")
   ensei("enseignant-chercheur
         20% chercheur, 80% enseignant")

   cnrs -->|sous_tutelle| labo
   univ -->|sous_tutelle| labo
   mnhn -->|sous_tutelle| labo
   collection -.-> restauration
   collection -.-> exposition
   mnhn --> muse

   subgraph labo["Laboratoire d'éco-anthropologie\n EA UMR 7206"]
    ensei
    cherch
    inge
      subgraph muse[Musée de l'homme]
         collection("gère la collection")
         restauration
         exposition
      end
   end



Evolution de l'entreprise
--------------------------

Histoire

  Créé en 1635, à l'origine jardin royal à vocation médicinale et lieu d'enseignement,
  il est devenu Muséum d'Histoire naturelle à la Révolution, en 1793. Porteur d'un
  héritage scientifique capital et tourné vers l’avenir, il apporte aujourd’hui un
  éclairage scientifique sur les grands défis du XXIe siècle, en France et au-delà des
  frontières. À la fois centre de recherche, musée et université, il mobilise pour
  cela des disciplines, des métiers et des savoirs incomparables qu’il partage dans le
  monde entier.

Progrès

  L'institution rassemble plus de 2 500 personnes, dont 600 chercheurs, abrite 67
  millions de spécimens dans ses réserves et galeries, forme plus de 450 étudiants
  par an et a accueilli 2,1 millions de visiteurs en 2021 dans ses 13 sites, à Paris
  et en région.

  Conservation, enrichissement, valorisation et mise à disposition de collections
  exceptionnelles, recherche fondamentale et appliquée, enseignement
  multidisciplinaire, expertise reconnue à l'échelle nationale et mondiale, action
  éducative et diffusion des savoirs pour instruire un large public : l’étendue de ses
  activités, croisant sciences naturelles, humaines et sociales, le positionne comme
  une référence en matière d’enjeux écologiques et sociaux

Exploration d’un métier
=======================

Description d’un métier
-----------------------

Le travail de chercheur consiste à se documenter et faire des recherches. Pour cela il
est parfois nécessaire de se déplacer sur le lieux d'étude: certains chercheurs partent dans
des pays parfois en guerre car ils ont besoin d'échantillons de la population locale.
Mais le chercheur peut aussi avoir recourt à des données statistiques déjà
collectées sur internet.

Le matériel nécessaire varie selon l'objet d'étude et de la méthode de travail du
chercheur, mon maître de stage, par exemple, n'utilise qu'un ordinateur et un
enregistreur, pour étudier des sons.
Ils peuvent aussi demander au muséum de l'argent pour employer une personne pour certaine
tache et aider les chercheurs dans leur recherches.

Le laboratoire de mon maître de stage et le Muséum d'Histoire Naturelle sont des
établissement public. Les chercheurs peuvent donc gérer eux-même la progression de
leur travail tant que l’objectif final est atteint (publier dans une recherche le
contenu de notre travail).

Interview d’un professionnel
----------------------------


**En quoi consiste votre travail ?**

  Le thème principal de mes recherches portent sur l'interaction entre le langage et
  le système cognitif humain.

**Avant de commencer à travailler, avez-vous fait des études ? Lesquelles ?**

  J'ai fait une licence en littérature arabes, en science politiques et un Master en
  linguistique. J'ai aussi fait un doctorat en linguistique et traitement automatique
  des langues.

**Quel est votre parcours avant de travailler dans cette entreprise ?**

  J'ai travaillé à ASUS en tant que chef de produit. Je devais concevoir les produits
  mais aussi gérer leur ventes et leurs marketing.

**Comment s’est déroulé le processus de votre recrutement dans l’entreprise ou la
création de votre entreprise ?**

  Grace au concours du CNRS qui a lieu tous les ans. J'ai postulé puis j'ai été
  sélectionné, et après un entretien d'embauche,  j'ai été pris. Cela fait plusieurs
  années que je travaille dans le laboratoire UMR 7206.

**Qu’est ce qui vous plaît le plus dans votre travail ?  création de votre entreprise ?**

  - Il y a plus de flexibilité par rapport au privé en terme d'horaires
  - Il y a plus de liberté de gestion de ses objectifs
  - Il y a moins de compétition.
  - Tu peux faire des recherches sur le long terme.
  - Il y a cependant une forte baisse au niveau du salaire. Il est possible que je gagne
    deux fois plus en étant dans le privé.

**Que produisez-vous dans l'entreprise (dans le présent comme dans le passé) ?**

  Mon travail consiste à faire des recherches afin de produire des articles.
  J'ai aussi des projets avec des équipes d'autres pays sur le domaine de la linguistique.

  L'un de mes projets internationaux consiste à automatiser la lecture des grammaires
  c'est à dire des livres qui décrivent la langue d'autre pays.

**Quels outils utilisez-vous ?**

  Personnellement je n'utilise qu'un ordinateur et quelques fois un enregistreur. Mais
  si on parle pour toutes les personnes du laboratoire, on peut avoir besoin du
  laboratoire de génétique pour analyser de l'adn qu'on a trouvé sur le terrain.

**A votre avis, quelles sont les qualités qui sont nécessaires pour pouvoir
faire ce métier ?**

  Je pense que, dans ce laboratoire, le plus important est bien entendu le niveau de
  compétence, c'est la contrepartie a notre liberté de travail. L'intégration, la bonne
  entente entre collègues est aussi très importante, surtout dans notre laboratoire, et
  c'est un aspect qui qui peut etre très apprecié mais qui ne convient pas a tous.
