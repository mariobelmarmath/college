Mon expérience
==============

Programme du 12 au 16 décembre
------------------------------

**Lundi Matin**

- Présentation du laboratoire
- Rapide discussion avec les collègues/autres chercheurs et avec quelques
  autres stagiaires.
- Présentation des locaux de mon maître de stage.

**Lundi après-midi**

- Discussion avec mon maitre de stage  sur ce qu'on allait faire pendant le stage.
- Manuel VALENTIN, ethnologue MNHN Responsable scientifique des collections
  d'ethnologie au Musée de l'Homme.

  - Visite des collections des objets du monde entier contemporain

**Mardi matin**

- Début du tri de documents avec Taoues (:ref:`src/conclusion:Annexe: soutien administratif`)

**Mardi après-midi**

- Soutenance de thèse de Margaux BIEUVILLE, doctorante Université Paris Cité en
  Biodémographie, sur la biologie évolutive et le développement.
- Trier les documents
- Début du code avec Marc(maître de stage)

**Mercredi matin**

- Trier les documents
- Soutenance de thèse de Caroline GERARD, doctorante MNHN en primatologie sur
  la prise alimentaire des bonobos en RDC.

**Mercredi après midi**

- Avancement sur le code
- Trier les documents 

**Jeudi matin**

- Avancement sur le code
- Trier les documents

**Jeudi après midi**

- Marie-Claude Kergoat, Généticienne (Paléogénomique) CEA
- Séminaire de recherche d'Aline THOMAS sur Momies et Insecte
- Liliana HUET, gestionnaire MNHN des collections d'anthropologies et biologiques

  - Visite des collection anthropologique, cranes, squelettes, momies
    (collection jamais exposés) etc..

**Vendredi matin**

- Régis Regis DEBRUYNE, Anthropologue MNHN, paléogénéticien. Travaille sur la
  compréhension de l'évolution des éléphants il analyse donc les ancêtres des
  éléphants(mamouth)

  - Travaille au Musée national d'histoire naturelle pour extraire informations
    génétiques des collections du musé
  - Activités faite: Dillution d'ADN d'un échantillon de vertèbre de poisson
    Cf. :ref:`src/conclusion:Annexe: Dilution d'ADN`

**Vendredi après-midi**

- Nicolas CESARD, ethno-entomologue MNHN
- Paul VERDU, chercheur en génétique des populations CNRS, soutenance HDR sur
  "Histoires de Métissages"

Appréciation personnelle
------------------------

Ce stage a été pour moi une très belle expérience, très enrichissante qui m'a permis de
découvrir l'essentiel du métier. Entouré de toute l'équipe qui m'a expliqué le rôle de
chacun dans l'entreprise, j'ai pu observer les méthodes de travail et constater l'
imortance de la multi-spécialisation dans le domaine de la recherche (il y avait des
linguiste, des généticien, des primatologue, etc...).

A part le tri de documents officiels, j’ai apprécié tous les moments que
j’ai passé au laboratoire et meme lorsque j'ai trié les documents, j'ai appris beacoup
de chose sur l'admnistration et son importance.

Au delà du fait que ce stage a été pour moi une merveilleuse découverte de la vie active,
il ne m'a pas permis de confirmer mon choix quant à mes futurs intérêts professionnels.
Mais la recherche, m’intéresse quand meme énormément.

J'ai aussi pu atteindre mes deux objectifs de programmation, qui sont tous les deux
programmes détaillés dans "Mes travaux de NLP". J'ai appris énormément en codant ces
deux programmes mais c'était aussi, de loin, ma partie préférée de toutes les choses que
j'ai faites. La dilution d'ADN était passionante également.

Après avoir passé une semaine dans le laboratoire UMR 7206, et discuté et observé
beaucoup de chercheurs et d'ingénieurs, j'ai pu trouver de nombreux points positifs et
quelques points négatifs:

**points positifs**

- Liberté de travail(surtout dans les horaires)
- Très bonne atmosphère général du laboratoire
- sécurité d'emploi liée au statut de fonctionnaire: garantie de trouver du
  travail dans une autre société

**points négatifs**

- salaire peu élevé(par rapport à des entreprises privées)
- infrastructure interne (internet, câble) bâclée

Mes travaux de NLP
==================

NLP pour **N** atural **L** anguage **P** rocessing

Objectif
--------
Avant le début du stage, avec mon maître de stage, nous nous étions mis
d'accord pour qu'à la fin du stage j'ai appris à coder et à mettre en application la
théorie et les techniques du NLP; voici donc mes deux objectifs de programmation qui
découlent de cette application:

- Produire un programme capable **d'extraire les verbes** et leurs occurences dans un
  texte.

- Produire un programme capable de révéler la **présence de typologies** dans certaines
  langues à partir de mots clés. Un exemple de typologies peut être l'utilisation de
  **ton** dans certaines langues (le Chinois ou autre) ou bien la présence de **genre**
  dans certaine langues (les langues européennes).

Prise en main du NLP
---------------------

Le NLP est une branche de la programmation qui s'est développée il y a une dizaine
d'années. Son but est que les ordinateurs puissent comprendre les langues humaines mais
surtout qu'ils puissent les analyser et de traiter des textes

Méthode d'apprentissage, théorie
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Il existe pour l'instant 3 grandes familles d’apprentissages utilisées pour les
librairies de NLP cf.  :ref:`src/conclusion:Annexe: Evolution des techniques NLP`

- Méthodes basées sur des règles
- Modèles de Machine Learning
- Modèles de Deep Learning

Librairies et capacités
~~~~~~~~~~~~~~~~~~~~~~~

J'utilise la librairie NLP appelés Spacy, qui est assez récente mais plus rapide.
Voici ce dont est capable Spacy:

- La **tokenisation** ou **word segmentation**: découper une phrase en plusieurs pièces,
  tokens.
  
  - Ex: 'bonjour les amis' -> 'bonjour', 'les', 'amis'
- **lemmatization**: donner la forme canonique du mot, celle de base.

  - Ex: 'trouvaient' -> 'trouver'
- **P.O.S tagging**: à partir de l'endroit où se trouve le verbe dans la phrase on
  assigne au mot(token) sa nature.

  - Ex: 'l'enfant mange une pomme' -> l'enfant : sujet | mange : verbe | etc..
- **dependency parsing**: dépendance à d'autre mots dans la phrase (c'est aussi le
  contexte, un mot peut changer le sens d'un autre mot).

  - Ex: le mot 'que' peut signifier plein de choses

Grace à toutes ces étapes nous serons capables de produire un code qui, par exemple,
trouve le nombre de fois qu’apparaît un mot dans un texte.

Premier code NLP: détection des verbes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
J'ai utilisé le langage appelé Python.  Un langage informatique est comme une langue
humaine, c'est une langue **compréhensible** par l'ordinateur. Il existe énormément de
langages diffèrents.

Premier code Python et Spacy

Mots clés :

- **fonction**: on donne à une fonction une ou plusieurs choses et la fonction nous
  renvoie uneversion transformée de cette chose. Ex: on donne a la fonction deux
  chiffres et elle nous renvoie la somme des deux.

Extraction de verbe
++++++++++++++++++++

On utilise la librairie Spacy: c'est donc l'extension qui me permet de faire
plus de choses (ici de traiter des textes).

.. code ::

  import spacy

Je crée une **fonction** à qui je donne le fichier qu'il va **tokeniser** c'est à dire
rendre lisible afin de l'analyser.

Les différentes étapes de ce programme sont:

- étape 1: récupérer le fichier et le rendre compréhensible pour l'ordinateur
- étape 2: récupérer les verbes du texte

Etape 1: Récupérer le fichier et le rendre compréhensible
_____________________________________________________________

- Je lui donne le 'path' (le chemin) pour récupérer le fichier texte.
- Et il me renvoie le texte lisible par la librairie spacy.  Voici la fonction
  qui fait cela:

.. code ::

  def get_text_from_file:

Etapes 2: récupérer les verbes du texte
___________________________________________

.. code ::

  verbes = [token.lemma_ for token in doc if token.pos_ == "VERB"]

Voilà comment elle fonctionne:

- étape 1: faire la **word segmentation du texte** (découper le texte en mots)...
- étape 2: regarder uniquement la **fonction** des mots (POS tagging) si la fonction est "verbe"...
- étape 3: ajouter le **lema** du mot (forme canonique, ici l'infinitif) a la liste

Après ces deux étapes, il ne reste plus qu'a éxécuter le code.

Extraction de typologies
--------------------------

Introduction
~~~~~~~~~~~~

Les données d'entrées sont une liste de grammaires, (nous avons travaillé sur 9 grammaires
mais le code peut être utilisé sur les grammaires de 7000 langues documentées au
laboratoire)

De tout ce que j'ai fait cette semaine, il s'agit de la partie la plus proche d'un
travail de recherche: détecter la présence de certaine typologies (genre, ton) dans ces
grammaires sur lesquelles travail le laboratoire UMR7206.

Comme expliqué dans 'Objectifs' je vais 'extraire' certaines typologies de certaines
langues. Je vais donc me concentrer sur la présence de tons(ex en chinois mais aussi en
espagnol), et la présence de genre(masc / fem / neut / etc...).
Pour cela je vais choisir des mots clés (ici ce sera très facile) qui seront 'tone' et
'gender' (mes pdf sont en anglais): dans certains cas, il est plus compliqué de trouver
les bons mots clés(pour les systèmes de multiplication)

Ce programme fonctionne en plusieurs étapes:

- étape 1: Compter les mot-clés de chaque typologie
- étape 2: Calculer la médiane d'occurrence de chaque mot du texte définissant la grammaire.
- étape 3: Comparer pour chaque typologie son nombre d'occurrence à la médiane
- étape 4: Arbitrer si la différence est suffisante pour décider de la présence de typologie.

Etape 1: Compter les mot-clés de chaque typologies
++++++++++++++++++++++++++++++++++++++++++++++++++

Ma première fonction me permet de recenser le nombre de fois qu’apparaîssent les mots
'tone' et 'tones'. Cette fonction est assez similaire au programme qui me renvoie les
verbes.

.. code ::

    tones = [token for token in doc if token.text in ['tone', 'tones']]


Etape 2: calculer la médiane d'occurence de chaque mot.
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Après avoir récupéré le nombre d’occurrences de mes mots-clés, je vais pouvoir savoir si
la langue possède bien ces typologies. Pour cela il faudra que je compare ce nombre
d’occurrences par rapport au reste du texte. Il y a plusieurs méthode. Mais je vais
utiliser la méthode de la médiane.

Pour cela j'ai la fonction:

.. code::

   words = [token.lemma_ for token in doc if token.pos_ not in ['PUNCT', 'SPACE']]


Elle fonctionne aussi comme la fonction qui récupère les verbes:

- étape 1: on fait la **word segmentation du texte** (découper le texte en mot)...
- étape 2: cette fois on ne garde **pas** les signes de ponctuation et les espaces...
- étape 3: on ajoute la forme canonique des mots qui ne sont pas les signes de
  ponctuations et les espaces.

Puis je crée une fonction qui me fait la médiane:

.. code::

  def mediane(liste des occurence de chaque mot):

Elle fonctionne de cette façon:

- étape 1: Vérifier si la suite de chiffre est pair ou impaire
- étape 2: si c'est paire: faire la moyenne du chiffre du milieu et du
  précédent au chiffre du milieu
- étape 3: si c'est impaire: prendre simplement le chiffre du milieu

Et pour terminer j'utilise la médiane que j'ai faite sur les nombres d'occurences des
mots du texte:

.. code::

  médiane = int(medianne([words.count(t) for t in set(words)]))

Etape 3: Comparer pour chaque typologie son nombre d'occurrences à la médiane
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Petite explication:

Meme en connaissant le nombre d'occurence des mots-clés, nous ne pouvons pas en déduire la
présence de la typologie. Il y a ce qu'on appelle le bruit.
Si dans la grammaire il est écrit: "cette langue ne possède pas de ton" ou
"contrairement au chinois, qui a 4 tons, cette langue n'en a pas", et bien il seront
comptabilisé par l'ordinateur.  C'est pourquoi on décide que le nombre d'occurences des
mots-clés doit dépasser la médiane des mots du texte (c'est une sorte de garantie).

Meme ainsi, si le nombre d'occurence des mots-clés est trop proche de la médiane alors c'est un cas d'incertitude.

Nous avons donc une fonction qui compare le nombre d'occurences des mots-clés et la
médiane: elle renvoie soit 'Yes', soit "No", soit "None" pour l'incertitude. Cf: :ref:`src/conclusion:Annexe: Organigramme de la fonction has_key`

.. code::

  def has_key(mediane, nb d'occurrence des mots clés):

Etape 4: Mettre les données dans un tableau
++++++++++++++++++++++++++++++++++++++++++++

La dernière étape consiste a mettre ces données dans un tableau.

Pour produire un tableau j'utilise une autre librairie appelée "pandas".

.. list-table:: tableau final d'extraction de typologies(ton)
   :widths: 100 100 100 100 100 100
   :header-rows: 1
   :stub-columns: 0

   * - fichier analysé
     - typologie recherché
     - nb d'occurence
     - mediane du texte
     - difference
     - hasornot
   * - ani_2000_o
     - genders
     - 16
     - 1
     - 15
     - True
   * - aari_1994_o
     - genders
     - 6
     - 1
     - 5
     - True

- **difference** : comme dans l'organigramme, k.occu - median_occurrence
- **hasornot**:existence de la typologie recherchée
