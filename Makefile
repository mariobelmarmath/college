clean:
	find . -type d -name _build       |while read i;do rm -fr $$i ; echo '===>' $$i removed;done
	find . -type d -name __pycache__  |xargs rm -fr
	find . -type d -name .mypy_cache  |while read i;do rm -fr $$i ; echo '===>' $$i removed;done
	find . -type d -name node_modules |while read i;do rm -fr $$i ; echo '===>' $$i removed;done
	find . -type d -name .pytest_cache|xargs rm -fr
