Todo
======

- Sphinx ./3eme/techno/conf.py
- Sphinx ./2nde/cours/conf.py
- Sphinx ./3eme/stage/conf.py

Note: no migration ./info/mt_appli/dev/doc/conf.py ./3eme/rst/conf.py

Done
======

Slides
------

old::

  ./3eme/music/conf.py
  ./3eme/ang/conf.py
  ./3eme/francais/les_fenetres/conf.py
  ./3eme/stage/conf.py
  ./4eme/esp/expocontaminacion/conf.py

new::

  slides_college/
  ├── 3eme_anglais
  │   ├── img
  │   └── spacex.rst
  ├── 3eme_francais
  │   ├── img
  │   ├── les_fenetres.rst
  │   └── say.rst
  ├── 3eme_musique
  │   ├── earthsong.rst
  │   └── img
  ├── 3eme_stage_cnrs
  │   ├── img
  │   ├── say.rst
  │   ├── say.txt
  │   └── stage.rst
  ├── 4eme_espagnol
  │   ├── contaminacion_slides.rst
  │   └── images
  ├── _build
  │   ├── 3eme_anglais
  │   ├── 3eme_francais
  │   ├── 3eme_musique
  │   ├── 3eme_stage_cnrs
  │   └── 4eme_espagnol
  ├── Makefile
  └── requirements.txt

